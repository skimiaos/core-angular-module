<?php

BladeHelper::createTag('View','<div ui-view></div>');

BladeHelper::createTag('FlashMessages','<mc-messages></mc-messages>');

BladeHelper::createJsCall('State','ng-click','$state.go');
BladeHelper::createTag('EndState','</a>');

BladeHelper::createTag('Controller','<?php if(isset($__app)&&isset($__state)):ob_start(); ?>');
BladeHelper::createTag('EndController','<?php Angular::get($__app)->States->setController($__state,ob_get_clean());endif; ?>');

BladeHelper::createFunction('AddDependency',null,'<?php if(isset($__app)&&isset($__state)){ Angular::get($__app)->States->addDependency($__state,$1);} ?>');

BladeHelper::createTag('MainController','<?php if(isset($__app)):ob_start(); ?>');
BladeHelper::createTag('EndMainController','<?php Angular::get($__app)->JsModule->setMainController(ob_get_clean());endif; ?>');


BladeHelper::createFunction('GlobController',null,'<?php if(isset($__app)): ob_start(); $__globController = $1;?> ');
/*BladeHelper::createTag('GlobController','<?php if(isset($__app)&&isset($__state)):ob_start(); ?>');*/

BladeHelper::createTag('EndGlobController','<?php Angular::get($__app)->JsModule->setController($__globController,ob_get_clean()); endif; ?>');

BladeHelper::createFunction('ImportGlobController',null,'<div ng-controller="$1">');
BladeHelper::createTag('EndImportGlobController','</div>');