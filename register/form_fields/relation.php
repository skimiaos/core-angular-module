<?php

AngularFormHelper::setTemplate('relation','skimia.angular::form.fields.relation',2000);

\AngularFormHelper::setDataViewTransformer('relation',function(&$data,&$value,$key,$field,$form){

    $relation = $form->_getRawOldData($key);
    //dd($relation);
    $primaryKey = 'id';
    if(isset($field['primary_key']))
        $primaryKey = $field['primary_key'];


    if(isset($field['show_collumn_mapped']) && $field['show_collumn_mapped'] == false){
        $db_fields = ['*'];
    }else{
        if(is_array($field['show_collumn']))
            $db_fields = $field['show_collumn'];
        else
            $db_fields = [$field['show_collumn']];

        $db_fields [] = $primaryKey;
    }

    if(!isset($data[$primaryKey]))
        $id_key = 'new';
    else
        $id_key = $data[$primaryKey];
    $cachekey = get_class($form).$id_key.$key.'relation';


    if(is_a($relation,'Illuminate\Database\Eloquent\Relations\BelongsToMany')){
        if($field['relation'] != 'many_to_many'){
            throw new \Exception('le field de formulaire "'.$key.'" devrait avoir pour relation "many_to_many"');
        }

        if (!Cache::has($cachekey) || !(isset($field['cache']) && $field['cache'] > 0 )) {
            $available = Cache::remember($cachekey, isset($field['cache'])? $field['cache']:60, function() use($key,$db_fields,$relation,$field){

                /*if($key == 'articles')
                    dd($relation->getRelated()->all($db_fields)->toArray());*/

                if(isset($field['whereRaw']))
                    $collection = $relation->getRelated()->whereRaw($field['whereRaw'])->get($db_fields);
                else
                    $collection = $relation->getRelated()->all($db_fields);

                if(is_array($field['show_collumn'])){
                    $f = $field['show_collumn'][0];

                    foreach($collection as $k=>$item){

                        $e = '';
                        foreach($field['show_collumn'] as $coll){

                            
                            if(method_exists($item,'get'.ucfirst($coll))){
                                $meth = 'get'.ucfirst($coll);
                                $e = $e . $item->$meth() . ' ';
                            }else if(isset($field['nameValues']) && isset($field['nameValues'][$coll])  && isset($field['nameValues'][$coll]['true']) && $item[$coll]){
                                $e = $e . $field['nameValues'][$coll]['true'].' ';
                            }
                            else if(isset($field['nameValues']) && isset($field['nameValues'][$coll])  && isset($field['nameValues'][$coll]['false']) && !$item[$coll]){
                                $e = $e . $field['nameValues'][$coll]['false'].' ';
                            }
                            else
                                $e = $e . $item[$coll] .' ';


                        }
                        $e = trim($e);

                        $item[$f] = $e;
                        $collection[$k] = $item;
                    }
                }
                return $collection;
            });
        } else {
            $available = Cache::get($cachekey);

        }



        /*$value = [];
        $rel_as_array = $relation->get()->toArray();
        foreach ($rel_as_array as $item) {

            $arr_item = [];
            foreach ($db_fields as $_field) {
                $arr_item[$_field] = $item[$_field];
            }


            $e = '';

            if(is_array($field['show_collumn'])){

                $f = $field['show_collumn'][0];
                foreach($field['show_collumn'] as $coll){

                    $e = $e . $arr_item[$coll].' ';

                }
            }else{
                $f = $field['show_collumn'];
                $e = $arr_item[$f];
            }



            $e = trim($e);

            $arr_item[$f] = $e;
            $value[] = $arr_item;

        }*/



        $data[$key.'.relation'] = [
            'show_collumn'=> is_array($field['show_collumn']) ? $field['show_collumn'][0] : $field['show_collumn'],
            'available' => $available
        ];

        $data['__deleting'][] = $key.'.relation';
    }
    elseif(is_a($relation,'Illuminate\Database\Eloquent\Relations\BelongsTo') || is_a($relation,'Illuminate\Database\Eloquent\Relations\HasOne')){
        if($field['relation'] != 'many_to_one'){
            throw new \Exception('le field de formulaire "'.$key.'" devrait avoir pour relation "many_to_one"');
        }

        if (!Cache::has($cachekey) || !(isset($field['cache']) && $field['cache'] > 0 )) {

            $available = Cache::remember($cachekey, isset($field['cache'])? $field['cache']:60, function() use($db_fields,$relation,$field,$primaryKey){

                if(isset($field['whereRaw']))
                    $collection = $relation->getRelated()->whereRaw($field['whereRaw'])->all($db_fields);
                else
                    $collection = $relation->getRelated()->all($db_fields);

                $items = [];
                foreach ($collection as $item) {
                    if(is_array($field['show_collumn'])){
                        $e = '';
                        foreach($field['show_collumn'] as $coll){

                            if(isset($field['nameValues']) && isset($field['nameValues'][$coll])  && isset($field['nameValues'][$coll]['true']) && $item->$coll){
                                $e = $e . $field['nameValues'][$coll]['true'].' ';
                            }
                            else if(isset($field['nameValues']) && isset($field['nameValues'][$coll])  && isset($field['nameValues'][$coll]['false']) && !$item->$coll){
                                $e = $e . $field['nameValues'][$coll]['false'].' ';
                            }
                            else
                                $e = $e . $item[$coll] .' ';

                        }
                        $e = trim($e);
                    }else
                        $e = $item->{$field['show_collumn']};

                    $items[$item->$primaryKey.''] = $e;
                }

                return $items;
            });
        } else {
            $available = Cache::get($cachekey);
        }


        $data[$key.'*selectItems'] = $available;
        $relation_key = $relation->getForeignKey();
        $value = isset($data[$relation_key])?$data[$relation_key].'':null;
        $data['__deleting'][] = $key.'*selectItems';
    }
    else{
        dd(get_class($relation));
    }

},2000);

\FieldHelper::setViewDataTransformer('relation',function(&$entity,&$value,$key,$field,$form){
    $data = $form->_getRawOldData($key);

    if(isset($field['primary_key']))
        $primaryKey = $field['primary_key'];
    else
        $primaryKey = 'id';

    if(is_object($data)) {

        if (is_a($data, 'Illuminate\Database\Eloquent\Relations\BelongsToMany')) {


            $entity->save();
            $ids = array_flip($entity->$key()->getRelatedIds());
            $modified = false;
            $created_ids = [];

            foreach ($value as $item) {
                $p_key = (isset($item[$primaryKey]) ?$item[$primaryKey]:$item['id']);
                if (!isset($ids[$p_key])) {
                    $entity->$key()->attach($p_key);
                    $created_ids[] = $p_key;
                    $modified = true;
                }

                unset($ids[$p_key]);
            }
            if (count($ids) > 0) {

                $entity->$key()->detach(array_flip($ids));
                $modified = true;

            }

            if(method_exists($entity,'relationUpdate')&&$modified)
                $entity->relationUpdate($key,$created_ids,array_flip($ids));
        }

        if(is_a($data,'Illuminate\Database\Eloquent\Relations\BelongsTo')){
            $current_related = $entity->$key()->getResults();
            if(isset($current_related))
                $current_related_id = $current_related->$primaryKey;
            else
                $current_related_id = null;

            if($current_related_id !== $value){
                $new_related = $data->getRelated()->find($value);
                if(isset($new_related)){
                    $entity->save();
                    $entity->$key()->associate($new_related);
                    $entity->$key()->touch();

                    if(method_exists($entity,'relationUpdate'))
                        $entity->relationUpdate($key,$new_related,$current_related);
                }

            }

        }
    }
},2000);

\ShowHelper::setDataViewTransformer('relation',function(&$data,$value,$key,$field,$form){

    if(!isset($field['show_collumn']))
        $selector = 'COUNT';
    else
        $selector = $field['show_collumn'];

    switch($field['relation']){
        case 'many_to_many':
        case 'one_to_many':
            $linked = $data->$key;
            $max = isset($field['listMax']) ? $field['listMax']:5;
            $i = 0;
            $items = [];

            if($selector == 'COUNT'){
                $count = count($linked);
                return $count.' associe'.($count > 1? 's':'');
            }

            foreach ($linked as $link) {

                if($i >= $max) {
                    $otheritems = count($linked) - $max;
                    $line = 'et '.$otheritems.' autre'.($otheritems> 1?'s':'');

                    $items[] = $line;
                    break;
                }

                if(is_array($selector)){
                    $e = '';
                    foreach($selector as $coll){
                        $e = $e . $link->$coll.' ';

                    }
                    $e = trim($e);
                    $items[] = $e;
                }else
                    $items[] = $link->$selector;


                $i++;

            }

            return count($linked) == 0 ? 'aucun':implode(', ',$items);
            break;

        case 'many_to_one':
        case 'one_to_one':
            $linked = $data->$key;
            if(!isset($linked) || empty($linked))
                return 'Non';
            if($selector == 'COUNT'){
                return 'Oui';
            }else{
                if(is_array($selector)){
                    $e = '';
                    foreach($selector as $coll){
                        $e = $e . $linked->$coll.' ';

                    }
                    $e = trim($e);
                    return $e;
                }else
                    return $linked->$selector;
            }
            break;
    }


},2000);
