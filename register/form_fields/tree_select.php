<?php
\AngularFormHelper::setTemplate('tree_select','skimia.angular::form.fields.tree_select',2000);


function getPathInTree(array $tree,$item_id, $parent = null){
    $path = [];
    foreach ($tree as $k=>$t) {
        if(isset($t->id) && $t->id == $item_id){
            if(isset($t->parent_id) && $t->parent_id != 0)
                $path[] = $t->parent_id;
            return $path;
        }elseif(isset($t->children)){
            $tree_return = getPathInTree($t->children, $item_id, $k);
            if(is_array($tree_return)){

                if(isset($t->parent_id) && $t->parent_id != 0)
                    $path[] = $t->parent_id;
                return array_merge($path,$tree_return );
            }
        }
    }
    return false;

}
\AngularFormHelper::setDataViewTransformer('tree_select',function(&$data,&$value,$key,$field,$form){

    $choices = $form->getChoices($field);
    $data[$key.'*selectItems'] = $choices;
    $path = getPathInTree($choices,$value);
    $path[] = $value;
    $data[$key.'*selectPath'] = $path;
    $data['__deleting'][] = $key.'*selectItems';
    $data['__deleting'][] = $key.'*selectPath';
},2000);