<?php
AngularFormHelper::setTemplate('select','skimia.angular::form.fields.select',2000);


AngularFormHelper::setDataViewTransformer('select',function(&$data,&$value,$key,$field,$form){

    $data[$key.'*selectItems'] = $form->getChoices($field);
    $data['__deleting'][] = $key.'*selectItems';
},2000);