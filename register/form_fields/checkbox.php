<?php

AngularFormHelper::setTemplate('checkbox','skimia.angular::form.fields.checkbox',2000);
AngularFormHelper::setDataViewTransformer('checkbox',function(&$data,&$value,$key,$field,$form){

    if(!is_bool($value)) $value = (bool)$value;
},2000);

\ShowHelper::setDataViewTransformer('checkbox',function(&$data,$value,$key,$field,$form){
    return $value ? 'Oui':'Non';
},2000);
