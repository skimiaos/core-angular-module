<?php

AngularFormHelper::setTemplate('dynamic','skimia.angular::form.fields.dynamic',2000);


AngularFormHelper::setDataViewTransformer('dynamic',function(&$data,&$value,$key,$field,$form){

    $data[$key.'*dynamicDefinition'] = $field['dataSelector']();
    $data['__deleting'][] = $key.'*dynamicDefinition';

});