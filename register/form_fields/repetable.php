<?php

AngularFormHelper::setTemplate('repetable','skimia.angular::form.fields.repetable',2000);

AngularFormHelper::setDataViewTransformer('repetable',function(&$data,&$value,$key,$field,$form){


    if(!is_array($value))
        $value = [];


    if(AngularFormHelper::hasDataViewTransformer($field['repeat']['type']) && count($value)>0){

        foreach ($value as &$subval) {
            AngularFormHelper::TransformDataToView($field['repeat']['type'],[&$data,&$subval,$key,$field['repeat'],&$form],true);
        }


    }else{
        $subval = [];
        AngularFormHelper::TransformDataToView($field['repeat']['type'],[&$data,&$subval,$key,$field['repeat'],&$form],true);
    }
});