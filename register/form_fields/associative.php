<?php

AngularFormHelper::setTemplate('associative','skimia.angular::form.fields.associative',2000);


AngularFormHelper::setDataViewTransformer('associative',function(&$data,&$value,$key,$field,$form){



    if(!is_array($value)){
        if(is_string($value))
            $value = unserialize($value);
        else
            $value = [];

    }


    foreach ($field['fields'] as $sub_key=>$sub_field) {
        if(!isset($value[$sub_key])){
            if(isset($sub_field['default']))
                $value[$sub_key] = $sub_field['default'];
            else{
                $value[$sub_key] = '';
            }

        }
        if(AngularFormHelper::hasDataViewTransformer($sub_field['type'])){


            AngularFormHelper::TransformDataToView($sub_field['type'],[&$data,&$value[$sub_key],$key.'.'.$sub_key,$sub_field,&$form],true);
        }
    }

});