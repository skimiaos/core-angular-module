<?php

/* Se retrouve ici toute la configuration des fields sans data transformer */

AngularFormHelper::setTemplate('text'    ,'skimia.angular::form.fields.text'    ,2000);
AngularFormHelper::setTemplate('password','skimia.angular::form.fields.password',2000);
AngularFormHelper::setTemplate('textarea','skimia.angular::form.fields.textarea',2000);

AngularFormHelper::setTemplate('submit','skimia.angular::form.fields.submit',2000);

