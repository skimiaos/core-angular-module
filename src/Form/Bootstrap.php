<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 21:38
 */

namespace Skimia\Angular\Form;

use Form;
use HTML;
class Bootstrap {

    private $placeholder = false;

    private $name = '';

    public function __construct(){

    }

    public function open($name = 'form-data', array $options = array()){
        /*$controller_name = str_plural(get_class($model)) . 'Controller';
        if ($model->id) {
            $options['method'] = 'PUT';
            if (!isset($options['action'])) {
                $options['action'] = ["$controller_name@update", $model->id];
            }
        } else {
            if (!isset($options['action'])) {
                $options['action'] = ["$controller_name@store"];
            }
        }*/
        $this->name = $name;
        if (isset($options['placeholder']) && $options['placeholder'] === true) {
            $this->placeholder = true;
            unset($options['placeholder']);
        }

        $options['ng-init'] = $name.' = '.$name.' == null ? {} :'.$name;
        $options['form-name'] = $name;
        $attributes = HTML::attributes($options);

        return '<form'.$attributes.'>';

    }

    public function input($type, $name, $label = null, $value = null, $options = array(), $list = array()) {
        //$errors = $this->session->get('errors');

        if (is_array($label)) {
            $options = $label;
            $label = null;
        }
        if (is_array($value)) {
            $options = $value;
            $value = null;
        }

        if ($label == null) {
            $label = trans("form.$name");
        }

        if ($this->placeholder) {
            $options['placeholder'] = $label;
            $label = false;
        }

        if (isset($options['class'])) {
            $options['class'] .= ' form-control';
        } else {
            $options['class'] = 'form-control';
        }

        $options['ng-model'] = $this->name.'[\''.$name.'\']';


        $return = '<div class="form-group has-feedback" flex ng-class="{\'has-error\': '.\AngularFormHelper::getNgModel('errors.'.$name,$this->name).'.length > 0}">';

        if ($label !== false) {

            $return .= Form::label($name, $label);
        }

        if ($type == 'textarea') {
            $return .= Form::textarea($name, $value, $options);
        } else if ($type == 'select') {
            $return .= '<select class="form-control" ng-model="'.\AngularFormHelper::getNgModel($name,$this->name).'" ng-options="key as item for (key,item) in '.(empty($this->name)?'form':$this->name).'[\''.str_replace('.%$index','',$name).'*selectItems\']'.'"><optionvalue="">-----</option></select>';
        } else {
            $return .= html_entity_decode(Form::input($type, $name, $value, $options));
        }

        $return .= '<span class="help-block" ng-repeat="err in '.\AngularFormHelper::getNgModel('errors.'.$name,$this->name).'">';
        $return .= '{{err}}';
        $return .= '</span>';

        $return .= '</div>';

        return $return;
    }

    public function text($name, $label = null, $value = null, $options = array()) {
        return $this->input('text', $name, $label, $value, $options);
    }

    public function file($name, $label = null, $value = null, $options = array()) {
        return $this->input('file', $name, $label, $value, $options);
    }

    public function email($name, $label = null, $value = null, $options = array()) {
        return $this->input('email', $name, $label, $value, $options);
    }

    public function password($name, $label = null, $value = null, $options = array()) {
        return $this->input('password', $name, $label, $value, $options);
    }

    public function checkbox($name, $label = null, $value = null, $options = array()) {
        return $this->input('checkbox', $name, $label, $value, array_merge($options,['ng-true-value'=>"'yes'",'ng-false-value'=>"'no'"]));
    }

    public function select($name, $label = null, $value = null, $list = array(), $options = array()) {

        if (is_array($label)) {
            $list = $label;
            $label = null;
        }
        if (is_array($value)) {
            $list = $value;
            $value = null;
        }
        return $this->input('select', $name, $label, $value, $options, $list);
    }

    public function textarea($name, $label = null, $value = null, $options = array()) {
        return $this->input('textarea', $name, $label, $value, $options);
    }

    public function submit($action, $name = null){
        if (!$name) {
            $name = trans('form.submit');
        }
        if(!str_contains($action,'(')){
            $action = $action.'('.$this->name.')';
        }
        $action = str_replace(')',',$event)',$action);
        return '<div class="form-group"><input type="submit" value="'.$name.'" class="form-control btn btn-default" ng-click="'.$action.'" /></div>';
    }

    public function getSubmitAction($action,$name = false){
        if(!str_contains($action,'(')){
            $action = $action.'('.($name != false ? $name:$this->name).')';
        }
        return str_replace(')',',$event)',$action);
    }

    public function close() {
        return Form::close();
    }
} 