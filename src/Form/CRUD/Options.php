<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 16:30
 */
namespace Skimia\Angular\Form\CRUD;
use Exception;
use Skimia\Angular\Form\CRUD\Options\Access;
use Skimia\Angular\Form\CRUD\Options\Fields;
use Skimia\Angular\Form\CRUD\Options\Flash;
use Skimia\Angular\Form\CRUD\Options\Template;

class Options implements OptionsInterface,ActionOptionsInterface{

    /**
     * Pour ajouter proprement un nouveau membre de configuration ajouter le
     * membre public ${Class} en majuscule
     * la fonction associée publique
     * et l'instantiation dans le constructeur
     * afin de faire comme il se doit l'intelligence de code
     */

    /**
     * @var Access
     */
    protected $Access;

    /**
     * @var Flash
     */
    protected $Flash;

    /**
     * Fields au niveau du form
     * Attention est traduit pour chaque action donc faire attention
     * @var Fields
     */
    protected $Fields;

    /**
     * @var Template
     */
    protected $Template;

    protected $crudForm;
    protected $actionsOptionsClasses;
    protected $translationContext;

    public function __construct(CRUDForm $crudForm){
        $this->crudForm = $crudForm;
        $this->actionsOptionsClasses = [];

        $this->Access   = new Access($this);
        $this->Flash    = new Flash($this);
        $this->Template = new Template($this);
        $this->Fields   = new Fields($this);

    }


    /**
     * @return Access
     */
    public function Access(){
        return $this->Access;
    }

    /**
     * @return Flash
     */
    public function Flash(){
        return $this->Flash;
    }

    /**
     * @return Fields
     */
    public function Fields(){
        return $this->Fields;
    }

    /**
     * @return Template
     */
    public function Template(){
        return $this->Template;
    }

    /**
     * @param $actionName
     * @return Access
     */
    public function ActionAccess($actionName){
        return $this->getActionOptionsClass($actionName,'Access');
    }

    /**
     * @param $actionName
     * @return Flash
     */
    public function ActionFlash($actionName){
        return $this->getActionOptionsClass($actionName,'Flash');
    }

    /**
     * @param $actionName
     * @return Fields
     */
    public function ActionFields($actionName){
        return $this->getActionOptionsClass($actionName,'Fields');
    }

    /**
     * @param $actionName
     * @return Template
     */
    public function ActionTemplate($actionName){
        return $this->getActionOptionsClass($actionName,'Template');
    }


    /**
     * remplace le contexte par defaut(debut du Lang::get)
     * @param string $translationContext
     * @return $this
     */
    public function setTranslationContext($translationContext){
        $this->translationContext = $translationContext;
        return $this;
    }

    /**
     * retourne le contexte de traduction
     * @return string Contexte de Traduction
     * @throws Exception Tentative d'acces au contexte non defini
     */
    public function getTranslationContext(){
        if(!isset($this->translationContext))
            throw new Exception('Veillez appliquer un contexte de traduction avant de vouloir traduire des champs a l\'aide de Options->setTranslationContext()');
        return $this->translationContext;
    }



    protected function hasClass($className){

        return isset($this->$className);//TODO Check aussi si c'est un membre public

    }
    protected function hasAction($actionName){

        //TODO check if CRUD FORM HAS ACTION TRAIT attention ex EditCrudAction et ApiEditCrudAction(sans l'interface gérée)

        return true;
    }

    protected function hasClassOrFails($className){
        if(!$this->hasClass($className))
            throw new Exception('La classe d\'option "'.$className.'" N\'existe pas');//TODO lister dans l'exception la liste des membre public comme Access Flash Field et Template Etc

        return true;
    }
    protected function hasActionOrFails($actionName){

        if(!$this->hasAction($actionName)){
            throw new Exception('L\'action '.$actionName.' n\'est pas définie ajouter le Trait "'.$actionName.'CrudActionTrait" ou "Api'.$actionName.'CrudActionTrait" en use de votre classe de formulaire');
        }
        return true;
    }

    protected function newClass($class,$actionName){
        if($this->hasClassOrFails($class)){
            $classWithNamespace = get_class($this->$class);
            return new $classWithNamespace($this,$this->$class,$actionName);
        }

    }

    protected function getActionOptionsClass($actionName,$class){

        if(isset($this->actionsOptionsClasses[$actionName][$class])){
            return $this->actionsOptionsClasses[$actionName][$class];
        }

        if($this->hasActionOrFails($actionName) && $this->hasClassOrFails($class)){
            if(!isset($this->actionsOptionsClasses[$actionName]))
                $this->actionsOptionsClasses[$actionName] = [];

            $this->actionsOptionsClasses[$actionName][$class] = $this->newClass($class,$actionName);

        }

        return $this->actionsOptionsClasses[$actionName][$class];


    }

    /**
     * @return CRUDForm
     */
    public function getForm(){
        return $this->crudForm;
    }
}