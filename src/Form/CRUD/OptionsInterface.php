<?php

namespace Skimia\Angular\Form\CRUD;

use Skimia\Angular\Form\CRUD\Options\Access;
use Skimia\Angular\Form\CRUD\Options\Fields;
use Skimia\Angular\Form\CRUD\Options\Flash;
use Skimia\Angular\Form\CRUD\Options\Template;

interface OptionsInterface{

    /**
     * @return Access
     */
    public function Access();

    /**
     * @return Flash
     */
    public function Flash();

    /**
     * Fields au niveau du form
     * Attention est traduit pour chaque action donc faire attention
     * @return Fields
     */
    public function Fields();

    /**
     * remplace le contexte par defaut(debut du Lang::get)
     * @param string $translationContext
     * @return $this
     */
    public function setTranslationContext($translationContext);

    /**
     * retourne le contexte de traduction
     * @return string Contexte de Traduction
     * @throws Exception Tentative d'acces au contexte non defini
     */
    public function getTranslationContext();




    /**
     * @return CRUDForm
     */
    public function getForm();
}