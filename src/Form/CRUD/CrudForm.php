<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 24/01/2015
 * Time: 13:53
 */

namespace Skimia\Angular\Form\CRUD;

use Eloquent;
use App;
use Route;
use Exception;
use Illuminate\Support\Collection;
use Skimia\Angular\Managers\Application;
use Skimia\Form\Base\EntityForm;
use Skimia\Angular\Form\AngularTrait;
abstract class CRUDForm extends EntityForm{


    use AngularTrait;

    /**
     * @var Application
     */
    protected $app = null;
    protected $parent_state = false;

    /**
     * @var Collection
     */
    protected $actions = null;

    /**
     * @var Options
     */
    protected $options = null;

    static $p_states = [];
    /**
     * The event dispatcher instance.
     *
     * @var \Illuminate\Events\Dispatcher
     */
    protected static $dispatcher;

    /**
     * @return Eloquent
     */
    protected abstract function getNewEntity();

    /**
     * @return string
     */
    public abstract function getCRUDName();

    protected abstract function configure(OptionsInterface $options);
    protected abstract function configureActions(ActionOptionsInterface $options);


    public function __construct($eloquent = false)
    {

        if($eloquent === false)
            parent::__construct($this->getNewEntity());
        else
            parent::__construct($eloquent);

        $this->actions = new Collection();
    }

    public static function register(Application $app, $state_parent = false)
    {
        if(!isset(self::$p_states[get_called_class()])){
            self::$p_states[get_called_class()] = $state_parent;
        }

        static::$dispatcher = App::make('events');
        static::callTraitsStaticFunction('boot');
        $class = new static();
        $class->_register($app,$state_parent);

        return $class;
    }

    public function _register(Application $app, $state_parent = false)
    {
        $this->app = $app;
        $this->parent_state = $state_parent;
        $this->callTraitsFunction('registerAction');
        $this->options = new Options($this);
        $this->callTraitsFunction('configure',[$this->options]);

        $this->configure($this->options);

        $this->callTraitsFunction('configureActions',[$this->options]);
        $this->configureActions($this->options);

        //INIT TRAITS
        $this->callTraitsFunction('register');

        // register events pour la generation acl par exemple
        $this->registerEvents();

        $this->makeFields();



    }

    public function getApplication(){
        return $this->app;
    }


    /**
     * Fire the given event for the crud form.
     *
     * @param  string  $event
     * @param  bool    $halt
     * @return mixed
     */
    protected function fireCRUDFormEvent($event, $payload = [], $halt = true)
    {
        if ( ! isset(static::$dispatcher)) return true;

        // We will append the names of the class to the event to distinguish it from
        // other model events that are fired, allowing us to listen on each model
        // event set individually instead of catching event for all the models.
        $event = "crudform.{$event}: ".get_class($this);

        $method = $halt ? 'until' : 'fire';

        return static::$dispatcher->$method($event,  (is_array($payload)? $payload : [$payload]));
    }

    /**
     * Register a crud form event with the dispatcher.
     *
     * @param  string  $event
     * @param  \Closure|string  $callback
     * @param  int $priority
     * @return void
     */
    public static function registerCRUDFormEvent($event, $callback, $priority = 0)
    {
        if (isset(static::$dispatcher))
        {
            $name = get_called_class();

            static::$dispatcher->listen("crudform.{$event}: {$name}", $callback,$priority);
        }
    }

    /**
     * Register a overrideVar event with the dispatcher.
     *
     * @param  string  $event
     * @param  \Closure|string  $callback
     * @param  int $priority
     * @return void
     */
    protected static function registerOverrideVarEvent($event, $callback, $priority = 0)
    {
        if (isset(static::$dispatcher))
        {
            $name = get_called_class();

            static::$dispatcher->listen("crudform.vars.{$event}: {$name}", $callback,$priority);
        }
    }

    /**
     * fire a overrideVar event
     * @param $event
     * @param $var
     * @param $payload
     * @return mixed
     */
    protected function fireOverrideVars($event, $var, $payload = [])
    {
        $response = $var;

        $event = "crudform.vars.{$event}: ".get_class($this);

        foreach (static::$dispatcher->getListeners($event) as $listener)
        {
            $before_response = $response;
            $response = call_user_func_array($listener, [$response] + $payload);

            if($response == null) throw new Exception('une fonction d\'override doit retourner une valeur non nulle');
            // If a boolean false is returned from a listener, we will stop propagating
            // the event to any further listeners down in the chain, else we keep on
            // looping through the listeners and firing every one in our sequence.
            if ($response === false) return $before_response;

        }


        return $response;
    }

    /**
     * Register an action trait
     * @param $name
     * @return $this
     */
    protected function registerAction($name){
        $this->actions->push($name);

        return $this;
    }

    protected function registerEvents(){
        $this->options->Access()->registerAclGeneration();

        foreach($this->actions->toArray() as $action){
            $this->options->ActionAccess($action)->registerAclGeneration();
        }
    }

    public function canAction($name){
        if(!$this->actions->contains($name))
            return false;

        return $this->options->ActionAccess($name)->actionCan();
    }

    protected static function callTraitsStaticFunction($function,$params = null)
    {
        foreach (class_uses_recursive(get_called_class()) as $trait)
        {
            if (method_exists(get_called_class(), $method = $function.class_basename($trait)))
            {
                forward_static_call([get_called_class(), $method],$params);
            }
        }
    }

    protected function callTraitsFunction($function,$params = [])
    {
        foreach (class_uses_recursive(get_called_class()) as $trait)
        {
            if (method_exists(get_called_class(), $method = $function.class_basename($trait)))
            {
                call_user_func_array([$this, $method],$params);
            }
        }
    }

    public function getRouteStart()
    {
        return 'angular.' . $this->app->getName() . $this->dotParrentState() . $this->getCRUDName() . '*';
    }

    protected function getUrlStart()
    {
        return 'angular/state/' . $this->app->getName() .'/'.ltrim($this->dotParrentState(),'.').$this->getCRUDName().'/'.$this->makeParamsUri();
    }

    public function getParentState(){
        if($this->parent_state)
            return $this->parent_state;
        elseif (isset(self::$p_states[get_called_class()]))
            return self::$p_states[get_called_class()];

    }
    public function getStateStart()
    {
        return ($this->getParentState() ? $this->getParentState().'.':'').$this->getCRUDName().'*';
    }

    protected function dotParrentState()
    {
        return $this->parent_state ? '.' . $this->parent_state . '.' : '.';
    }

    protected function getQuery(){
        return $this->fireOverrideVars('rest-query',$this->getNewEntity()->newQuery(),$this->getQueryRouteParams());
    }

    /**
     * retourne la liste des paramètres pour la requette
     * @return array
     */
    public function getQueryParams(){
        return [];
    }

    /**
     * retourne la liste des paramètres pour la requette
     * @return array
     */
    protected function getQueryRouteParams(){
        $params = [];
        foreach ($this->getQueryParams() as $param) {
            $params[$param] = Route::input($param);
        }

        return $params;
    }

    public function getRouteEntityIdParam(){
        return Route::getCurrentRoute()->parameter('entityId');
    }

    protected function makeParamsUri($endBySlash = true,$forceSlash = false){
        if(count($this->getQueryParams()) > 0)
            return '{'.implode('}/{',$this->getQueryParams()).'}'.($endBySlash ? '/':'');
        return ($forceSlash ? ($endBySlash ? '/':''):'');
    }

    protected function makeRoute($name,$closure,$method = 'get', $url = false){

        forward_static_call([\Route::getFacadeRoot(),$method],
            $this->getUrlStart() . ($url? ltrim($url,'/'): $name) ,
            [
                'as'=>  $this->getRouteName($name) ,
                'uses'=> $closure

            ]);
        return $this->getRouteStart().$name;

    }

    protected function makeState($name, $templateClosure, $root = false,  $route = false,$append_to_url = ''){

        if(is_bool($route)){
            if($route)
                $route = $this->getRouteName($name);
            else
                $route = [];
        }

        $this->app->addState($this->getStateStart().$name,'/'.$this->getCRUDName().rtrim('/'.$this->makeParamsUri(false),'/').($root === true ? '':'/'.$name).$append_to_url,
            $templateClosure,
            $route
            );
    }

    protected function getRouteName($name){
        return $this->getRouteStart().$name;
    }

    protected function makeFields(){
        $this->fieldsMaked = true;
    }

    public function getOptions(){
        return $this->options;
    }

    protected function getKey($e,$key){
        if(is_object($e) && $e instanceof \stdClass)
            return $e->$key;
        else
            return $e[$key];
    }
}