<?php

namespace Skimia\Angular\Form\CRUD;

use Skimia\Angular\Form\CRUD\Options\Access;
use Skimia\Angular\Form\CRUD\Options\Fields;
use Skimia\Angular\Form\CRUD\Options\Flash;
use Skimia\Angular\Form\CRUD\Options\Template;

interface ActionOptionsInterface{

    /**
     * @param $actionName
     * @return Access
     */
    public function ActionAccess($actionName);

    /**
     * @param $actionName
     * @return Flash
     */
    public function ActionFlash($actionName);

    /**
     * @param $actionName
     * @return Fields
     */
    public function ActionFields($actionName);

    /**
     * @param $actionName
     * @return Template
     */
    public function ActionTemplate($actionName);
}