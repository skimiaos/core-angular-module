<?php

namespace Skimia\Angular\Form\CRUD\Options;

class Template extends BaseOption{

    const MODE_NULL = 0x00;
    const MODE_BLADE = 0x01;

    protected $mode = self::MODE_NULL;

    protected $template = null;
    protected $title = null;
    protected $icon = null;
    protected $addParams = null;


    /**
     * lie une vue blade
     * @param string $template nom de la template Blade
     * @param array $addParams Paramètres additionnels
     * @return $this
     */
    public function bindBlade($template,$addParams = []){
        $this->mode = static::MODE_BLADE;
        $this->template = $template;
        $this->addParams = $addParams;

        return $this;
    }

    public function render($angular_params,$params = []){
        switch($this->mode){
            case static::MODE_BLADE:
                return \View::make($this->template,
                    $params, array_merge($this->addParams,$angular_params))->render();
                break;

            default:
                return 'Unhandled View Mode';
                break;
        }
    }

    public function setTitle($title){
        $this->title = $title;

        return $this;
    }

    public function setIcon($icon){
        $this->icon = $icon;

        return $this;
    }

    public function getTitle(){
        if($this->title)
            return $this->title;
        else if($this->parent)
            return $this->parent->getTitle();
        else
            return 'Titre de l\'action';
    }

    public function getIcon(){
        if($this->icon)
            return $this->icon;
        else if($this->parent)
            return $this->parent->getIcon();
        else
            return 'os-icon-question';
    }

    public function toArray()
    {
        // TODO: Implement toArray() method.
    }
}