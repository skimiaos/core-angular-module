<?php
/**
 * Created by PhpStorm.
 * User: gsm
 * Date: 15/07/2015
 * Time: 09:50
 */

namespace Skimia\Angular\Form\CRUD\Options\Fields;

use Skimia\Angular\Form\CRUD\Options;
use Skimia\Angular\Form\CRUD\Options\Fields;

class DynamicField extends BaseField{


    protected $dataSelector = null;

    public function __construct(Options $options, $name, Fields $fieldsContainer)
    {
        parent::__construct($name, $fieldsContainer);

    }

    public function setDataSelectorClosure($function){
        $this->dataSelector = $function;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(),[
            'dataSelector'=>$this->dataSelector
        ]);
    }
}
