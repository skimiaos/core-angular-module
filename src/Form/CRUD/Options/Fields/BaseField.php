<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/06/2015
 * Time: 16:29
 */

namespace Skimia\Angular\Form\CRUD\Options\Fields;
use Skimia\Angular\Form\CRUD\Options\Fields;

class BaseField {

    protected $fieldsContainer;

    protected $name = null;
    protected $type = 'text';
    protected $label = null;
    protected $info = null;
    protected $mapped = true;
    protected $disabled = false;
    protected $default;

    protected $dispOrder = 1000;
    protected $display = true;

    protected $attrs =[];

    protected $ngShow = false;
    protected $twoWayEToForm = false;
    protected $twoWayFormToE = false;

    protected $generating = false;
    protected $template = false;

    public function __construct($name, Fields $fieldsContainer){
        $this->name = $name;
        $this->label = ucfirst($name);
        $this->fieldsContainer = $fieldsContainer;
    }

    /**
     * @param bool|true $disabled
     * @return $this
     */
    public function setDisabled($disabled = true){
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Applique le label au champ
     * @param string $label Texte du label
     * @return $this
     */
    public function setLabel($label){
        $this->label = $label;
        return $this;
    }

    /**
     * Met la chaine de traduction automatique du label
     * Par defaut applique la chaine de traduction = base(voir Options->setTranslationContext) + . le nom . + label
     * @param bool|string $translation la clef de traduction
     * @return $this
     */
    public function setTranslatedLabel($translation = false){
        if($translation)
            $this->label = trans($translation);
        else{
            $translation_base = $this->fieldsContainer->getOptions()->getTranslationContext();
            if( ($prefix = $this->fieldsContainer->getNamePrefix()) != '')
                $translation_base .= '.' . $prefix;
            $this->label = trans($translation_base . '.fields.' . $this->name .'.label');
        }
        return $this;
    }

    /**
     * Applique l'info au champ
     * @param string $label Texte du label
     * @return $this
     */
    public function setInfo($label){
        $this->info = $label;
        return $this;
    }

    /**
     * Applique les deux fonctions setTranslatedInfo et setTranslatedLabel
     * @return $this
     */
    public function transAll(){
        return $this->setTranslatedInfo()->setTranslatedLabel();
    }

    /**
     * Met la chaine de traduction automatique de l'info
     * Par defaut applique la chaine de traduction = base(voir Options->setTranslationContext) + . le nom . + info
     * @param bool|string $translation la clef de traduction
     * @return $this
     */
    public function setTranslatedInfo($translation = false){
        if($translation)
            $this->info = trans($translation);
        else{
            $translation_base = $this->fieldsContainer->getOptions()->getTranslationContext();
            if( ($prefix = $this->fieldsContainer->getNamePrefix()) != '')
                $translation_base .= '.' . $prefix;
            $this->info = trans($translation_base . '.' . $this->name .'.info');
        }
        return $this;
    }

    /**
     * Ajoute les classes css en paramètre sur le champ
     * @param array $cssClasses array de classes css
     * @return $this
     */
    public function classAttr($cssClasses = []){
        $this->mergeAttr('class',$cssClasses);
        return $this;
    }

    /**
     * Merge un attribut html avec de nouvelle valeurs
     * @see array_merge
     * @param $attr attribut
     * @param array $value array de string(implode(' '))
     * @return $this
     */
    public function mergeAttr($attr,$value = []){
        if(!isset($this->attrs[$attr]))
            $this->attrs[$attr] = [];

        $this->attrs[$attr] = array_merge($this->attrs[$attr],$value);

        return $this;
    }

    /**
     * Remplace un attribut html
     * @param $attr attribut
     * @param array $value array de string(implode(' '))
     * @return $this
     */
    public function setAttr($attr,$value = []){
        if(!isset($this->attrs[$attr]))
            $this->attrs[$attr] = [];

        $this->attrs[$attr] = $value;

        return $this;
    }

    /**
     * Met le type pour le input type
     * @param string $type
     * @return $this
     */
    public function setType($type = 'text'){

        $this->type = $type;
        return $this;
    }

    public function setNgShow($ngshow){

        $this->ngShow = $ngshow;
        return $this;
    }

    /**
     * change l'ordre dans le formulaire lower first
     * @param $order
     * @return $this
     */
    public function setDisplayOrder($order){
        $this->dispOrder = $order;
        return $this;
    }

    /**
     * indique si oui ou non le champ dépend de données relatives a l'entitée ou non
     * @param bool|false $enabled false par defaut
     * @return $this
     */
    public function setMapped($enabled = false){
        $this->mapped = $enabled;
        return $this;
    }

    /**
     * @param bool|false $value valuer par defaut
     * @return $this
     */
    public function setDefaultValue($value){
        $this->default = $value;
        return $this;
    }

    /**
     * Hide le champ(mais toujours dispo par rest query)
     * @return $this
     */
    public function hide(){
        $this->display = false;
        return $this;
    }

    protected function setName($new_name){
        $this->name = $new_name;
        return $this;
    }

    /**
     * @param $newName
     * @return BaseField
     */
    public function cloneTo($newName){
        $new  = clone $this;

        $new->setName($newName);
        $new->setTranslatedInfo();
        $new->setTranslatedLabel();

        return $this->fieldsContainer->registerField($newName,$new);
    }
    public function toArray(){
        if(is_callable($this->generating))
            call_user_func_array($this->generating,[$this]);
        $attrs = [];

        foreach ($this->attrs as $attr=>$values) {
            $attrs[$attr] = implode(' ',$values);
        }

        $field =  [
            'attrs'  => $attrs,
            'name'   => $this->name,
            'label'  => $this->label,
            'order'  => $this->dispOrder,
            'info'   => $this->info,
            'type'   => $this->type,
            'mapped' => $this->mapped,
            'template' => $this->template,
            'eToForm'=> $this->twoWayEToForm,
            '_display'=> $this->display,
            'formToE'=> $this->twoWayFormToE,
        ];
        if($this->disabled){
            $field[] = 'disabled';
        }

        if($this->ngShow){
            $field['ng-show'] = $this->ngShow;
        }

        if(isset($this->default)){
            $field['default'] = $this->default;
        }

        return $field;
    }

    /**
     * Attention unmapped by default
     * @param bool|false $eToForm
     * @param bool|false $formToE
     */
    public function setTwoWayField($eToForm = false, $formToE = false){
        $this->mapped = false;
        $this->twoWayEToForm = $eToForm;
        $this->twoWayFormToE = $formToE;
    }


    /**
     * @param $generating
     * @return $this
     */
    public function setGenerating($generating){
        $this->generating = $generating;

        return $this;
    }

    /**
     * @param $template
     * @return $this
     */
    public function setBladeTemplate($template){
        $this->template = $template;

        return $this;
    }
}