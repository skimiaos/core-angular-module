<?php
/**
 * Created by PhpStorm.
 * User: gsm
 * Date: 15/07/2015
 * Time: 09:50
 */

namespace Skimia\Angular\Form\CRUD\Options\Fields;

use Skimia\Angular\Form\CRUD\Options;
use Skimia\Angular\Form\CRUD\Options\Fields;

class AssociativeField extends BaseField{

    protected $direction = 'row';
    /**
     * Fields au niveau du form
     * Attention est traduit pour chaque action donc faire attention
     * @var Fields
     */
    protected $Fields;

    /**
     * @return Fields
     */
    public function Fields(){
        return $this->Fields;
    }

    public function setDirection($direction = 'column'){
        $this->direction = $direction;
        return $this;
    }

    public function __construct(Options $options, $name, Fields $fieldsContainer)
    {
        parent::__construct($name, $fieldsContainer);
        $this->Fields   = new Fields($options,$fieldsContainer,false, $this->name);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(parent::toArray(),[
            'fields'=>$this->Fields->toFieldsArray(false,true),
            'direction'=> $this->direction
        ]);
    }
}
