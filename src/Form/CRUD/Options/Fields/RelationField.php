<?php
/**
 * Created by PhpStorm.
 * User: gsm
 * Date: 15/07/2015
 * Time: 09:50
 */

namespace Skimia\Angular\Form\CRUD\Options\Fields;

use Skimia\Angular\Form\CRUD\Options;
use Skimia\Angular\Form\CRUD\Options\Fields;

class RelationField extends BaseField{

    protected $relationType = 'many_to_many';
    protected $primaryKey = 'id';
    protected $displayColumns = 'COUNT';
    protected $maxRelated = 5;
    protected $cache = false;
    protected $showColumnMapped = true;
    protected $rawWhere = false;

    protected $nameValues = [];


    public function setPrimaryKey($primaryKey = 'id'){
        $this->primaryKey = $primaryKey;
        return $this;
    }

    public function ManyToManyRelation(){
        $this->relationType = 'many_to_many';
        return $this;
    }

    public function OneToManyRelation(){
        $this->relationType = 'one_to_many';
        return $this;
    }

    public function ManyToOneRelation(){
        $this->relationType = 'many_to_one';
        return $this;
    }

    public function OneToOneRelation(){
        $this->relationType = 'one_to_one';
        return $this;
    }


    /**
     * indique si la display column est mappé ou générée
     * @param bool|false $mapped
     * @return $this
     */
    public function showColMapped($mapped = false){
        $this->showColumnMapped = $mapped;
        return $this;
    }


    public function setBoolValuesForColumnNames($col,$true,$false){

        $this->nameValues[$col] = [
            'true'=>$true,
            'false'=>$false
        ];
        return $this;
    }

    public function displayColumns($cols = []){
        $this->displayColumns = is_array($cols) ? $cols : [$cols];
        return $this;
    }

    public function displayCounter(){
        $this->displayColumns = 'COUNT';
        return $this;
    }


    public function setMaxDisplayRelatedItems($count = 5){
        $this->maxRelated = $count;
        return $this;
    }

    /**
     * @param int $minutes
     * @return $this
     */
    public function setCacheLimit($minutes = 60){
        $this->cache = $minutes;
        return $this;
    }

    /**
     * @param string $raw
     * @return $this
     */
    public function setWhereRaw($raw){
        $this->rawWhere = $raw;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $arr = array_merge(parent::toArray(),[

            'primary_key'=> $this->primaryKey,
            'relation'=> $this->relationType,
            'show_collumn' => $this->displayColumns,
            'listMax' => $this->maxRelated,
            'cache' => $this->cache,
            'show_collumn_mapped' => $this->showColumnMapped,
            'nameValues' => $this->nameValues


        ]);
        if($this->rawWhere)
            $arr['whereRaw'] = $this->rawWhere;

        return $arr;
    }
}
