<?php
/**
 * Created by PhpStorm.
 * User: gsm
 * Date: 15/07/2015
 * Time: 09:50
 */

namespace Skimia\Angular\Form\CRUD\Options\Fields;

use Skimia\Angular\Form\CRUD\Options;
use Skimia\Angular\Form\CRUD\Options\Fields;

class TreeSelectField extends BaseField{

    protected $static_choices = [];
    protected $choices_func = false;


    /**
     * utilise les choix d'une fonction de rappel (Closure)
     * @param $closure
     * @return $this
     */
    public function setProgrammingChoices($closure){
        $this->choices_func = $closure;

        return $this;
    }

    /**
     * utilise les choix bruts d'un array
     * @param $closure
     * @return $this
     */
    public function setChoices($choices = []){
        $this->static_choices = $choices;

        return $this;
    }


    /**
     * @return array
     */
    public function toArray()
    {
        $arr = parent::toArray();

        if($this->choices_func !== false)
            $arr['choicesFct'] = $this->choices_func;
        else
            $arr['choices'] = $this->static_choices;

        return $arr;
    }
}
