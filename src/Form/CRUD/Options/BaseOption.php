<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 17:21
 */

namespace Skimia\Angular\Form\CRUD\Options;


use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Options;
use Exception;

abstract class BaseOption {

    /**
     * @var BaseOption
     */
    protected $parent = false;

    protected $action = false;

    /**
     * @var Options
     */
    protected $options = null;

    public function __construct(Options $options, $parent = false, $action = false){

        $this->parent = $parent;
        $this->action = $action;

        $this->options = $options;
    }

    abstract public function toArray();

    /**
     * retourne les options générales
     * @return Options
     */
    public function getOptions(){
        return $this->options;
    }

    public function toCollection(){
        return new Collection($this->toArray());
    }

    protected function isActionConfig(){
        return $this->action !== false;
    }

    protected function throwUnImplementedOption($name){
        throw new Exception('Option ['.$name.'] non implémentée elle n\'aura aucune incidence');
    }
}