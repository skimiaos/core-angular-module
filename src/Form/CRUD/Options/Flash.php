<?php

namespace Skimia\Angular\Form\CRUD\Options;
class Flash extends BaseOption{




    protected $stadardMessages = [];


    /**
     * Applique un Flashmessage pour un contexte donné
     * Lancé automatiquement lors de l'appel du contexte voir les actionTrait
     * pour les contextes utilisées
     * %{name} ou name est un champ de l'entitée
     * @param string $context le contexte ex 'create'
     * @param string $message le message
     * @param string $type le type success, info, warning, error
     * @param int $timeout timeout en ms
     * @return $this
     */
    public function setForContext($context, $message, $type='success', $timeout = 5000){

        $this->stadardMessages[$context] = [
            'message' => $message,
            'type'    => $type,
            'timeout' => $timeout
        ];
        return $this;
    }

    /**
     * @param $context
     * @param bool|false $entity
     * @return $this
     */
    public function fireContextFlashMessages($context, $entity = false){
        if(!isset($this->stadardMessages[$context]))
            return $this;

        $flash = $this->stadardMessages[$context];

        $message = $flash['message'];

        if(str_contains($message,'%')){
            if(!$entity)
                \AResponse::addMessage('Vous utilisez la personalisation de message avec entité or aucune entitée n\'a été transmise veuillez vous renseigner sur les messages envoyées par votre action','warning');
            else{
                preg_match_all("/%([a-zA-Z0-9_]+)%/",
                    $message,
                    $matches, PREG_PATTERN_ORDER);
                if(count($matches) > 1){
                    foreach ($matches[1] as $field) {
                        $message = str_replace("%$field%",$entity->$field,$message);
                    }

                }

            }
        }
        \AResponse::addMessage($message,$flash['type'],$flash['timeout']);

        return $this;

    }

    public function toArray()
    {
        // TODO: Implement toArray() method.
    }
}