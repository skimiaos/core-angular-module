<?php

namespace Skimia\Angular\Form\CRUD\Options;
use Orchestra\Support\Collection;
use Skimia\Angular\Form\CRUD\Options;
use Skimia\Angular\Form\CRUD\Options\Fields\BaseField;
use Skimia\Angular\Form\CRUD\Options\Fields\AssociativeField;
use Skimia\Angular\Form\CRUD\Options\Fields\RelationField;
use Skimia\Angular\Form\CRUD\Options\Fields\RepeatableField;
use Skimia\Angular\Form\CRUD\Options\Fields\SelectField;
use Skimia\Angular\Form\CRUD\Options\Fields\TreeSelectField;
use Skimia\Angular\Form\CRUD\Options\Fields\DynamicField;

class Fields extends BaseOption{


    /**
     * @var Collection
     */
    protected $fields;

    protected $field;
    protected $fieldName;

    protected $oneFieldContainer = false;

    protected $fieldNamePrefix = '';

    protected $displayRoutine = null;

    public function __construct(Options $options, $parent = false, $action = false, $fieldNamePrefix = '')
    {
        $this->fields = new Collection();
        $this->fieldNamePrefix = $fieldNamePrefix;
        parent::__construct($options, $parent,$action);
    }

    public function oneFieldContainer($ofc = true){
        $this->oneFieldContainer = $ofc;
        return $this;
    }


    /**
     * Crée un nouveau champ simple
     * @param $name
     * @return BaseField
     */
    public function makeField($name,$type = 'text'){
        $field = new BaseField($name,$this);
        $field->setType($type);
        return $this->registerField($name, $field);
    }

    /**
     * Crée un nouveau champ text
     * @param $name
     * @return BaseField
     */
    public function makeTextField($name){
        return $this->makeField($name,'text');
    }


    /**
     * Crée un nouveau champ password
     * @param $name
     * @return BaseField
     */
    public function makePasswordField($name){
        return $this->makeField($name,'password');
    }

    /**
     * Crée un nouveau champ textarea
     * @param $name
     * @return BaseField
     */
    public function makeTextareaField($name){
        return $this->makeField($name,'textarea');
    }

    /**
     * Crée un nouveau champ wysiwyg
     * @param $name
     * @return BaseField
     */
    public function makeWYSIWYGField($name){
        return $this->makeField($name,'wysiwyg');
    }

    /**
     * Crée un nouveau champ image
     * @param $name
     * @return BaseField
     */
    public function makeImageField($name){
        return $this->makeField($name,'fileselector');
    }

    /**
     * Crée un nouveau champ checkbox
     * @param $name
     * @return BaseField
     */
    public function makeCheckboxField($name){
        return $this->makeField($name,'checkbox');
    }

    /**
     * Crée un nouveau champ Associatif
     * @param $name
     * @return AssociativeField
     */
    public function makeAssociativeField($name){
        $field = new AssociativeField($this->options, $name,$this);
        $field->setType('associative');
        return $this->registerField($name, $field);
    }

    /**
     * Crée un nouveau champ Dynamique
     * @param $name
     * @return DynamicField
     */
    public function makeDynamicField($name){
        $field = new DynamicField($this->options, $name,$this);
        $field->setType('dynamic');
        return $this->registerField($name, $field);
    }

    /**
     * Crée un nouveau champ Relationnel
     * @param $name
     * @return RelationField
     */
    public function makeRelationField($name){
        $field = new RelationField($name,$this);
        $field->setType('relation');
        return $this->registerField($name, $field);
    }

    /**
     * Crée un nouveau champ Repeatable
     * @param $name
     * @return RepeatableField
     */
    public function makeRepeatableField($name){
        $field = new RepeatableField($this->options, $name,$this);
        $field->setType('repetable');
        return $this->registerField($name, $field);
    }

    /**
     * Crée un nouveau champ Select
     * @param $name
     * @return SelectField
     */
    public function makeSelectField($name){
        $field = new SelectField($name,$this);
        $field->setType('select');
        return $this->registerField($name, $field);
    }

    /**
     * Crée un nouveau champ Select
     * @param $name
     * @return TreeSelectField
     */
    public function makeTreeSelectField($name){
        $field = new TreeSelectField($name,$this);
        $field->setType('tree_select');
        return $this->registerField($name, $field);
    }



    public function registerField($name,BaseField $class){
        if($this->oneFieldContainer){
            if(isset($this->field) && !empty($this->field)){
                throw new \Exception('ce conteneur de field est un conteneur unique(un seul champ) et le champ a déja été défini');
            }else{
                $this->field = $class;
                $this->fieldName = $name;
            }
        }else{
            $this->fields->put($name,$class);
        }

        return $class;
    }

    /**
     * definie une fonction de rappel qui retoure un bool pour l'affichage ou non d'un truc;
     * @param $closure
     */
    public function setDisplayRoutine($closure){
        $this->displayRoutine = $closure;
    }
    /**
     * retourne le prefixe pour les champs de ce container
     * @return string
     */
    public function getNamePrefix(){
        return $this->fieldNamePrefix;
    }

    /**
     * renvoie un array mergé et trié pour l'action définie
     * @param $actionName
     * @return array|bool
     */
    public function toFieldsArray($actionName = false, $direct = false)
    {
        if($direct)
            $glob_fields = $this->toArray();
        else
            $glob_fields = $this->options->Fields()->toArray();

        if(!$actionName && !$this->action)
            $fields = $glob_fields;
        else
            $fields = array_merge($glob_fields,$this->options->ActionFields($actionName ? $actionName : $this->action)->toArray());

        $fields = array_reverse($fields,true);


        uasort($fields,[$this,"fieldsComparator"]);

        return $this->__displayRoutine($fields);
    }

    public function __displayRoutine($fields = []){
        if(!isset($this->displayRoutine))
            return $fields;

        $newfields = [];
        $dr = $this->displayRoutine;
        /*get_class($fields);
        die();*/
        try{
            foreach ($fields as $k=>$value) {
                if($dr($k,$value))
                    $newfields[$k] = $value;
            }
        }catch(\Exception $e){
            var_dump(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,5));

            die();
        }


        return $newfields;


    }

    /**
     * Attention array non mergé
     * @return array
     */
    public function toArray(){

        if($this->oneFieldContainer){
            return [
                'field'=> $this->field->toArray(),
                'name'=>$this->fieldName
            ];
        }
        $fields = [];

        foreach ($this->fields as $name=>$field) {
            $fields[$name] = $field->toArray();
        }

        return $this->__displayRoutine($fields);


    }

    public function singleFieldToArray(){
        if(!$this->oneFieldContainer)
            throw new \Exception('ce conteneur n\'est pas un conteneur unique (un seul field)');
        elseif(!isset($this->field) || !is_object($this->field))
            throw new \Exception('le field n\'est pas defini!');
        else
            return $this->field->toArray();
    }

    public function fieldsComparator($a,$b){
        if(!isset($a['order']))
            $a['order'] = 100;
        if(!isset($b['order']))
            $b['order'] = 100;

        if ($a['order'] == $b['order']) {
            return 0;
        }
        return ($a['order'] < $b['order']) ? -1 : 1;


    }

}

