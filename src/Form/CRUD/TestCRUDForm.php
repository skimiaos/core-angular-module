<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 17:42
 */

namespace Skimia\Angular\Form\CRUD;

use Eloquent;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudConfiguration;

class TestCRUDForm extends CRUDForm{

    use ListCrudActionTrait;

    /**
     * @inheritdoc
     */
    protected function getNewEntity()
    {
        // TODO: Implement getNewEntity() method.
    }

    /**
     * @inheritdoc
     */
    protected function configure(Options $options)
    {
        $options->setTranslationContext('skimia.angular::form');
        // Utilise les droits de l'app ici le backend (utilisateur avec l'access backend)
        $options->Access->applicationAccess();
        // Utilise les droits customisables(fields etc) et par defaut tout le monde y a accés
        //par ailleurs cette config est spécifique a l'action(page) de listing
        $options->ActionAccess('list')->configurableAuto(true);
        // listConfiguration ajoutée par le trait d'action ListCrudActionTrait
        $this->listConfiguration
            ->enableSorting(true)
            ->enableFiltering(true)
            ->enableColumnMenus(true)
            ->enableGridMenu(true)
            ->enableHorizontalScrollbar(ListCrudConfiguration::SCROLLBARS_ALWAYS)
            ->enableVerticalScrollbar(ListCrudConfiguration::SCROLLBARS_ALWAYS)
            ->setPaginationPageSizes(25, [25, 50])->setBigPaginationPageSizes();

        //Ajout la possibilité de suppression par lot
        $this->listConfiguration->addBatchDelete();

        //Ajout de la batch action test pour executer la methode de test du model managé
        $this->listConfiguration->addBatchAction('test',function(array $ids){

            $this->getNewEntity()->findMany($ids)->map(function(Eloquent $entity){
                $entity->testMethod();
            });

        });

        $idColumn = $this->listConfiguration->getNewColumnDefinition('id');

        $idColumn->displayName('Identifiant');
        $idColumn->enableColumnMenu(true);
        $idColumn->enableFiltering(false);
        $idColumn->enableSorting(true);
        $idColumn->enableHiding(false);
        $idColumn->type(ListCrudColumnConfiguration::TYPE_NUMBER);


        /**
         * MESSAGES
         */
        $options->Flash->setForContext('create-save','Test est bien crée','info',3000);
        $options->Flash->setForContext('create-get','Nouveau Test bien chargé','info',2000);

        $options->ActionTemplate('list')->bindBlade('skimia.angular::form.crud.list',[
            'crudOPT' => 'WTF?'
        ]);

        $options->Fields->makeField('name')
            ->classAttr(['msc-header'])
            ->setTranslatedInfo() //traduction auto
            ->setTranslatedLabel()
            ->setType('text');//texte par defaut



    }
}