<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Get;


use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Options;

/**
 * Class GetCrudActionTrait
 * @package Skimia\Angular\Form\CRUD\Actions\Get
 * @property GetRestConfiguration $getRestConfiguration
 */
trait GetCrudActionTrait {

    use GetRestActionTrait;
    /**
     * @var GetCrudConfiguration
     */
    protected $getConfiguration = null;

    protected function configureGetCrudActionTrait(Options $options){
        $this->getConfiguration = new GetCrudConfiguration($options);
    }

    protected function configureActionsGetCrudActionTrait(ActionOptionsInterface $options){
        //$options->ActionTemplate(static::$EDIT_REST_ACTION)->bindBlade('skimia.angular::form.crud.actions.edit');

    }

    /**
     * @return GetCrudConfiguration
     */
    public function getGetConfiguration(){
        return $this->getConfiguration;
    }


    protected function registerGetCrudActionTrait(){

        $form = $this;

        $this->makeState('get',function($params) use($form){


            if(!$this->options->ActionAccess(static::$GET_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }


            $tpl = $this->options->ActionTemplate(static::$GET_REST_ACTION)->render($params,[
                'form'=>$this,
                'fields'=>$this->makeRestGetFields(),
                'list_state'=> $this->getStateStart().'list',
                'icon'  => $this->options->ActionTemplate(static::$GET_REST_ACTION)->getIcon() ,
                'title' => $this->options->ActionTemplate(static::$GET_REST_ACTION)->getTitle() ,

            ]);

            return $tpl;

        },true,true,$this->getConfiguration->getStateUrl());


    }
}