<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Get;

use AResponse;
use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Options;

trait GetRestActionTrait {

    public static $GET_REST_ACTION = 'get';
    /**
     * @var GetRestConfiguration
     */
    protected $getRestConfiguration = null;
    protected $getRestRoute = null;

    protected function configureGetRestActionTrait(Options $options){
        $this->getRestRoute = $this->getRouteName(static::$GET_REST_ACTION);
        $this->getRestConfiguration = new GetRestConfiguration($options);


    }

    /**
     * @return GetRestConfiguration
     */
    public function getGetRestConfiguration(){
        return $this->getRestConfiguration;
    }

    protected function registerActionGetRestActionTrait(){
        $this->registerAction(static::$GET_REST_ACTION);
    }

    protected function registerGetRestActionTrait(){

        $this->getRestRoute = $this->makeRoute(static::$GET_REST_ACTION,function($entityId) {

            if(!$this->options->ActionAccess(static::$GET_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }


            $entityId = $this->getRouteEntityIdParam();

            $this->initGetFor($entityId);


            return AResponse::r([
                'entity'=>$this->data
            ]);


        },'get','/{entityId}/show');

    }

    public function initgetFor($id){

        $query = $this->fireOverrideVars('rest-get-query',$this->getQuery(),$this->getQueryRouteParams());

        $this->data = $this->fireOverrideVars('rest-get',$query->where('id',$id)->first());
    }

    public function makeRestGetFields(){
        $this->fields = $this->options->ActionFields(self::$GET_REST_ACTION)->toFieldsArray(self::$GET_REST_ACTION);
        $this->fieldsMaked = true;
        return $this->fields;
    }

}
