<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:45
 */

namespace Skimia\Angular\Form\CRUD\Actions\Get;


use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Actions\BaseCRUDConfiguration;
use Eloquent;
use Skimia\Angular\Form\CRUD\Options;

class GetCrudConfiguration extends BaseCRUDConfiguration{

    protected $getCrudStateUrl = '/{entityId}';


    public function setStateUrl($url){
        $this->getCrudStateUrl = $url;

        return $this;
    }

    public function getStateUrl(){
        return $this->getCrudStateUrl;
    }

}