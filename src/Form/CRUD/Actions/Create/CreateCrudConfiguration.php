<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:45
 */

namespace Skimia\Angular\Form\CRUD\Actions\Create;


use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Actions\BaseCRUDConfiguration;
use Eloquent;
use Skimia\Angular\Form\CRUD\Options;

class CreateCrudConfiguration extends BaseCRUDConfiguration{


    protected $createCrudStateUrl = '/new';

    protected $redirect = 'list';


    public function setStateUrl($url){
        $this->createCrudStateUrl = $url;

        return $this;
    }

    public function getStateUrl(){
        return $this->createCrudStateUrl;
    }

    public function redirectToListAfter(){
        $this->redirect = 'list';
    }

    public function redirectToEditAfter(){
        $this->redirect = 'edit';
    }

    public function getRedirectAfter(){
        return $this->redirect;
    }
}