<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Create;

use AResponse;
use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Options;

trait CreateRestActionTrait {

    public static $CREATE_REST_ACTION = 'create';
    /**
     * @var CreateRestConfiguration
     */
    protected $createRestConfiguration = null;
    protected $createRestRoute = null;
    protected $getCreateRestRoute = null;

    protected function configureCreateRestActionTrait(Options $options){
        $this->createRestRoute = $this->getRouteName(static::$CREATE_REST_ACTION);
        $this->editRestConfiguration = new CreateRestConfiguration($options);

        $this->options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave',
            'Entitée bien crée','success',5000);

    }

    /**
     * @return CreateRestConfiguration
     */
    public function getCreateRestConfiguration(){
        return $this->createRestConfiguration;
    }

    public function createRestRoute(){
        return $this->createRestRoute;
    }

    protected function registerActionCreateRestActionTrait(){
        $this->registerAction(static::$CREATE_REST_ACTION);
    }

    protected function registerCreateRestActionTrait(){

        $this->getCreateRestRoute = $this->makeRoute(static::$CREATE_REST_ACTION,function() {

            if(!$this->options->ActionAccess(static::$CREATE_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }


            $this->makeRestCreateFields();
            $this->data = $this->getNewEntity();


            $entity = $this->fireOverrideVars('rest-get-create', $this->getAllOldData());

            return AResponse::r($this->fireOverrideVars('rest-get-create-response',[
                'form'=>$entity
            ]));


        },'get','/new');

        $this->createRestRoute = $this->makeRoute(static::$CREATE_REST_ACTION.'-post',function() {

            if(!$this->options->ActionAccess(static::$CREATE_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }


            $this->makeRestCreateFields();
            $this->data = $this->getNewEntity();

            if(!is_object($this->fields))
                $this->fields = new Collection($this->fields);



            if($this->isValid()){
                $entity = $this->makeEntity();

                $this->fireCRUDFormEvent('beforeSave',$entity);
                $this->fireCRUDFormEvent('beforeCreateSave',$entity);
                $entity->save();
                $this->fireCRUDFormEvent('afterSave',$entity);
                $this->fireCRUDFormEvent('afterCreateSave',$entity);
                $this->options->ActionFlash(self::$CREATE_REST_ACTION)->fireContextFlashMessages('createSave',$entity);

                //\AResponse::addMessage($this->saveSuccessMessage); Message de sauvegarde effectuée
                return \AResponse::r(
                    [
                        'create'=>true,
                        'entity'=>$this->data->toArray()
                    ]);
            }

            $form = $this->data->toArray();
            foreach($this->fields as $key=>$field){
                if(!isset($form[$key]))
                    $form[$key] = $this->getOldData($key);
            }
            return \AResponse::r(['create'=>false,'form'=>$form]);



        },'post','/new');

    }

    public function makeRestCreateFields(){
        $this->fields = $this->options->ActionFields(self::$CREATE_REST_ACTION)->toFieldsArray(self::$CREATE_REST_ACTION);
        $this->fieldsMaked = true;
        return $this->fields;
    }


}
