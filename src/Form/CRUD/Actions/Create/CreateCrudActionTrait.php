<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Create;


use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Options;

/**
 * Class CreateCrudActionTrait
 * @package Skimia\Angular\Form\CRUD\Actions\Create
 * @property CreateRestConfiguration $createRestConfiguration test
 */
trait CreateCrudActionTrait {

    use CreateRestActionTrait;
    /**
     * @var CreateCrudConfiguration
     */
    protected $createConfiguration = null;

    protected function configureCreateCrudActionTrait(Options $options){
        $this->createConfiguration = new CreateCrudConfiguration($options);
    }

    protected function configureActionsCreateCrudActionTrait(ActionOptionsInterface $options){
        $options->ActionTemplate(static::$CREATE_REST_ACTION)->bindBlade('skimia.angular::form.crud.actions.create');

    }

    /**
     * @return CreateCrudConfiguration
     */
    public function getCreateConfiguration(){
        return $this->createConfiguration;
    }


    protected function registerCreateCrudActionTrait(){

        $form = $this;

        $this->makeState(static::$CREATE_REST_ACTION,function($params) use($form){


            if(!$this->options->ActionAccess(static::$CREATE_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }

            $pp = [];
            foreach ($this->getQueryParams() as $p) {
                $pp[$p] = ':'.$p;
            }

            $params['post_url'] = route($this->getRouteName(self::$CREATE_REST_ACTION.'-post'),$pp);


            return $this->options->ActionTemplate(static::$CREATE_REST_ACTION)->render($params,[
                'form'=>$this,
                'fields'=> $this->makeRestCreateFields(),
                'list_state'=> $this->getStateStart().'list',
                'edit_state'=> $this->getStateStart().'edit',
                'redirect'=> $this->createConfiguration->getRedirectAfter(),
                'icon'  => $this->options->ActionTemplate(static::$CREATE_REST_ACTION)->getIcon() ,
                'title' => $this->options->ActionTemplate(static::$CREATE_REST_ACTION)->getTitle() ,
                'action' => 'create'

            ]);

        },true,true,$this->getCreateConfiguration()->getStateUrl());


    }
}