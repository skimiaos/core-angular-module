<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Delete;

use AResponse;
use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Options;

trait DeleteRestActionTrait {

    public static $DELETE_REST_ACTION = 'delete';
    /**
     * @var DeleteRestConfiguration
     */
    protected $deleteRestConfiguration = null;
    protected $deleteRestRoute = null;

    protected function configureDeleteRestActionTrait(Options $options){
        $this->deleteRestRoute = $this->getRouteName(static::$DELETE_REST_ACTION);
        $this->deleteRestConfiguration = new DeleteRestConfiguration($options);

        $this->options->ActionFlash(self::$DELETE_REST_ACTION)->setForContext('deleteDone',
            'Entitée bien supprimée','warning',5000);

    }

    /**
     * @return DeleteRestConfiguration
     */
    public function getDeleteRestConfiguration(){
        return $this->deleteRestConfiguration;
    }

    protected function registerActionDeleteRestActionTrait(){
        $this->registerAction(static::$DELETE_REST_ACTION);
    }

    protected function registerDeleteRestActionTrait(){

        $this->deleteRestRoute = $this->makeRoute(static::$DELETE_REST_ACTION,function($entityId) {

            if(!$this->options->ActionAccess(static::$DELETE_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }


            $entityId = $this->getRouteEntityIdParam();

            $this->initDeleteFor($entityId);

            if(!is_object($this->fields))
                $this->fields = new Collection($this->fields);


            $this->data->delete();

            $this->options->ActionFlash(self::$DELETE_REST_ACTION)->fireContextFlashMessages('deleteDone',$this->data);

            return \AResponse::r(['delete'=>true]);



        },'delete','/{entityId}/delete');

    }


    public function initDeleteFor($id){

        $query = $this->fireOverrideVars('rest-get-delete-query',$this->getQuery(),$this->getQueryRouteParams());


        $this->data = $query->where('id',$id)->first();
    }

}
