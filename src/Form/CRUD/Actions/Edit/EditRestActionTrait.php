<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Edit;

use AResponse;
use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Options;

trait EditRestActionTrait {

    public static $EDIT_REST_ACTION = 'edit';
    /**
     * @var EditRestConfiguration
     */
    protected $editRestConfiguration = null;
    protected $editRestRoute = null;
    protected $getEditRestRoute = null;

    protected function configureEditRestActionTrait(Options $options){
        $this->editRestRoute = $this->getRouteName(static::$EDIT_REST_ACTION);
        $this->editRestConfiguration = new EditRestConfiguration($options);

        $this->options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave',
            'Entitée bien sauvegardée','success',5000);

    }

    /**
     * @return EditRestConfiguration
     */
    public function getEditRestConfiguration(){
        return $this->editRestConfiguration;
    }

    public function editRestRoute(){
        return $this->editRestRoute;
    }

    protected function registerActionEditRestActionTrait(){
        $this->registerAction(static::$EDIT_REST_ACTION);
    }

    protected function registerEditRestActionTrait(){

        $this->getEditRestRoute = $this->makeRoute(static::$EDIT_REST_ACTION,function($entityId) {

            if(!$this->options->ActionAccess(static::$EDIT_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }

            $entityId = $this->getRouteEntityIdParam();



            $this->initEditFor($entityId);

            $entity = $this->fireOverrideVars('rest-get-edit', $this->getAllOldData());

            return AResponse::r($this->fireOverrideVars('rest-get-edit-response',[
                'form'=>$entity
            ]));


        },'get','/{entityId}/edit');

        $this->editRestRoute = $this->makeRoute(static::$EDIT_REST_ACTION.'-post',function($entityId) {

            if(!$this->options->ActionAccess(static::$EDIT_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }

            $entityId = $this->getRouteEntityIdParam();
            //dd($entityId);
            $this->initEditFor($entityId);

            if(!is_object($this->fields))
                $this->fields = new Collection($this->fields);



            if($this->isValid()){
                $entity = $this->makeEntity();
                $this->fireCRUDFormEvent('beforeSave',$entity);
                $entity->save();
                $this->fireCRUDFormEvent('afterSave',$entity);
                $this->options->ActionFlash(self::$EDIT_REST_ACTION)->fireContextFlashMessages('editSave',$entity);

                //\AResponse::addMessage($this->saveSuccessMessage); Message de sauvegarde effectuée
                return \AResponse::r(
                    [
                        'edit'=>true,
                        'entity'=>$this->data->toArray()
                    ]);
            }

            $form = $this->data->toArray();
            foreach($this->fields as $key=>$field){
                if(!isset($form[$key]))
                    $form[$key] = $this->getOldData($key);
            }
            return \AResponse::r(['edit'=>false,'form'=>$form]);



        },'post','/{entityId}/edit');

    }

    public function makeRestEditFields(){
        $this->fields = $this->options->ActionFields(self::$EDIT_REST_ACTION)->toFieldsArray(self::$EDIT_REST_ACTION);
        $this->fieldsMaked = true;
        return $this->fields;
    }

    public function initEditFor($id){

        $query = $this->fireOverrideVars('rest-get-edit-query',$this->getQuery(),$this->getQueryRouteParams());


        //dd(\Route::getCurrentRoute()->parameters());
        $this->makeRestEditFields();
        $this->data = $query->where('id',$id)->first();
    }

}
