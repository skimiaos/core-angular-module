<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:45
 */

namespace Skimia\Angular\Form\CRUD\Actions\Edit;


use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Actions\BaseCRUDConfiguration;
use Eloquent;
use Skimia\Angular\Form\CRUD\Options;

class EditCrudConfiguration extends BaseCRUDConfiguration{

    protected $editCrudStateUrl = '/{entityId}/edit';


    public function setStateUrl($url){
        $this->editCrudStateUrl = $url;

        return $this;
    }

    public function getStateUrl(){
        return $this->editCrudStateUrl;
    }

}