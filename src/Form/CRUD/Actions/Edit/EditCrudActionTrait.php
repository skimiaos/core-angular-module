<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Edit;


use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Options;

/**
 * Class EditCrudActionTrait
 * @package Skimia\Angular\Form\CRUD\Actions\Edit
 * @property EditRestConfiguration $editRestConfiguration
 */
trait EditCrudActionTrait {

    use EditRestActionTrait;
    /**
     * @var EditCrudConfiguration
     */
    protected $editConfiguration = null;

    protected function configureEditCrudActionTrait(Options $options){
        $this->editConfiguration = new EditCrudConfiguration($options);
    }

    protected function configureActionsEditCrudActionTrait(ActionOptionsInterface $options){
        $options->ActionTemplate(static::$EDIT_REST_ACTION)->bindBlade('skimia.angular::form.crud.actions.edit');

    }

    /**
     * @return EditCrudConfiguration
     */
    public function getEditConfiguration(){
        return $this->editConfiguration;
    }


    protected function registerEditCrudActionTrait(){

        $form = $this;

        $this->makeState('edit',function($params) use($form){


            if(!$this->options->ActionAccess(static::$EDIT_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }


            $pp = [];
            foreach ($this->getQueryParams() as $p) {
                $pp[$p] = ':'.$p;
            }

            $params['post_url'] = route($this->getRouteName(self::$EDIT_REST_ACTION.'-post'),array_merge($pp,[
                'entityId' => ':id'
            ]));

            return $this->options->ActionTemplate(static::$EDIT_REST_ACTION)->render($params,[
                'form'=>$this,
                'fields'=> $this->makeRestEditFields(),
                'list_state'=> $this->getStateStart().'list',
                'edit_state'=> $this->getStateStart().'edit',
                'icon'  => $this->options->ActionTemplate(static::$EDIT_REST_ACTION)->getIcon() ,
                'title' => $this->options->ActionTemplate(static::$EDIT_REST_ACTION)->getTitle() ,
                'action' => 'edit'

            ]);

        },true,true,$this->getEditConfiguration()->getStateUrl());


    }
}