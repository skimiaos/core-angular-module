<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:47
 */
namespace Skimia\Angular\Form\CRUD\Actions;

use Skimia\Angular\Form\CRUD\Options;

class BaseCRUDConfiguration {

    /**
     * @var Options
     */
    protected $options = null;
    public function __construct(Options $options){
        $this->options = $options;
    }
}