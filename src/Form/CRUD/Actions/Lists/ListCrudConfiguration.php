<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:45
 */

namespace Skimia\Angular\Form\CRUD\Actions\Lists;


use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Actions\BaseCRUDConfiguration;
use Eloquent;
use Skimia\Angular\Form\CRUD\Options;

class ListCrudConfiguration extends BaseCRUDConfiguration{

    const SCROLLBARS_ALWAYS = 'uiGridConstants.scrollbars.ALWAYS';
    const SCROLLBARS_NEVER  = 'uiGridConstants.scrollbars.NEVER';

    protected $enableSorting = true;
    protected $enableFiltering = true;
    protected $enableColumnMenus = true;
    protected $enableGridMenu = true;
    protected $enableColumnResizing = true;
    protected $enableMinHeightCheck = true;
    protected $enableHorizontalScrollbar = self::SCROLLBARS_ALWAYS;
    protected $enableVerticalScrollbar = self::SCROLLBARS_ALWAYS;

    protected $paginationPageSize = 25;
    protected $paginationPageSizes = [25, 50, 100, 200, 500];

    protected $rowHeight = 40;

    protected $enableBatch = false;

    protected $enableDraggableRows = false;
    protected $draggableRowsOrderField = 'order';

    protected $autoRedirect = false;
    protected $autoRedirectMode = 'raw';
    protected $autoRedirectState = null;
    protected $autoRedirectParams = [];
    protected $autoRedirectOnlyWithout = 'crudform-x-dont-redirect';

    protected $enableRowSelection = false;
    protected $enableSelectAll = false;
    protected $multiSelect = false;
    protected $selectionRowHeaderWidth = 0;

    protected $emptyMessage = false;
    protected $emptyMessageTitle = false;

    /**
     * @var Collection
     */
    protected $batchActions = null;

    /**
     * @var Collection
     */
    protected $actions = null;

    /**
     * @var Collection
     */
    protected $itemActions = null;

    /**
     * @var Collection
     */
    protected $columnDefinitions = null;

    public function __construct(Options $options){
        parent::__construct($options);
        $this->batchActions = new Collection();
        $this->itemActions = new Collection();
        $this->actions = new Collection();
        $this->columnDefinitions = new Collection();

    }

    /**
     * Active le Tri
     * Activé par defaut
     * @param bool $enabled
     * @return $this
     */
    public function enableSorting($enabled = true){
        $this->enableSorting = $enabled;
        return $this;
    }

    /**
     * Active les Filtres
     * Activé par defaut
     * @param bool $enabled
     * @return $this
     */
    public function enableFiltering($enabled = true){
        $this->enableFiltering = $enabled;
        return $this;
    }

    /**
     * Active les Menus sur les colonnes
     * Activé par defaut
     * @param bool $enabled
     * @return $this
     */
    public function enableColumnMenus($enabled = true){
        $this->enableColumnMenus = $enabled;
        return $this;
    }
    /**
     * Active le Menu de grid
     * Activé par defaut
     * @param bool $enabled
     * @return $this
     */
    public function enableGridMenu($enabled = true){
        $this->enableGridMenu = $enabled;
        return $this;
    }

    /**
     * Active ou non la fonctionnalité de redimentionnement des collones
     * Activé par defaut
     * @param bool $enabled
     * @return $this
     */
    public function enableColumnResizing($enabled = true){
        $this->enableColumnResizing = $enabled;
        return $this;
    }

    /**
     * Active la verification de hauteur minimum
     * True by default. When enabled, a newly initialized grid will check to see if it is tall enough to display at least one row of data. If the grid is not tall enough, it will resize the DOM element to display minRowsToShow number of rows.
     * @param bool $enabled
     * @return $this
     */
    public function enableMinHeightCheck($enabled = true){
        $this->enableMinHeightCheck = $enabled;
        return $this;
    }

    /**
     * Active les scrollbars horizontales
     * ALWAYS par defaut
     * @param mixed $mode
     * @return $this
     */
    public function enableHorizontalScrollbar($mode = self::SCROLLBARS_ALWAYS){
        $this->enableHorizontalScrollbar = $mode;
        return $this;
    }

    /**
     * Active les scrollbars verticales
     * ALWAYS par defaut
     * @param mixed $mode
     * @return $this
     */
    public function enableVerticalScrollbar($mode = self::SCROLLBARS_ALWAYS){
        $this->enableVerticalScrollbar = $mode;
        return $this;
    }

    /**
     * Active le drag/drop sur les rows
     * @param mixed $mode
     * @return $this
     */
    public function enableDraggableRows($mode = true,$field = 'order'){
        $this->enableDraggableRows = $mode;
        return $this;
    }

    /**
     * Active la pagination et set le nombre d'elements par page
     * @param int $default nombre par page par defaut
     * @param array $sizes liste d'int nombre par page [25, 50, 100, 200, 500] par defaut
     * @return $this
     */
    public function setPaginationPageSizes($default = 25, $sizes = [25, 50, 100, 200, 500]){
        $this->paginationPageSize = $default;
        $this->paginationPageSizes = $sizes;
        return $this;
    }

    /**
     * Active la pagination pour les grosses listes de données
     * default=>100, sizes=>[25,50,100,200,500,1000,2000,4000,8000,16000]
     * @return $this
     */
    public function setBigPaginationPageSizes(){
        return $this->setPaginationPageSizes(100, [25,50,100,200,500,1000,2000,4000,8000,16000]);
    }

    /**
     * Ajuste la hauteur d'une ligne dans la liste
     * @param int $height
     */
    public function setRowHeight($height = 40){
        $this->rowHeight = $height;
    }


    public function setEmptyMessage($message,$title){
        $this->emptyMessage = $message;
        $this->emptyMessageTitle = $title;
    }


    public function getEmptyMessage(){
        if($this->emptyMessage == false && $this->emptyMessageTitle == false)
            return false;
        return [$this->emptyMessage,$this->emptyMessageTitle];
    }


    /**
     * AUTOREDIRECT
     * @param $state
     * @param array $params
     * @param string $onlyFor
     * @return $this
     *
     */
    public function enableAutoRedirect($state, $params = [], $onlyWithout = 'crudform-x-dont-redirect'){
        $this->autoRedirect = true;

        if(is_object($state)){
            $this->autoRedirectMode = 'crud';
            $this->autoRedirectState = $state->getStateStart().(is_string($params) ?$params:'list');
            $params = $state->getQueryParams();
            $this->autoRedirectParams = ($params == [] ? 'entityId' : current($params));
        }else{
            $this->autoRedirectMode = 'raw';
            $this->autoRedirectState = $state;
            $this->autoRedirectParams = $params;

        }

        $this->autoRedirectOnlyWithout = $onlyWithout;

        return $this;
    }




    /**
     * Active ou non les batch actions
     * @param bool $enabled batchActions activé ou pas
     * @return $this
     */
    public function enableBatchActions($enabled = true){
        $this->enableBatch = true;
        $this->enableRowSelection = $enabled;
        $this->enableSelectAll = $enabled;
        $this->multiSelect = $enabled;
        $this->selectionRowHeaderWidth = $enabled ? 35 : 0;
        return $this;
    }

    /**
     * @param $name
     * @param callable $closure fonction a executé paramètre array d'ids
     * @return $this
     */
    public function addPHPBatchAction($name,\Closure $closure,$label = false, $icon='os-icon-hash-1', $class = false){

        $this->enableBatchActions(true);

        $this->batchActions->put($name,[
            'name'=>$name,
            'label'=>$label ? $label:ucfirst($name),
            'icon'=>$icon,
            'closure'=>$closure,
            'class'=> $class ? $class:'',
            'type'=>'php'
        ]);
        return $this;
    }

    /**
     * @param $name
     * @param callable $closure fonction a executé paramètre array d'ids
     * @return $this
     */
    public function addJSBatchAction($name,$label = false, $icon='os-icon-hash-1', $class = false){

        $this->enableBatchActions(true);

        $this->batchActions->put($name,[
            'name'=>$name,
            'label'=>$label ? $label:ucfirst($name),
            'icon'=>$icon,
            'class'=> $class ? $class:'',
            'type'=>'js'
        ]);
        return $this;
    }

    public function registerBatchRoute($name,$route){
        $batch = $this->batchActions->get($name);
        $batch['route'] = $route;
        $this->batchActions->put($name,$batch);
        return $this;
    }

    /**
     * Ajoute la possibilité de suppression par lot
     * @return $this
     */
    public function addBatchDelete(){
        $this->addPHPBatchAction('delete',function(array $ids){
            $this->getNewEntity()->findMany($ids)->map(function(Eloquent $entity){
                $entity->delete();
            });
        });
        return $this;
    }

    /**
     * Crée une nouvelle colonne
     * @param $name
     * @return ListCrudColumnConfiguration
     */
    public function getNewColumnDefinition($name){
        $column = new ListCrudColumnConfiguration($name,$this->options,$this);
        $this->columnDefinitions->put($name,$column);

        return $column;
    }

    public function addIdColumn($visible_default = false,$field = 'id',$trans = false){
        $col = $this->getNewColumnDefinition($field);

        $col->type(ListCrudColumnConfiguration::TYPE_NUMBER);
        $col->displayName($trans ? trans($trans):trans('skimia.angular::crud.actions.list.id-col'));
        $col->setWidth(50);
        $col->setVisibility($visible_default);
        return $col;
    }

    /**
     * @param $name string nom de l'action
     * @param $label string label
     * @param $state string state
     * @param string array $stateParams params
     * @param string $icon icone
     * @param bool|false $class css class
     * @return $this
     */
    public function addAction($name,$label= false, $state, $stateParams = '{}', $icon=false, $class = false){
        $action = [
            'name'=>$name,
            'label'=>$label ? $label : $name,
            'state'=>[
                'name'=>$state,
                'params'=>$stateParams
            ],
            'icon'=>$icon,
            'class'=> $class ? $class:''
        ];

        $this->actions->put($name,$action);

        return $this;
    }

    /**
     * @param $name string nom de l'action
     * @param $label string label
     * @param $state string state
     * @param string array $stateParams params
     * @param string $icon icone
     * @param bool|false $class css class
     * @return $this
     */
    public function addItemAction($name,$label= false, $state, $stateParams = '{id:row.entity.id}', $icon='os-icon-hash-1', $class = false,$ngShow = false,$ngHide = false){
        $action = [
            'name'=>$name,
            'label'=>$label ? $label : $name,
            'state'=>[
                'name'=>$state,
                'params'=>$stateParams
            ],
            'icon'=>$icon,
            'class'=> $class ? $class:''
        ];

        if($ngHide)
            $action['ngHide'] = $ngHide;

        if($ngShow)
            $action['ngShow'] = $ngShow;

        $this->itemActions->put($name,$action);

        return $this;
    }


    public function getItemActions(){
        return $this->itemActions->toArray();

    }

    public function getActions(){
        return $this->actions->toArray();

    }

    public function getBatchActions(){
        return $this->batchActions->toArray();

    }

    public function canDragRows(){
        return $this->enableDraggableRows;
    }

    public function getDraggableRowsOrderField(){
        return $this->draggableRowsOrderField;
    }

    public function canBatchActions(){
        return $this->enableBatch;
    }


    public function toArray(){
        $arr =  [
            'enableFiltering' => $this->enableFiltering ? 'true':'false',
            'enableSorting' => $this->enableSorting ? 'true':'false',
            'enableGridMenu' => $this->enableGridMenu ? 'true':'false',
            'enableColumnMenus' => $this->enableColumnMenus ? 'true':'false',
            'enableColumnResizing' => $this->enableColumnResizing ? 'true':'false',


            'paginationPageSize' => $this->paginationPageSize,
            'paginationPageSizes' => json_encode($this->paginationPageSizes),

            'rowHeight' => $this->rowHeight,

            'enableRowSelection' => $this->enableRowSelection ? 'true':'false',
            'enableSelectAll' => $this->enableSelectAll ? 'true':'false',
            'multiSelect' => $this->multiSelect ? 'true':'false',
            'dragabbleRows' => $this->enableDraggableRows,
            'selectionRowHeaderWidth' => $this->selectionRowHeaderWidth,

            'columns' => [],

            'autoRedirect'=> $this->autoRedirect
        ];

        if($this->autoRedirect){
            $arr['autoRedirectOptions'] = [
                'mode'=>$this->autoRedirectMode,
                'state'=>$this->autoRedirectState,
                'onlyWithout'=>$this->autoRedirectOnlyWithout,
                'params' =>  $this->autoRedirectParams
            ];
        }

        /**
         * @var ListCrudColumnConfiguration $col
         */
        foreach ($this->columnDefinitions as $name=> $col) {

            $arr['columns'][$name] = $col->toArray();
        }
        $arr['columns'] = $this->options->Fields()->__displayRoutine($arr['columns']);

        return $arr;

    }



    protected $listCrudStateUrl = '';


    public function setStateUrl($url){
        $this->listCrudStateUrl = $url;

        return $this;
    }

    public function getStateUrl(){
        return $this->listCrudStateUrl;
    }
}