<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:45
 */

namespace Skimia\Angular\Form\CRUD\Actions\Lists;


use Illuminate\Support\Collection;
use Skimia\Angular\Form\CRUD\Actions\BaseCRUDConfiguration;
use Eloquent;
use Skimia\Angular\Form\CRUD\Options;

class ListRestConfiguration extends BaseCRUDConfiguration{


    protected $stepping = false;
    protected $steppingFiltersEnabled = false;
    protected $steppingContext = 'entities';
    /**
     * @var Collection
     */
    protected $steppingFilters = null;


    public function __construct(Options $options){
        parent::__construct($options);
        $this->steppingFilters = new Collection();

    }

    public function steppingFilter($column,callable $callable){
        $this->enableSteppingFilters();
        $this->steppingFilters->put($column,$callable);
    }

    public function getSteppingFilters(){
        if(!$this->steppingFiltersEnabled)
            return [];
        return $this->steppingFilters->toArray();
    }

    /**
     * Active les fonctionnalitées de chargement par vague
     * default 100
     * @param int $count le nombre d'elements par vague
     * @return $this
     */
    public function stepping($count = 100){
        $this->stepping = $count;
        return $this;
    }


    /**
     * Désactive les fonctionnalitées de chargement par vague
     * @return $this
     */
    public function disableStepping(){
        $this->stepping = false;
        return $this;
    }

    public function enableSteppingFilters(){
        $this->steppingFiltersEnabled = true;
        return $this;
    }

    public function isSteppingFiltersEnabled(){
        return $this->steppingFiltersEnabled;

    }

    /**
     * @return bool
     */
    public function steppingEnabled(){
        if(is_bool($this->stepping))
            return $this->stepping;
        else
            return $this->stepping > 0;
    }

    /**
     * Retourne le nombre d'objets par étape
     * @return int
     */
    public function getSteppingByStep(){
        if(is_bool($this->stepping))
            return 0;
        else
            return $this->stepping;
    }

    /**
     * Applique le contexte pour le stepping
     * @param  string $context
     * @return $this
     */
    public function setSteppingContext($context){
        $this->steppingContext = $context;
        return $this;
    }

    /**
     * Retourne le contexte de stepping
     * @return string
     */
    public function getSteppingContext(){
        return $this->steppingContext;
    }

}