<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Lists;


use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Options;

/**
 * Class ListCrudActionTrait
 * @package Skimia\Angular\Form\CRUD\Actions\Lists
 * @property ListRestConfiguration $listRestConfiguration test
 */
trait ListCrudActionTrait {

    use ListRestActionTrait;
    /**
     * @var ListCrudConfiguration
     */
    protected $listConfiguration = null;



    protected function configureListCrudActionTrait(Options $options){
        $this->listConfiguration = new ListCrudConfiguration($options);
    }

    protected function configureActionsListCrudActionTrait(ActionOptionsInterface $options){
        $options->ActionTemplate(static::$LIST_REST_ACTION)->bindBlade('skimia.angular::form.crud.actions.list',[

                ]);

    }

    /**
     * @return ListCrudConfiguration
     */
    public function getListConfiguration(){
        return $this->listConfiguration;
    }


    protected function registerListCrudActionTrait(){

        $form = $this;

        $this->makeState('list',function($params) use($form){

            if(!$this->options->ActionAccess(static::$LIST_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }

            $nparams = [
                'form'=>$this,
                'options'=>$this->options,
                'item_actions'=>$this->listConfiguration->getItemActions(),
                'actions'=>$this->listConfiguration->getActions(),
                'can_batch'=>$this->listConfiguration->canBatchActions(),
                'batch_actions'=>$this->listConfiguration->getBatchActions(),
                'config' =>$this->listConfiguration->toArray(),
                'icon'  => $this->options->ActionTemplate(static::$LIST_REST_ACTION)->getIcon() ,
                'title' => $this->options->ActionTemplate(static::$LIST_REST_ACTION)->getTitle() ,
                'empty_message' => $this->listConfiguration->getEmptyMessage() ,
                'filter_enabled' => $this->listRestConfiguration->isSteppingFiltersEnabled(),
                'srv_filters'=> array_keys($this->listRestConfiguration->getSteppingFilters())

            ];

            if($this->canAction('delete'))
            {
                $this->getQueryParams();
                $pp = [];
                foreach ($this->getQueryParams() as $p) {
                    $pp[$p] = ':'.$p;
                }

                $nparams['delete_url'] = route($this->getRouteName(self::$DELETE_REST_ACTION),array_merge($pp,[
                    'entityId' => ':id'
                ]));
            }


            return $this->options->ActionTemplate(static::$LIST_REST_ACTION)->render($params,$nparams);

        },true,true,$this->listConfiguration->getStateUrl());



        if($this->listConfiguration->canBatchActions()){

            foreach($this->listConfiguration->getBatchActions() as $action => $batch){

                $method = 'batch'.ucfirst($action);
                $route = $this->getRouteStart() .'batch'.ucfirst($action);
                if($batch['type'] == 'php' && (method_exists($this,$method) || isset($batch['closure']))) {

                    \Route::post($this->getUrlStart() . 'batch' . ucfirst($action), ['uses' => function () use ($method,$batch) {
                        $this->app->isSecure();
                        if (\Input::has('ids_batch')) {
                            if(method_exists($this,$method))
                                return $this->$method(\Input::get('ids_batch'));
                            else
                                return $batch['closure'](\Input::get('ids_batch'));
                        }
                    }, 'as' => $this->getRouteStart() . 'batch' . ucfirst($action)]);

                    $this->listConfiguration->registerBatchRoute($action,$route);
                }else if ($batch['type'] == 'js'){

                }
                else
                    throw new \Exception('l\'action de masse '.$action.' a besoin de la methode '.'batch'.ucfirst($action).' dans le formulaire');
            }
        }

        if($this->listConfiguration->canDragRows()){

            $field = $this->listConfiguration->getDraggableRowsOrderField();

            \Route::post($this->getUrlStart() . 'reorder', ['uses' => function () use ($field) {
                $this->app->isSecure();
                $orders = \Input::get('orders');
                $entities = $this->getNewEntity()->whereIn('id',array_keys($orders))->get(['id',$field]);
                $dirty = false;
                foreach ($entities as $entity) {
                    if($entity->$field != $orders[$entity->id]){
                        $entity->$field = $orders[$entity->id];
                        $entity->save();
                        $dirty = true;
                    }




                }
                if($dirty)
                    \AResponse::addMessage('Nouvel ordre des éléments enrigistré');

                return \AResponse::r();
            }, 'as' => $this->getRouteStart() . 'reorder']);
        }


    }
}