<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 20/06/2015
 * Time: 18:35
 */

namespace Skimia\Angular\Form\CRUD\Actions\Lists;

use AResponse;
use Illuminate\Database\Eloquent\Collection;
use Skimia\Angular\Form\CRUD\Options;

trait ListRestActionTrait {

    public static $LIST_REST_ACTION = 'list';
    /**
     * @var ListRestConfiguration
     */
    protected $listRestConfiguration = null;
    protected $listRestRoute = null;

    protected function configureListRestActionTrait(Options $options){
        $this->listRestRoute = $this->getRouteName(static::$LIST_REST_ACTION);
        $this->listRestConfiguration = new ListRestConfiguration($options);


        static::registerOverrideVarEvent('rest-list-query',function($query){
            return $query;
        });

        /**
         * OVERRIDE FOR LIST FORM EVENTS
         */
        static::registerOverrideVarEvent('rest-list',function($collection){
            //$array_col = $collection->toArray();
            $fields = $this->options->ActionFields(self::$LIST_REST_ACTION)->toFieldsArray();
            $list = [];
            foreach ($collection as &$entity) {

                $listEntity = $entity->toArray();
                foreach ($fields as $k=>$field) {

                    $listEntity[$k] =  \ShowHelper::TransformDataToView($field['type'],[&$entity,$this->getKey($entity,$k),$k,$field,&$this],true);
                }
                $listEntity['id'] = $this->getKey($entity,'id');
                $list[] = $listEntity;

            }

            return new Collection($list);
        });
    }

    /**
     * @return ListRestConfiguration
     */
    public function getListRestConfiguration(){
        return $this->listRestConfiguration;
    }

    protected function registerActionListRestActionTrait(){
        $this->registerAction(static::$LIST_REST_ACTION);
    }

    protected function registerListRestActionTrait(){

        $this->listRestRoute = $this->makeRoute(static::$LIST_REST_ACTION,function() {

            if(!$this->options->ActionAccess(static::$LIST_REST_ACTION)->actionCan()){
                return 'Unauthorized';
            }
            $query = $this->fireOverrideVars('rest-list-query',$this->getQuery(),$this->getQueryRouteParams());



            if($this->listRestConfiguration->steppingEnabled()){
                $e_by_step = $this->listRestConfiguration->getSteppingByStep();
                $step_context = $this->listRestConfiguration->getSteppingContext();

                if(($context = AResponse::getBigDataContext()) === false){
                    //si le contexte est faux c'est qu'il n'est pas initialisé

                    $filters = \Input::get('big_data_filters',false);

                    if($this->listRestConfiguration->isSteppingFiltersEnabled() && $filters !== false){
                        $query = $this->modifyQuery(json_decode($filters,true),$query);

                    }
                    $stepping = AResponse::getBigDataStep($step_context,$query->count(),$e_by_step);

                }elseif($context == $step_context){
                    //il est initialisé nouvelle vague
                    $stepping = AResponse::getBigDataStep($step_context,false,$e_by_step);

                    if($this->listRestConfiguration->isSteppingFiltersEnabled() && $stepping->filters !== false){
                        $query = $this->modifyQuery($stepping->filters,$query);
                    }
                }


                $entities = $this->fireOverrideVars('rest-list',$query->skip($stepping->skip)->take($stepping->take)->get());

                if($entities->count() == 0)
                $stepping->isLastStep = true;
                AResponse::setBigData($stepping,$entities);
                return AResponse::r($this->fireOverrideVars('rest-list-response',[]));
            }else{



                $entities = $this->fireOverrideVars('rest-list',$query->get());

                return AResponse::r($this->fireOverrideVars('rest-list-response',[
                    'entities'=>$entities->toArray()
                ]));
            }

        });

    }

    protected function modifyQuery($filters, $query){
        if($this->listRestConfiguration->steppingEnabled()){
            $callFilters = $this->listRestConfiguration->getSteppingFilters();
            foreach ($callFilters as $column=>$filter) {
                if(isset($filters[$column]))
                    $query = $filter($filters[$column],$query);
            }
        }
        return $query;
    }

    /**
     * Register a transform list event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function transformListEvent($callback)
    {
        static::registerCRUDFormEvent('transform.list', $callback, 100);
    }
}
