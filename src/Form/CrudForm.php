<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 24/01/2015
 * Time: 13:53
 */

namespace Skimia\Angular\Form;


use Illuminate\Support\Collection;
use Skimia\Angular\Managers\Application;
use Skimia\Form\Base\EntityForm;
use Skimia\Angular\Form\AngularTrait;
abstract class CrudForm extends EntityForm{

    use AngularTrait;
    protected $name = '';

    /**
     * @return boolean
     */
    public function hasCreate()
    {
        return $this->create;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    protected $list = false;

    /**
     * @return boolean
     */
    public function hasList()
    {
        return $this->list;
    }

    /**
     * @return boolean
     */
    public function hasEdit()
    {
        return $this->edit;
    }

    /**
     * @return boolean
     */
    public function hasGet()
    {
        return $this->get;
    }

    /**
     * @return boolean
     */
    public function hasDuplicate()
    {
        return $this->duplicate;
    }

    /**
     * @return boolean
     */
    public function hasDelete()
    {
        return $this->delete;
    }
    protected $listBigData = false;
    protected $listRoute = false;
    protected $listFields = false;

    protected $edit = false;
    protected $editRoute = false;

    protected $create = false;
    protected $createRoute = false;

    protected $readOnlyFields = [];
    protected $dateFields = true;

    protected $get = false;
    protected $getRoute = false;
    protected $getFields = false;

    protected $duplicate = false;
    protected $duplicateRoute = false;

    protected $delete = false;
    protected $deleteRoute = false;

    protected $listTemplate = 'skimia.angular::form.crud.list';
    protected $getTemplate = 'skimia.angular::form.crud.get';
    protected $editTemplate = false;
    protected $createTemplate = false;

    protected $template = 'skimia.angular::form.crud.form';

    protected $saveSuccessMessage = "Mise a jour Terminée";
    protected $createSuccessMessage = "Création Terminée";

    protected $app = null;
    protected $parent_state = false;
    /**
     * @return Eloquent
     */
    protected abstract function getNewEntity();



    public function __construct($eloquent = false){

        if($eloquent === false)
            parent::__construct($this->getNewEntity());
        else
            parent::__construct($eloquent);

    }

    protected function prepareForm($form){
        return $form;
    }

    protected function prepareEntity($entity){
        return $entity;
    }

    protected function prepareEntities($entities){
        return $entities;
    }

    protected function prepareEdit($form){

        return $this->prepareForm($form);
    }
    protected function prepareNew($form){

        return $this->prepareForm($form);
    }
    protected function prepareCreate($form){

        return $this->prepareForm($form);
    }
    protected function prepareList($entities){

        return $this->prepareEntities($entities);
    }
    protected function prepareGet($entity){

        return $this->prepareEntity($entity);
    }
    protected function prepareGetEdit($entity){

        return $this->prepareEntity($entity);
    }


    public static function register(Application $app, $state_parent = false){
        $class = new static();
        $class->_register($app,$state_parent);

        return $class;
    }

    public function _register(Application $app, $state_parent = false){
        $this->app = $app;
        $this->parent_state = $state_parent;

        $this->register_list();
        $this->register_batch();
        $this->register_create();
        $this->register_get();
        $this->register_delete();
        $this->register_edit();
        $this->register_template();



    }


    protected function register_list(){

        $closure = function(){
            $this->app->isSecure();
            $params = \Route::current()->parameters();
            if($this->listBigData !== false) {
                $e_by_step = is_integer($this->listBigData) ? $this->listBigData :100;
                $context = \AResponse::getBigDataContext();

                if ($context === false) {
                    $bigData = \AResponse::getBigDataStep('entities', call_user_func_array([$this, 'getListQuery'], [$params])->count(), $e_by_step);

                    $entities = call_user_func_array([$this, 'getListQuery'], [$params])->skip($bigData->skip)->take($bigData->take)->get();
                    \AResponse::setBigData($bigData,$this->transformList($entities));

                    //$fields = array_keys($this->getArrayableFields());
                    return \AResponse::r([]);

                } elseif ($context == 'entities') {
                    $bigData = \AResponse::getBigDataStep('entities', '', $e_by_step);
                    $entities = call_user_func_array([$this, 'getListQuery'], [$params])->skip($bigData->skip)->take($bigData->take)->get();
                    \AResponse::setBigData($bigData, $this->transformList($entities));
                    return \AResponse::r([]);
                }
            }else{
                return \AResponse::r(['entities'=>$this->transformList(call_user_func_array([$this, 'getListQuery'], [$params])->get())]);
            }





        };
        if($this->list){
            $form = $this;
            $this->app->addState($this->getStateStart().'list','/'.$this->name.$this->getParamsToUri(),
                function($params) use($form){
                    $this->app->isSecure();
                    $fields = $this->getArrayableFields();

                    if($this->listFields !== false){
                        $fields = $this->listFields;
                    }
                    $is_batch = count($this->batchActions) > 0 && $this->allowBatch;
                    return \View::make($form->listTemplate,['form'=>$form,
                        'fields'=>$fields,
                        'actions'=>$this->listActions,
                        'item_actions'=>$this->listItemActions,
                        'is_batch'=>$is_batch,
                        'batch' =>$this->batchActions],$params)->render();
                },
                $closure
            );
        }

        else if($this->listRoute){

            \Route::get($this->getUrlStart() .'list'.$this->getParamsToUri() ,[
                'as'=> $this->getRouteStart().'list',
                'uses'=> $closure

            ]);

        }
    }

    protected function register_get(){
        $closure = function($entityId){
            $this->app->isSecure();
            //$fields = array_keys($this->getArrayableFields());
            return \AResponse::r(['entity'=>$this->prepareGet(call_user_func_array([$this, 'getQuery'], [\Route::current()->parameters()])->where('id',$entityId)->first())]);

        };

        if($this->get){
            $form = $this;
            $this->app->addState($this->getStateStart().'get','/'.$this->name.'/{entityId}',
                function($params) use($form){

                    $this->app->isSecure();

                    $fields = $this->getArrayableFields();

                    if($this->getFields !== false){
                        $fields = $this->getFields;
                    }
                    $tpl = \View::make($form->getTemplate,['form'=>$form,'fields'=>$fields],$params)->render();


                    return $tpl;
                },

                $closure
            );
        }

        else if($this->getRoute){

            \Route::get($this->getUrlStart() .'get/{entityId}' ,[
                'as'=> $this->getRouteStart().'get',
                'uses'=>
                    $closure
            ]);

        }
    }

    protected function register_create(){
        $closure = function(){
            $this->app->isSecure();
            $this->data = $this->getNewEntity();


            return \AResponse::r($this->prepareNew(['form'=>$this->getAllOldData()]));
        };

        if($this->create){
            $form = $this;
            $this->app->addState($this->getStateStart().'create','/'.$this->name.rtrim($this->getParamsToUri(),'/').'/create',
                function($params) use($form){

                    $this->app->isSecure();

                    $tpl = $this->make(false,array_merge(['angular_submit'=>'save','state'=>'create','urls'=>$this->getUrls()],$params))->render();

                    if($this->createTemplate === false){
                        return $tpl;
                    }



                    return \View::make($form->createTemplate,['fields'=>$this->fields,'form'=>$form,'template'=>$tpl],$params)->render();

                },
                [
                    'as'=> $this->getRouteStart().'new',
                    'uses'=>$closure,
                    'route_url' => $this->getUrlStart() .'new',

                ]
            );
        }

        if($this->create || $this->createRoute){

            \Route::post($this->getUrlStart() .'create'.$this->getParamsToUri() ,[
                'as' => $this->getRouteStart().'create',
                'uses' => function(){
                    $this->app->isSecure();

                    $this->data = $this->getNewEntity();
                    if($this->isValid()){
                        $entity = $this->makeEntity();

                        $entity = $this->beforeCreateSave($entity);

                        $entity->save();
                        $this->afterCreateSave($entity);
                        \AResponse::addMessage($this->createSuccessMessage);
                        return \AResponse::r($this->data->toArray());
                    }

                    $form = $this->data->toArray();
                    foreach($this->fields as $key=>$field){
                        if(!isset($form[$key]))
                            $form[$key] = $this->getOldData($key);
                    }
                    return \AResponse::r($this->prepareCreate(['form'=>$form]));
                }
            ]);

            if($this->createRoute){
                \Route::get($this->getUrlStart() .'new' ,[
                    'as'=> $this->getRouteStart().'new',
                    'uses'=> $closure

                ]);
            }


        }
    }

    protected function beforeCreateSave($entity){return $this->beforeSave($entity);}
    protected function afterCreateSave($entity){return $this->afterSave($entity);}
    protected function beforeSave($entity){return $entity;}
    protected function afterSave($entity){return $entity;}
    protected function register_delete(){
        if($this->delete){

        }

        if($this->delete || $this->deleteRoute){

            \Route::delete($this->getUrlStart() .'delete/{entityId}' ,[
                'as'=> $this->getRouteStart().'delete',
                'uses'=>
                    function($entityId){
                        $this->app->isSecure();
                        call_user_func_array([$this, 'getQuery'], [\Route::current()->parameters()])->where('id',$entityId)->first()->delete();
                        \AResponse::addMessage('Donnée bien supprimée','danger');
                        return \AResponse::r([]);

                    }
            ]);

        }
    }

    protected function register_edit(){
        $closure = function($entityId){
            $this->app->isSecure();
            $this->data = call_user_func_array([$this, 'getQuery'], [\Route::current()->parameters()])->where('id',$entityId)->first();

            return \AResponse::r($this->prepareGetEdit(['form'=>$this->getAllOldData()]));
        };


        if($this->edit){
            $form = $this;
            $this->app->addState($this->getStateStart().'edit','/'.$this->name.'/{entityId}/edit',
                function($params) use($form){

                    $this->app->isSecure();
                    $tpl = $this->make(false,array_merge(['angular_submit'=>'save','state'=>'edit','urls'=>$this->getUrls()],$params))->render();

                    if($this->editTemplate === false){
                        return $tpl;
                    }



                    return \View::make($form->editTemplate,['form'=>$form,'template'=>$tpl],$params)->render();

                },
                [
                    'as'=> $this->getRouteStart().'get_edit',
                    'uses'=>$closure,
                    'route_url' => $this->getUrlStart() .'get_edit/{entityId}',

                ]
            );
        }

        if($this->edit || $this->editRoute){

            \Route::post($this->getUrlStart() .'edit/{entityId}' ,[
                'as' => $this->getRouteStart().'edit',
                'uses' => function($entityId){

                    $this->app->isSecure();
                    $this->data = $this->getNewEntity()->where('id',$entityId)->first();
                    if($this->isValid()){
                        $entity = $this->makeEntity();
                        $entity = $this->beforeSave($entity);
                        $entity->save();
                        $this->afterSave($entity);
                        \AResponse::addMessage($this->saveSuccessMessage);
                        return \AResponse::r($this->data->toArray());
                    }

                    $form = $this->data->toArray();
                    foreach($this->fields as $key=>$field){
                        if(!isset($form[$key]))
                            $form[$key] = $this->getOldData($key);
                    }
                    return \AResponse::r($this->prepareEdit(['form'=>$form]));
                }
            ]);

            if($this->editRoute){
                \Route::get($this->getUrlStart() .'get_edit/{entityId}' ,[
                    'as'=> $this->getRouteStart().'get_edit',
                    'uses'=>$closure

                ]);
            }

        }
    }

    protected function register_template(){
        $template = function(){
            $this->app->isSecure();
            return $this->make(false,[ '__app'=>$this->app->getName(), 'angular_submit'=>'save','urls'=>$this->getUrls()])->render();
        };

        \Route::get($this->getUrlStart() .'form' ,[
            'as'=>$this->getRouteStart().'form',
            'uses'=>$template
        ]);
        $this->app->JsModule->registerBeforeGenerate($template);
    }

    protected function register_batch(){
        if($this->allowBatch){

            foreach($this->batchActions as $action => $batch){

                $method = 'batch'.ucfirst($action);
                $route = $this->getRouteStart() .'batch'.ucfirst($action);
                if($batch['type'] == 'php' && method_exists($this,$method)) {

                    \Route::post($this->getUrlStart() . 'batch' . ucfirst($action), ['uses' => function () use ($method) {
                        $this->app->isSecure();
                        if (\Input::has('ids_batch')) {
                            return $this->$method(\Input::get('ids_batch'));
                        }
                    }, 'as' => $this->getRouteStart() . 'batch' . ucfirst($action)]);

                    $this->batchActions[$action]['route'] = $route;
                }else if ($batch['type'] == 'js'){

                }
                else
                    throw new \Exception('l\'action de masse '.$action.' a besoin de la methode '.'batch'.ucfirst($action).' dans le formulaire');
            }
        }
    }

    public function getUrls(){
        $route_start = $this->getRouteStart();

        $urls = [];

        if($this->list || $this->listRoute){
            $urls['list'] = route($route_start . 'list');
        }

        if($this->get || $this->getRoute){
            $urls['get'] = route($route_start . 'get',['entityId'=>':id']);
        }

        if($this->create || $this->createRoute){
            $urls['create'] = route($route_start . 'create');
            $urls['new'] = route($route_start . 'new');
        }

        if($this->delete || $this->deleteRoute){
            $urls['delete'] = route($route_start . 'delete',['entityId'=>':id']);
        }

        if($this->edit || $this->editRoute){
            $urls['edit'] = route($route_start . 'edit',['entityId'=>':id']);
            $urls['get_edit'] = route($route_start . 'get_edit',['entityId'=>':id']);
        }

        $urls['form'] = route($route_start . 'form');

        return $urls;
    }

    protected function getRouteStart(){
        return 'angular.' . $this->app->getName() . $this->dotParrentState() . $this->name . '*';
    }

    protected function getUrlStart(){
        return 'angular/state/' . $this->app->getName() .'/'.ltrim($this->dotParrentState(),'.').$this->name.'/';
    }

    public function getStateStart(){
        return ($this->parent_state ? $this->parent_state.'.':'').$this->name.'*';
    }

    protected function dotParrentState(){
        return $this->parent_state ? '.'.$this->parent_state.'.':'.';
    }



    protected function getArrayableFields()
    {

        $this->makeFieldsIfNotMaked();

        $entity = $this->getNewEntity();

        $p = new \ReflectionProperty('Illuminate\Database\Eloquent\Model','visible');
        $p->setAccessible(true);
        $visible = $p->getValue($entity);


        if (count($visible) > 0)
        {
            return array_intersect_key($this->allFields, array_flip($visible));
        }

        $a = array_diff_key($this->allFields, array_flip($entity->getHidden()));
        if(!$this->dateFields)
            $a = array_merge(array_diff_key($a,array_flip(['created_at','deleted_at','updated_at'])),$this->fields->toArray());
        else
            $a = array_merge($a,$this->fields->toArray());

        array_walk($a,function(&$item,$key){
            if(!isset($item['label'])){
                $item['label'] = $key;
            }
        });

        return $a;
    }


    protected $allFields = [];

    protected function makeFields(){
        $schema = \Schema::getColumnListing($this->data->getTable());
        $supp = array_merge(array_flip($this->hiddenFields), array_flip($this->data->getGuarded()));

        if(is_array($this->fields))
            $this->fields = new Collection($this->fields);
        foreach($schema as $field){
            $this->allFields[$field] = ['type'=>'text','label'=>trans($this->langKey.'.'.$field.'.label')];

            if(!$this->fields->has($field) && !isset($supp[$field]))
                $this->add($field,'text',trans($this->langKey.'.'.$field.'.label'),trans($this->langKey.'.'.$field.'.info'));
        }

        $this->fieldsMaked = true;

    }






    protected function getParamsToUri(){
        if(count($this->listQueryParams) > 0)
            return '/p/{'.implode('}/{',$this->listQueryParams).'}/';
        else
            return '';
    }


    protected $listQueryParams = [];

    protected function getListQuery($params = []){
        return $this->getQuery($params);
    }

    protected function getQuery($params = []){
        return $this->getNewEntity()->newQuery();
    }


    protected $listItemActions = [];

    protected $listActions = [];

    protected $allowBatch = false;

    protected $batchActions = [
        'delete'=> [
            'label' => 'Suppression Multiple',
            'type' => 'php',//appel a batchDelete OU js appel a batchDelete(dans le javascript)
        ]
    ];

    protected function batchDelete($ids){
        $this->getNewEntity()->whereIn('id',$ids)->delete();
        \AResponse::addMessage('Données bien supprimées','danger');
        return \AResponse::r([]);
    }

    protected function transformList($entities){
        $fields = $this->getArrayableFields();

        if($this->listFields !== false){
            $fields = $this->listFields;
        }
        $list = [];

        foreach ($entities as &$entity) {
            $listEntity = [];
            foreach ($fields as $k=>$field) {

                $listEntity[$k] =  \ShowHelper::TransformDataToView($field['type'],[&$entity,$this->getKey($entity,$k),$k,$field,&$this],true);
            }
            $listEntity['id'] = $this->getKey($entity,'id');
            $list[] = $listEntity;
        }

        return $list;

    }
    protected function getKey($e,$key){
        if(is_object($e) && $e instanceof \stdClass)
            return $e->$key;
        else
            return $e[$key];
    }
}