<?php

namespace Skimia\Angular\Form;

use Config;

trait AngularTrait{

    protected $submit;

    public function getTemplate(){
        if($this->template === false){
            $this->template = Config::get('skimia.angular::templates.form');
        }
        return parent::getTemplate();

    }

    public static function render($submit = '' ,$template = false){
        $instance = static::getNew();



        $template = $instance->make($template,['angular_submit'=>$submit])->render();

        return $template;
    }

    protected static function getNew(){
        return new static();
    }

    protected function getAllOldData(){
        $this->makeFieldsIfNotMaked();
        $data = $this->getData();
        foreach($this->fields as $key=>$field){
            if(!isset($data[$key])|| empty($data[$key]))
                $data[$key] = $this->getOldData($key);

            $this->mergeDataForField($data,$data[$key],$key,$field);
        }
        return $data;
    }

    protected function mergeDataForField(&$data,&$value,$key,$field){

        if(isset($field['eToForm']) && is_callable($field['eToForm'])){

            $field['eToForm']($data,$value,$key,$field);

        }

        if(!isset($data['__deleting']))
            $data['__deleting'] = [];
        if(\AngularFormHelper::hasDataViewTransformer($field['type'])){

            \AngularFormHelper::TransformDataToView($field['type'],[&$data,&$value,$key,$field,&$this],true);
        }
        return;
    }

    public function _getRawOldData($key){
        return $this->getRawOldData($key);
    }
}