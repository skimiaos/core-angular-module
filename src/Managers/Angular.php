<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 08:22
 */

namespace Skimia\Angular\Managers;

use Illuminate\Support\Collection;
use Route;
use Skimia\Angular\Generators\AngularAppHtml;
use Skimia\Angular\Generators\AngularTplHtml;
use View;
use Skimia\Angular\Facades\State as StatesFacade;
use Closure;
use Blade;

class Angular {

    /**
     * @var array|Collection
     */
    protected $applications = [];
    protected $current_application;

    public function __construct(){
        $this->applications = new Collection($this->applications);

        return $this;
    }

    /**
     * Instancie une nouvelle application et la garde en memoire
     * @param $name nom unique de l'application angular (angular.module($name))
     * @param bool $url si omis l'application ne sera pas bind au router(see bind)
     * @param bool $controller deuxieme paramètre de la fonction bind(see bind)
     * @param array $opts Array d'options
     * @return Application
     */
    public function application($name, $url = false, $controller = false, $opts=[]){
        if($url = false && $controller = false && $opts = [] && $this->applications->has($name))
            return $this->get($name);
        //create
        $app = new Application($name,$url,$controller,$opts);
        //store
        $this->applications->put($name,$app);
        //return
        return $app;
    }

    /**
     * @param $name Application name
     * @return Application app
     * @throws \Exception si l'app n'existe pas
     */
    public function get($name){
        if($this->applications->has($name))
            return $this->applications->get($name);
        else
            throw new \Exception('Application angular ['.$name.'] n\' existe pas');
    }

    /**
     *
     * @param $name
     * @return $this
     * @throws \Exception
     */
    public function setCurrentApp($name){
        if(!$this->applications->has($name))
            throw new \Exception('Application angular ['.$name.'] n\' existe pas');

        $this->current_application = $this->applications->get($name);

        return $this->current_application;

    }

    /*
    public function Application($name, $controller, $url = false, $opts=[]){
        $url = $url?$url:'/angular/'.$name;
        $this->applications[$name] = new Collection([
            'name'=>$name,
            'fron_url'=> $url,
            'boot' => $controller,
            'options' => $opts,
            'states' => new Collection()
        ]);

        if(!is_a($controller ,'Closure')) {
            Route::get($url, [
                'as' => 'application.' . $name,
                'before'=>'angular.app:'.$name,
                'uses' => $controller
            ]);
        }else
        {
            Route::get($url, [
                'as' => 'application.' . $name,
                'before'=>'angular.app:'.$name,
                $controller
            ]);
        }
        return StatesFacade::app($name);

    }

    public function addState($app, $name, Collection $state){

        $this->applications[$app]['states']->put($name,$state);
        return $this;
    }


    public function getApplicationData($name = false){
        if(!$name){
            return $this->current_application;
        }else
        {
            if(!$this->applications->has($name))
                throw new \Exception('Application angular ['.$name.'] n\' existe pas');

            return $this->applications->get($name);
        }
    }

    public function front($view, $data = array(), $mergeData = array()){

        start_measure('render_front','Generation de la front app');
        $generator = new AngularAppHtml(View::make($view, $data, $mergeData),$this->getApplicationData());

        $generated_content =  $generator->generate();
        stop_measure('render_front');
        return $generated_content;
    }

    protected $state = '';
    public function setState($name){
        $this->state = $name;
    }
    public function getState($application= false, $state = false){
        if(!$application){
            $application = $this->current_application['name'];
        }
        if(!$state){
            $state = $this->state;
        }
        return $this->applications[$application]['states'][$state];
    }
    public function returnView(){
        return isset($this->current_application);
    }
    public function make($view, $data = array(), $mergeData = array()){

        $generator = new AngularTplHtml(View::make($view, $data, $mergeData),$this->getApplicationData(), $this->state);

        Blade::setEscapedContentTags('[[[', ']]]');
        Blade::setContentTags('[[', ']]');
        return $generator->generate();
    }


    */

} 