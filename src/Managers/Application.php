<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 03/01/2015
 * Time: 12:26
 */

namespace Skimia\Angular\Managers;


use Illuminate\Support\Collection;
use Skimia\Angular\Generators\States;
use Skimia\Auth\Traits\Acl;
use Skimia\Modules\Modules;
use Skimia\Angular\Generators\FrontHtml;
use Skimia\Angular\Generators\JsModule;
use Route;
class Application {

    use Acl;
    public $generating = false;
    protected $name;
    protected $url;
    protected $options;
    protected $acl;
    protected $aclClosure;

    public $FrontHtml;
    public $JsModule;
    public $States;


    /**
     * @return Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return nom
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Représente une application angularJs
     * @param $name nom unique de l'application angular (angular.module($name))
     * @param string $url si omis l'application ne sera pas bind au router(see bind)
     * @param mixed $controller deuxieme paramètre de la fonction bind(see bind)
     * @param array $opts Array d'options
     */
    public function __construct($name, $url='', $controller = false, $opts=[]){

        $this->name = $name;
        $this->options = new Collection($opts);

        // si l'url est definie on bind l'application sur une url
        //Dans le cas d'une Application Globale
        if(!empty($url))
            $this->bind($url, $controller);

        $this->FrontHtml = new FrontHtml($this);
        $this->JsModule = new JsModule($this);
        $this->States = new States($this);

        $this->addDependency('MessageCenterModule', module_assets('skimia/angular','/js/flash.js'));
        $this->addDependency('ui.router', module_assets('skimia/angular','/js/angular-ui-router.js'));
        $this->addDependency('ngAnimate');
        $this->addDependency('dataSource');
        $this->addDependency('angular-loading-bar', module_assets('skimia/angular','/js/angular-loading-bar.js'));
        $this->addDependency('multi-select', module_assets('skimia/angular','/js/angular-multi-select.js'));
        $this->addDependency('oc.modal', module_assets('skimia/angular','/js/angular-oc-modal.js'));

        $this->addScriptAfter(module_assets('skimia/angular','/js/filter.order_object_by.js'));
        $this->addScriptAfter(module_assets('skimia/angular','/js/treeSelectController.js'));
    }

    /**
     * Bind au router la page pour charger l'application
     * @param $url url de la page front de l'appication
     * @param bool $controller si la variable contient @(appel controller texte) ou est une closure elle sera executée sinon c'est le nom de la vue d'initialisation
     * @return Application
     */
    public function bind($url, $controller){

        $this->url = $url;
        // Si c'est une action Controller@Action on bind l'action a l'url
        if(is_string($controller) && str_contains($controller,'@')){
            Route::get($url, [
                'as' => 'angular.application.' . $this->name,
                'before'=>'angular.app:' . $this->name,
                'uses'=>$controller
            ]);
        }
        //Si c'est une Closure pareil
        elseif(is_a($controller ,'Closure')) {
            Route::get($url, [
                    'as' => 'angular.application.' . $this->name,
                    'before'=>'angular.app:' . $this->name
                , $controller
            ]);
        }
        //du coup ça doit être une vue on link une closure qui générera la vue pour l'application
        elseif(is_string($controller))
        {
            Route::get($url, [
                'as' => 'angular.application.' . $this->name,
                'before'=>'angular.app:' . $this->name,
                function()use($controller){
                    return $this->renderHtml($controller);
                }
            ]);
        }else
            // $controller n'est pas annalysible du coup ERREUR:!
            $this->throwException('Impossible de bind l\'application au router: Aucune "Action" ou vue definie');

        return $this;

    }

    /**
     * Ajoute une dépendance a l'application
     * @param $name dépendance angular a ajouter
     * @param bool $path si un fichier est necessaire l'ajouter ici(see addScript)
     * @return Application
     */
    public function addDependency($name, $path=false){

        $this->JsModule->addDependency($name,$path);
        return $this;
    }

    public function acl($rule, $closure){
        $this->acl = $rule;
        $this->aclClosure = $closure;
        return $this;
    }

    public function addTemplateScript($script){
        $this->FrontHtml->addJavascript($script);
    }
    /**
     * Ajoute un script a ajouter a l'application (fichier js généré ajouté avant angular.module)
     * @param string $path si un fichier est necessaire l'ajouter ici
     * @return Application
     */
    public function addScript($path){

        $this->JsModule->addScript($path);
        return $this;
    }

    public function addScriptAfter($path){

        $this->JsModule->addScriptAfter($path);

        return $this;
    }

    public function addDependencyForScript($fileName,$path){
        Route::get('/angular/application/'.$fileName,function()use($path){
            return file_get_contents($path);
        });
    }

    public function addState($name,$url,$template,$opt=[],$sub_opts=[]){

        $this->States->add($name,$url,$template,$opt,$sub_opts);

    }
    /**
     * @see View::make($view,$data,$mergeData
     * @param $view nom de la vue
     * @param array $data
     * @param array $mergeData
     * @return html page front de l'application
     */
    public function renderHtml($view, $data = array(), $mergeData = array()){

        $this->generating = 'html';
        $this->isSecure('js-app');
        //mesures pour l'optimisation
        start_measure('render_front','Generation de la front app');
        //instantiation du générateur de "Front Html" ( ng-app ng-controller et ajouts des dépendances javascripts comme angular route ui-router et le js généré pour l'application
        $this->FrontHtml->setView(\View::make($view, array_merge(['__app'=>$this->name],$data), $mergeData));
        //GENERATE!!!
        $generated_content =  $this->FrontHtml->generate();
        stop_measure('render_front');
        $this->generating = false;
        return $generated_content;
    }

    public function renderHtmlContent($content,$scripts_paths =[],$css_paths = []){
        $this->generating = 'html';
        $this->isSecure('js-app');
        //mesures pour l'optimisation
        start_measure('render_front','Generation de la front app');

        //GENERATE!!!
        $generated_content =  $this->FrontHtml->generate($content,$scripts_paths,$css_paths);
        stop_measure('render_front');
        $this->generating = false;
        return $generated_content;
    }

    /**
     * @return string javascript compilé de l'application
     */
    public function renderJs(){
        $this->generating = 'js';
        $js =  $this->JsModule->generate();
        $this->generating = false;

        return $js;
    }

    public function throwException($message){
        throw new \Exception('Application Angular['.$this->name.'] : '.$message);
    }

    public function toArray(){
        return [
            'name'=>$this->name,
            'url'=>$this->url,
            'options'=>$this->options->all()
        ];
    }

    public function can(){
        try{
            if($this->acl !== null && !$this->getAcl()->can($this->acl))
            {
                return false;
            }
        }catch(\Exception $e){
            return false;

        }
        return true;
    }
    public function isSecure($context = 'undefined'){

        if(!$this->can())
        {
            $closure = $this->aclClosure;
            $closure($context);
            \App::abort(403, 'Unauthorized action.');
        }
        return true;
    }

    public function getUrl(){
        return $this->url;
    }



    public function provideDefaultSource($type,$context,$data){
        $this->JsModule->provideDefaultSource($type,$context,$data);
    }




}
