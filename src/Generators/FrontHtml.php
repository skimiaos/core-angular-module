<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 09:15
 */

namespace Skimia\Angular\Generators;

use Illuminate\View\View;
use Illuminate\Support\Collection;
use Angular;
use Skimia\Angular\Managers\Application;

class FrontHtml extends BaseGenerator{

    /**
     * @var View
     */
    protected $view;


    public function setView(View $view){
        $this->view = $view;
    }

    public function generate($content = false,$scripts = [],$css = []){
        if($content === false)
            $this->content = $this->view->render();
        else
            $this->content = $content;

        $this->content = $this->addMeta([
            'ng-app'=> $this->application->getName()
        ],'html');
        if(!str_contains($this->content,'ng-app')){
            $this->content = '<div ng-app="'.$this->application->getName().'">'.$this->content.'</div>';
        }

        $this->content = $this->addMeta([
            'ng-controller'=> 'MainController'
        ],'body');

        if(!str_contains($this->content,'ng-controller="MainController"')){
            $this->content = $this->addMeta([
                'ng-controller'=> 'MainController'
            ],'div');
        }

        foreach ($css as $path) {
            $this->content = $this->injectBefore($this->getCss(\Str::startsWith($path,'@') ? substr($path,1): \Assets::css($path)),'</head>');
        }
        foreach ($scripts as $path) {
            $this->content = $this->injectBefore($this->getScript(\Str::startsWith($path,'@') ? substr($path,1): \Assets::js($path)),'</body>');
        }
        //TODO de même pour les scripts

        $this->content = $this->injectBefore($this->getJavascripts());
        return $this->content;
    }

    protected $_jsScripts = [];

    public function addJavascript($script){

        $this->_jsScripts[] = $script;
    }
    protected function getJavascripts(){
        $scripts = '';

        $scripts .= $this->getScript(\Assets::js('angular.js','skimia/angular'));
        $scripts .= $this->getScript(\Assets::js('angular-route.js','skimia/angular'));
        $scripts .= $this->getScript(\Assets::js('angular-animate.js','skimia/angular'));


        foreach ($this->_jsScripts as $inclued) {
            $scripts .= $this->getScript($inclued);
        }


        $scripts .= $this->getScript(route('angular::resource.application',['application'=>$this->encode($this->application->getName())]));
        return $scripts;
    }


} 