<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 09:15
 */

namespace Skimia\Angular\Generators;

use File;
use Illuminate\View\View;
use Angular;
use Route;
use Illuminate\Support\Collection;
use Skimia\Angular\Managers\Application;

class States extends BaseGenerator{

    protected $states;


    public function __construct(Application $app){
        parent::__construct($app);

        $this->states = new Collection();
    }

    public function generate(){
        //generate Application States
        $this->content .=$this->generateStates();

        //generate Controller States
        $this->content .=$this->generateStatesControllers();
        return $this->content;
    }


    protected function generateStates(){

        $states = [];
        foreach ($this->states as $name =>$state){
            $states[$name] = array_merge($state,[
                'uses'=>'',
                'controller'=>$this->getController($name),
                'url'=>preg_replace('/{([a-zA-Z0-9_]+)}/',':$1',$state['url']),
                'templateUrl'=> route('angular::resource.template',['application'=>$this->encode($this->application->getName()),'template'=>$this->encode($state['name'])])
            ]);
            unset($states[$name]['uses']);
            unset($states[$name]['as']);
            unset($states[$name]['template']);
            unset($states[$name]['controller_contents']);
            unset($states[$name]['route_url']);
        }
        $return_event = \Event::fire('skimia.angular::states.render_state',[$this->application->getName(),$states]);
        \Debugbar::info($states);

        //TODO meilleur methode de merge
        return $this->renderView('skimia.angular::javascript.states',[
            'states'=>isset($return_event[0]) && is_array($return_event[0])? $return_event[0]: $states
        ]);
    }

    protected function is_closure($t) {
        return is_object($t) && ($t instanceof \Closure);
    }

    protected function generateStatesControllers(){
        global $app;
        $states_controllers = '';

        /**
         * @var Route $route
         */
        $public_call = [];
        $app_call = [];
        foreach($app['router']->getRoutes()->getRoutes() as $route){
            if(Collection::make($route->beforeFilters())->has('angular.call')){
                if(count($route->beforeFilters()['angular.call']) > 1)

                    $app_call[$route->beforeFilters()['angular.call'][1]] = $this->getCall($route);
                elseif(count($route->beforeFilters()['angular.call']) == 1)
                    $public_call[] = $this->getCall($route);
                else
                    throw new \Exception('filter angular.call un paramètre est obligatoire [alias]');
            }

        }

        foreach($this->states as $name =>$state) {


            if($this->is_closure($state['template'])){

                $tpl = $state['template']([
                    '__app'=>$this->application->getName(),
                    '__state'=>$state['name']
                ]);
                if(!is_string($tpl))
                    $tpl->render();
            }else{
                $tpl = \View::make($state['template'],[
                    '__app'=>$this->application->getName(),
                    '__state'=>$state['name']
                ])->render();
            }

            \BladeBlock::reset();


        }

        foreach($this->states as $name =>$state){
            $vars = [];
            $vars['controller'] = $this->getController($state['name']);

            if(isset($state['uses'])) {
                $vars['rest'] = preg_replace('/%7B([a-zA-Z0-9_]+)%7D/', '\'+$stateParams.$1+\'', route(isset($state['as'])? $state['as'] :'angular.' . $this->application->getName() . '.' . $name));
                if (!$this->is_closure($state['uses'])) {

                    $class = explode('@', $state['uses'])[0];
                    $calls = [];
                    foreach ($public_call as $call) {
                        if ($call['class'] == $class)
                            $calls[] = $call;
                    }
                    if (isset($app_call[$this->application->getName()]))
                        foreach ($app_call[$this->application->getName()] as $call) {
                            if ($call['class'] == $class)
                                $calls[] = $call;
                        }

                    $vars['calls'] = $calls;
                }

            }
            $vars['controller_contents'] = isset($state['controller_contents'])?$state['controller_contents']:'';
            $vars['controller_deps'] = isset($state['controller_deps'])?$state['controller_deps']:'';
            $states_controllers .= \View::make('skimia.angular::javascript.statescontrollersjs',$vars)->render().PHP_EOL;
        }

        $calls = [];
        foreach($public_call as $call){
            $calls[] = $call;
        }
        if(isset($app_call[$this->application->getName()]))
            foreach($app_call[$this->application->getName()] as $call){
                $calls[] = $call;
            }

        $vars['calls'] = $calls;

        $states_controllers .= \View::make('skimia.angular::javascript.apiservicejs',$vars)->render().PHP_EOL;

        return $states_controllers;
    }

    public function generateStateTemplate($state_name){

        $state = $this->states->get($state_name);
        if(!isset($state)){
            throw new \Exception('state['.$state_name.'] n\'existe pas');
        }
        $generator = new AngularTplHtml($this->application,$state);


        return $generator->generate();
    }

    protected function getCall($route){

        //dd($route->uri());
        $params = [];
        foreach($route->parameterNames() as $param){
            $params[$param] = ':'.$param;
        }
        //die($route->getPrefix());
        $use = explode('@',$route->getActionName());
        return [
            'methods'=>$route->getMethods(),
            'parameters'=> $route->parameterNames(),
            'uri'=>\URL::route('',$params,true,$route),
            'class'=>$use[0],
            'action'=>$use[1],
            'alias'=> implode('',array_map('ucfirst',explode('.',$route->beforeFilters()['angular.call'][0])))
        ];
    }

    public function add($name,$url,$template,$opt=[],$sub_opts=[]){

        //options du state
        if(is_string($opt)){
            $sub_opts['uses'] = $opt;
            $opt = $sub_opts;
        }elseif($this->is_closure($opt)){
            $sub_opts['uses'] = $opt;
            $opt = $sub_opts;
        }elseif(is_array($opt)){
            $opt = array_merge($opt,$sub_opts);


        }
        $opt['url'] = $url;
        $opt['template'] = $template;
        $opt['name'] = $name;

        //ajout
        $this->states->put($name,$opt);

        if(isset($opt['uses'])) {
            if($this->is_closure($opt['uses'])){
                Route::get(isset($opt['route_url'])? $opt['route_url'] : 'angular/state/' . $this->application->getName() .'/'.$name.$url, [
                    'as' => isset($opt['as'])? $opt['as'] : 'angular.' . $this->application->getName() . '.' . $name,
                    'uses' => $opt['uses']

                ]);
            }else {
                //si on a un namespace pour la route
                try {
                    $namespace = Route::mergeWithLastGroup([])['namespace'];
                } catch (\Exception $e) {
                    $namespace = null;
                }


                //ajout de la route pour l'acces au state
                Route::get(isset($opt['route_url'])? $opt['route_url'] :'angular/state/' . $this->application->getName() .'/'.$name. $url, [
                    'as' => isset($opt['as'])? $opt['as'] : 'angular.' . $this->application->getName() . '.' . $name,
                    'uses' => isset($namespace) ? str_replace($namespace . '\\', '', $opt['uses']) : $opt['uses']

                ]);
            }

        }
        return $this;
    }


    public function setController($name,$controller){
        $state = $this->states->get($name);
        if(isset($state['controller_contents']))
            $state['controller_contents'] .=';'. $controller;
        else
            $state['controller_contents'] = $controller;

        $this->states->put($name, $state);
    }

    public function addDependency($name,$dep){
        $state = $this->states->get($name);
        if(isset($state['controller_deps']))
            $state['controller_deps'] .=','. $dep;
        else
            $state['controller_deps'] = $dep;

        $this->states->put($name, $state);
    }

    public function getController($state){
        $exp = explode('.',$state);
        $c = '';
        foreach($exp as $ex)
            $c .= ucfirst($ex);

        return $c;
    }

    public function has($state){
        return $this->states->has($state);
    }
}