<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 09:15
 */

namespace Skimia\Angular\Generators;

use File;
use Illuminate\View\View;
use Illuminate\Support\Collection;
use Angular;
use Skimia\Angular\Managers\Application;

class JsModule extends BaseGenerator{

    protected $dependencies;
    protected $mainController ='';
    protected $incluedScriptsBefore;
    protected $incluedScriptsAfter;


    public function __construct(Application $app){
        parent::__construct($app);

        $this->dependencies = new Collection();
        $this->controllers = new Collection();
        $this->incluedScriptsBefore = new Collection();
        $this->incluedScriptsAfter = new Collection();
    }

    public function generate(){
        global $app;
        //generate Application
        \Auth::roles();
        $acl = \App::make('acl');
        $actions = $acl->actions()->get();
        $can_actions = array_flip($actions);
        foreach ($actions as $action) {
            $can_actions[$action] = $acl->can($action);
        }


        //Ajoute les scripts dont depant notre application
        $this->content.= $this->includeBeforeScripts()."\n";

        //fichiers pouvant ajouter de la data de génération comme des vues avec des controlleurs globeaux etc Attention a ajouter a la main dans l'array $this->beforeRegisterClosures
        $this->executeRegisterBeforeGenerate();
        //genere le angular.module
        $this->content.= $this->generateModule($can_actions)."\n";


        //genere le main controller
        $this->content.= $this->generateMainController()."\n";
        //genere les controllers
        $this->content.= $this->generateControllers()."\n";
        //genere les states
        $this->content.= $this->generateStates()."\n";

        //Ajoute les scripts depandant de notre application
        $this->content.= $this->includeAfterScripts()."\n";

        return $this->content;
    }

    protected function includeBeforeScripts(){
        $content = '';

        foreach($this->incluedScriptsBefore as $file){
            $content .= $this->includeResource($file) . ';'."\n";
        }

        return $content;
    }

    protected function includeAfterScripts(){
        $content = '';

        foreach($this->incluedScriptsAfter as $file){
            $content .= $this->includeResource($file) . ';'."\n";
        }

        return $content;
    }

    protected function generateModule($can_actions){
        \Debugbar::info($this->dependencies->all());
        return $this->renderView('skimia.angular::javascript.module',[
            'deps'=>$this->dependencies->all(),
            'default_sources'=>$this->getDefaultSources(),
            'can_actions'=> $can_actions
        ]);
    }

    protected function generateMainController(){
        \Debugbar::info($this->dependencies->all());

        $request = \Request::create($this->application->getUrl(), 'GET',[]);
        $html = \Route::dispatch($request)->getContent();

        return $this->renderView('skimia.angular::javascript.maincontrollerjs',[
            'main_controller'=>$this->mainController
        ]);
    }

    protected function generateControllers(){
        \Debugbar::info($this->controllers->all());

        $controllers = '';
        foreach ($this->controllers as $name=>$controller) {
            $controllers .= $this->renderView('skimia.angular::javascript.controllerjs',[
                'name'=>$name,
                'dependencies'=>isset($controller['controller_deps_text'])?$controller['controller_deps_text']:'',
                'contents'=>$controller['controller_contents_text'],
            ]);
        }

        return $controllers;
    }

    public function setMainController($controller){
        $this->mainController = $controller;
    }

    protected function generateStates(){
        return $this->application->States->generate();
    }

    protected $beforeRegisterClosures = [];

    protected function executeRegisterBeforeGenerate(){
        foreach ($this->beforeRegisterClosures as $closure) {
            $closure();
        }

    }

    public function registerBeforeGenerate($closure){
        $this->beforeRegisterClosures[] = $closure;

    }












    /**
     * Ajoute une dépendance a l'application
     * @param $name dépendance angular a ajouter
     * @param bool $path si un fichier est necessaire l'ajouter ici(see addScript)
     * @return Application
     */
    public function addDependency($name, $path=false){


        $this->dependencies->push($name);
        if($path !==false){
            $this->addScript($path);
        }
        return $this;
    }


    public function addScript($path)
    {
        $this->addScriptBefore($path);
    }


    /**
     * Ajoute un script a ajouter a l'application (fichier js généré ajouté avant angular.module)
     * @param string $path si un fichier est necessaire l'ajouter ici
     * @return Application
     */
    public function addScriptBefore($path){

        $this->incluedScriptsBefore->push($path);

        return $this;
    }

    /**
     * Ajoute un script a ajouter a l'application (fichier js généré ajouté apres angular.module)
     * @param string $path si un fichier est necessaire l'ajouter ici
     * @return Application
     */
    public function addScriptAfter($path){

        $this->incluedScriptsAfter->push($path);

        return $this;
    }

    protected $controllers;

    protected function getOrCreate($name){
        if(!$this->controllers->has($name)){
            $controller = ['controller_deps'=>['$scope','$dataSource']];
            $controller['controller_deps_text'] = implode(',',$controller['controller_deps']);
            $this->controllers->put($name, $controller );
            return $this->controllers->get($name);
        }
        else
            return $this->controllers->get($name);
    }

    public function setController($name,$controller_text){


        $controller = $this->getOrCreate($name);

        if(isset($controller['controller_contents']))
        {
            if(!in_array($controller_text,$controller['controller_contents']))
                $controller['controller_contents'][] = $controller_text;
        }
        else{
            $controller['controller_contents'] = [$controller_text];
        }


        $controller['controller_contents_text'] = implode(';', $controller['controller_contents']).';';
        $this->controllers->put($name, $controller);
    }

    public function addControllerDependency($name,$dep){

        $controller = $this->getOrCreate($name);
        if(isset($controller['controller_deps']))
            if(!in_array($dep,$controller['controller_deps']))
                $controller['controller_deps'][] = $dep;
        else
            $controller['controller_deps'] = [$dep];

        $controller['controller_deps_text'] = implode(',',$controller['controller_deps']);
        $this->controllers->put($name, $controller);
    }


    protected $defaultSourcesData = null;

    public function provideDefaultSource($type,$context,$data){
        if(!isset($this->defaultSourcesData[$type]))
            $this->defaultSourcesData[$type] = [];
        $this->defaultSourcesData[$type][$context] = $data;
    }

    public function getDefaultSources(){
        if($this->defaultSourcesData == null){
            $this->defaultSourcesData = [];
            \Event::fire('skimia.angular::dataSources.provideDefaultSources:'.$this->application->getName());

        }
        return $this->defaultSourcesData;
    }




} 