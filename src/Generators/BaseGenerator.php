<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 09:24
 */

namespace Skimia\Angular\Generators;


use Skimia\Angular\Managers\Application;

class BaseGenerator {

    protected $content = '';

    /**
     * @var Application
     */
    protected $application;

    public function __construct(Application $app){
        $this->application = $app;
    }

    public function throwException($message)
    {
        $this->application->throwException($message);
    }


    protected function injectBefore($html, $before = '</body>', $content = false){
        if(!$content){
            $content = $this->content;
        }

        $pos = strripos($content, $before);
        if (false !== $pos) {
            $content = substr($content, 0, $pos) . $html . substr($content, $pos);
        } else {
            $content = $content . $html;
        }
        return $content;
    }

    protected function injectAfter($html, $before = '</body>', $content = false){
        if(!$content){
            $content = $this->content;
        }

        $pos = strripos($content, $before);
        if (false !== $pos) {
            $pos = $pos + count($before);
            $content = substr($content, 0, $pos ) . $html . substr($content, $pos);
        } else {
            $content = $content . $html;
        }

        return $content;
    }

    protected function addMeta($metas,$balise = 'html',$content = false){
        if(!$content){
            $content = $this->content;
        }

        $regex = "/<[ ]*{$balise}(.*)[ ]*>/i";
        $r_metas = '';
        foreach($metas as $meta=>$value){
            $r_metas .= " {$meta}=\"{$value}\"";
        }
        return preg_replace($regex,"<{$balise}$1{$r_metas}>",$content,1);
    }

    protected function getScript($url){
        return sprintf(
            '<script type="text/javascript" src="%s?%s"></script>' . "\n",
            $url,
            microtime()
        );
    }
    protected function getCss($url){
        return sprintf(
            '<link rel="stylesheet" href="%s?%s">' . "\n",
            $url,
            microtime()
        );
    }

    protected function encode($url){
        return str_replace('.','-',urlencode($url));
    }

    protected function renderView($name, $data=[]){
        \BladeBlock::reset();
        $content =  \View::make($name,array_merge($this->application->toArray(), $data))->render();
        return $content;
    }

    protected function includeResource($path){

        if(\File::exists($path))
            return \File::get($path);
        elseif(\View::exists($path))
            return $this->renderView($path);
        else
            $this->throwException('le Script ou la Vue ["'.$path.'"] est introuvable');

    }
} 