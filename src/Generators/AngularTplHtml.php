<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 09:15
 */

namespace Skimia\Angular\Generators;

use Illuminate\View\View;
use Illuminate\Support\Collection;
use Angular;
use Skimia\Angular\Managers\Application;
class AngularTplHtml extends BaseGenerator{

    protected $state = '';
    public function __construct(Application $app, $state){
        parent::__construct($app);
        $this->state = $state;
        \Debugbar::disable();
    }

    public function generate(){

        //$this->content = '<div ng-controller="'.$this->application->States->getController($this->state['name']).'">';
        if($this->is_closure($this->state['template'])){
            $this->content .= $this->state['template']($this->application->toArray());
        }else{
            $this->content .= $this->renderView($this->state['template']);
        }
        //$this->content .= '</div>';

        return $this->content;
    }

    protected function is_closure($t) {
        return is_object($t) && ($t instanceof \Closure);
    }



} 