<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 10:23
 */
namespace Skimia\Angular\Controllers;
use Controller;
use Angular;
use Response;
use Illuminate\Http\Response as Http;
use Skimia\Angular\Generators\ApplicationJs;

class Resources extends Controller{

    public function application($name){
        $name = str_replace('-','.',urldecode($name));
        $app = Angular::get($name);
        //securisation
        $app->isSecure('html-app');
        //generation et rendu de l'application
        $file = $app->renderJs();

        return Response::make($file, Http::HTTP_OK, ['Content-Type' => 'text/javascript']);

    }

    public function template($application,$state){
        $application = str_replace('-','.',urldecode($application));;
        $state = str_replace('-','.',urldecode($state));
        $app = Angular::get($application);
        //securisation
        $app->isSecure('html-template');
        return $app->States->generateStateTemplate($state);

    }

} 