<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 11/11/2014
 * Time: 10:36
 */

namespace Skimia\Angular\Http;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;

class AngularResponse {

    protected $response;

    public function __construct(){
        $this->response = new Collection();
    }

    public function setErrors($errors = []){

        if($this->response->has('errors')){
            $this->response['errors'] = new Collection(array_merge($this->response['errors'],$errors));
        }else{
            $this->response->put('errors', new Collection($errors->toArray()));
        }
        return $this;
    }

    public function addMessage($value, $type='success',$timeout = false){
        if($this->response->has('messages')){
            $this->response->get('messages')->push([$type,$value,$timeout]);
        }else{
            $this->response->put('messages', Collection::make([[$type,$value,$timeout]]));
        }
        return $this;
    }

    public function redirect($url, $params = []){
        if(starts_with($url,'http')){
            $this->response->put('redirect-url',$url);
        }else
            $this->response->put('redirect-state',Collection::make(['name'=>$url,'params'=>$params]));
        return $this;
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function r($data = false){

        if($data !== false)
            $this->response['data'] = $data;

        return $this->response;
    }


    protected $bigsDatasQuery = null;

    public function getBigDataStep($data_key, $count = false, $per_step = 100){
        if($this->bigsDatasQuery == null)
            $this->handleBigDataQuery();

        if($this->bigsDatasQuery === false){
            $step = 0;
            if($count === false){
                throw new \Exception('getBigDataStep: $count est manquant');
            }

        }elseif($this->bigsDatasQuery['context'] == $data_key){
            $step = $this->bigsDatasQuery['step'];
            $count = $this->bigsDatasQuery['count'];
        }else{
            $step = 0;
            if($count === false){
                throw new \Exception('getBigDataStep: $count est manquant');
            }
        }

    //get request step
    $bigData = new \stdClass();

    $bigData->count      = $count;
    $bigData->step       = $step;
    $bigData->skip       = $per_step * $step;
    $bigData->take       = $per_step;
    $bigData->isLastStep = ($count - (($step+1) * $per_step)) <= 0;
    $bigData->query      = $data_key;
    $bigData->filters      = json_decode($this->bigsDatasQuery['filters'],true);
    $bigData->queryUrl   = \Request::url();
    $bigData->queryMethod = strtolower(\Request::method());

    return $bigData;
}

    public function setBigData($bigData, $data){
        $bigData->data = $data;

        if($this->response->has('bigData')){
            $this->response['bigData'] = new Collection(array_merge($this->response['bigData'],[$bigData->query =>$bigData]));
        }else{
            $this->response->put('bigData', new Collection([$bigData->query =>$bigData]));
        }

    }

    public function getBigDataContext(){
        if($this->bigsDatasQuery == null)
            $this->handleBigDataQuery();

        if($this->bigsDatasQuery === false){
            return false;
        }else{
            return $this->bigsDatasQuery['context'];
        }
        //check le contexte ou false
        return false;
    }

    protected function handleBigDataQuery(){
        if(\Input::has('big_data_query')){
            $context = Input::get('big_data_query');
            $step = Input::get('big_data_step');
            $count = Input::get('big_data_count');
            $filters = Input::get('big_data_filters',false);
            $this->bigsDatasQuery = [
                'context' => $context,
                'step'=>$step,
                'count'=>$count,
                'filters'=>$filters
            ];
            return;
        }
        $this->bigsDatasQuery = false;

    }
} 