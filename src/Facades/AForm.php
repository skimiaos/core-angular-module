<?php

namespace Skimia\Angular\Facades;

use \Illuminate\Support\Facades\Facade;

class AForm extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Angular\Form\Bootstrap';
    }
}