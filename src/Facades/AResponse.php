<?php

namespace Skimia\Angular\Facades;

use \Illuminate\Support\Facades\Facade;

class AResponse extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Angular\Http\AngularResponse';
    }
}