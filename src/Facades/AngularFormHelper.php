<?php
namespace{

    function a_field_id($name,$fields){
        foreach ($fields as $k=>$value) {
            if(isset($value['__alias']) && $value['__alias'] == $name){
                return $k;

            }
        }
        return false;
    }
    function a_render_field($name,$fields){
        $field = null;
        if(!isset($fields[$name])){
            foreach ($fields as $k=>$value) {
                if(isset($value['__alias']) && $value['__alias'] == $name){
                    $field = $value;
                    $name = $k;
                    break;
                }
            }

        }else
            $field = $fields[$name];

        return AngularFormHelper::render($field['type'],$name, $field);
    }
}


namespace Skimia\Angular\Facades{
    use \Illuminate\Support\Facades\Facade;

    class AngularFormHelper extends Facade{

        protected static function getFacadeAccessor(){
            return 'angular.form.viewDataHelper';
        }


        public static function getNgModel($key,$keyname = 'form'){
            if(empty($keyname))
                $keyname = 'form';
            return preg_replace('/\[\'%([a-zA-Z$-]*)\'\]/','[$1]',$keyname.'[\''.str_replace('.','\'][\'',$key).'\']');
        }
    }
}

