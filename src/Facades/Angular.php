<?php

namespace Skimia\Angular\Facades;

use \Illuminate\Support\Facades\Facade;

class Angular extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Angular\Managers\Angular';
    }
}