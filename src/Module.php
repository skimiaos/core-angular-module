<?php

namespace Skimia\Angular;

use Skimia\Modules\ModuleBase;
use Blade;
use BladeHelper;

class Module extends ModuleBase{

    public function preBoot(){

        BladeHelper::createTag('View','<div ui-view></div>');

        BladeHelper::createTag('FlashMessages','<mc-messages></mc-messages>');

        BladeHelper::createJsCall('State','ng-click','$state.go');
        BladeHelper::createTag('EndState','</a>');

        BladeHelper::createTag('Controller','<?php if(isset($__app)&&isset($__state)):ob_start(); ?>');
        BladeHelper::createTag('EndController','<?php Angular::get($__app)->States->setController($__state,ob_get_clean());endif; ?>');

        BladeHelper::createFunction('AddDependency',null,'<?php if(isset($__app)&&isset($__state)){ Angular::get($__app)->States->addDependency($__state,$1);} ?>');

        BladeHelper::createTag('MainController','<?php if(isset($__app)):ob_start(); ?>');
        BladeHelper::createTag('EndMainController','<?php Angular::get($__app)->JsModule->setMainController(ob_get_clean());endif; ?>');

        BladeHelper::createFunction('FormField',null,'<?php echo AngularFormHelper::render($fields[$1][\'type\'],$1, $fields[$1]); ?>');

        Blade::extend(function($view, $compiler){
            $pattern = $compiler->createPlainMatcher('flashmessages');
            return preg_replace($pattern, '$1 <mc-messages></mc-messages> $2', $view);
        });

    }

    public function beforeRegisterModules(){

        $formViewDataHelper = new \Skimia\Form\Transformers\ViewDataHelper();


        \App::singleton('angular.form.viewDataHelper',function() use ($formViewDataHelper){
            return $formViewDataHelper;
        });
    }

    public function register(){
        parent::register();


    }

    public function getAliases(){
        return [
            'Angular' => 'Skimia\Angular\Facades\Angular',
            'AngularFormHelper' => 'Skimia\Angular\Facades\AngularFormHelper',
            'AForm' => 'Skimia\Angular\Facades\AForm',
            'AResponse' => 'Skimia\Angular\Facades\AResponse',
        ];
    }

} 