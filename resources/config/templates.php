<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 03/12/2014
 * Time: 18:03
 */
return [
    'form'=>'skimia.angular::form.theme',
    'crud.form'=>'skimia.angular::form.crud.form'
];