<?php

return [
    'actions'=>[
        'list'=> [
            '__name-action'=>'',
            '__add'=>'Ajouter',
            '__filters'=>'Filtres',
            '__server-filters'=>'Recherche sur le serveur ',
            '__actions'=>'Actions',

            'id-col' => 'ID'
        ],
        'get'=>[
            '__name-action'=>'Voir',
        ],
        'edit'=>[
            '__name-action'=>'Editer',
        ],
        'duplicate'=>[
            '__name-action'=>'Dupliquer',
        ],
        'delete'=>[
            '__name-action'=>'Supprimer',
        ]

    ]
];