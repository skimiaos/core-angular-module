
@block('form.start')
{{AForm::open('form')}}
@endblock

@block('form.end')
{{AForm::close()}}
@endblock

@block('form.submit')
{{AngularFormHelper::render('submit','', ['angular_submit'=>isset($angular_submit) ? $angular_submit : '']);}}
@endblock

@block('form.row.widget.content')
    @block('form.row.widget.content.'.$field['type'])
        {{AngularFormHelper::render($field['type'],$name, $field);}}
    @endshow
@endoverride

@block('form.row.label')@endblock
@block('form.row.errors')@endblock

@block('content')
    <os-container direction="column" class="layout--page-content">
        @include('skimia.form::global',array('fields'=>$fields,'form'=>$form))
    </os-container>
@endshow
