
@AddDependency('$interval')
@AddDependency('$rootScope')
@AddDependency('$timeout')
@AddDependency('uiGridConstants')
@AddDependency('i18nService')
@AddDependency('$filter')
@AddDependency('$location')
<?php
       $width = 0;

if($form->canAction('get'))
    $width ++ ;
if($form->canAction('edit'))
    $width ++ ;
if($form->canAction('duplicate'))
    $width ++ ;
if($form->canAction('delete'))
    $width ++ ;

$width += count($item_actions);
        $hasActions = $width !== 0;
        $width = $width * 50;


        ?>
//<script>
    i18nService.setCurrentLang('{{App::getLocale()}}');
    var widthActions = {{$width}};
    var filteredServer = {{json_encode($srv_filters)}};
    $scope.highlightServerFilteredHeader = function( row, rowRenderIndex, col, colRenderIndex ) {
        if($scope.gridOptions.enableFiltering && col.enableFiltering && filteredServer.indexOf(col.name) != -1){
            return 'header-filtered';
        } else {
            return '';
        }
    };
    $scope.gridOptions = {

        data : [],

        //standard options
        enableFiltering: false,
        enableSorting: {{$config['enableSorting']}},
        enableGridMenu: {{$config['enableGridMenu']}},
        enableColumnMenus: {{$config['enableColumnMenus']}},
        enableColumnResizing: {{$config['enableColumnResizing']}},

        //pagination options
        paginationTemplate: 'ui-grid/pagination-ng.html',
        paginationPageSizes: {{$config['paginationPageSizes']}},
        paginationPageSize: {{$config['paginationPageSize']}},

        //Selection options
        enableRowSelection: {{$config['enableRowSelection']}},
        enableSelectAll: {{$config['enableSelectAll']}},
        selectionRowHeaderWidth: {{$config['selectionRowHeaderWidth']}},
        multiSelect: {{$config['multiSelect']}},

        rowHeight: {{$config['rowHeight']}},
        @if($config['dragabbleRows'])
        rowTemplate: '<div grid="grid" class="ui-grid-draggable-row" draggable="true"><div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'custom\': true }" ui-grid-cell></div></div>',
        @endif
        <?php $image = false;?>
        columnDefs:[
                <?php $i = 0?>
                @foreach($config['columns'] as $name=>$column)
                    <?php $i++?>
                    {
                field: '{{$column['selector']}}',
                displayName: '{{$column['displayName']}}',

                @if($filter_enabled) headerCellClass: $scope.highlightServerFilteredHeader,@endif

                //ACCESS
                visible: {{$column['visible']}},
                enableColumnMenu: {{$column['enableColumnMenu']}},
                enableFiltering: {{$column['enableFiltering']}},
                enableSorting: {{$column['enableSorting']}},
                enableColumnResizing: {{$column['enableColumnResizing']}},
                @if($column['type'] == \Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration::_TYPE_SELECT)
                type : '{{\Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration::TYPE_STRING}}',
                @elseif($column['type'] == \Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration::_TYPE_PICTURE)
                @else
                type : '{{$column['type']}}',
                @endif

                @if($column['cellTemplate']) cellTemplate:'{{$column['cellTemplate']}}',
                @elseif($column['type'] == \Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration::_TYPE_SELECT)
                 cellTemplate:'<div class="ui-grid-cell-contents">'+
                <?php $fields = $form->getOptions()->ActionFields($form::$LIST_REST_ACTION)->toFieldsArray();


                    if(!array_key_exists('choices',$fields[$name]) && array_key_exists('choicesFct',$fields[$name])){
                        $fields[$name]['choices'] = $fields[$name]['choicesFct']();
                    }else if(!array_key_exists('choices',$fields[$name])){
                        die('la config du select doit avoir une clef choices conenant la liste -> MEMORY OF DIE');
                    }

                 ?>
                        @if(array_key_exists($name,$fields))


                            @foreach($fields[$name]['choices'] as $key=>$sub_name)
                                '<span ng-if="row.entity[col.field] == {{$key}}">{{$sub_name}}</span>'+
                            @endforeach

                        @else

                            'impossible : manque l\'actionField list "{{$name}}"' +
                        @endif
                        '</div>',
                @elseif($column['type'] == \Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration::_TYPE_PICTURE)<?php $image = true;?>
                    cellTemplate:'ui-grid/imagecell.html',
                @endif
                @if($column['width']) width: {{$column['width']}},@endif
                @if(count($column['filters']) > 0)
                        <?php echo count($column['filters']) > 1 ? 'filters:[':'filter:';?>
                        <?php $fi = 0?>
                        @foreach($column['filters'] as $filter)
                            @if($fi !=0),@endif
                            {
                condition: {{$filter['condition']}},
                @if($filter['placeholder']) placeholder:'{{$filter['placeholder']}}',@endif

                                flags:{
                    caseSensitive: {{$filter['flags']['caseSensitive'] ? 'true':'false'}}
                                },
                @if($filter['selectOptions']) selectOptions:[
                    <?php $i = 0?>
                    @foreach($filter['selectOptions'] as $value=>$opt)
                    @if($i !=0),@endif
                                        {value:'{{$value}}',label:'{{$opt}}'}
                    <?php $i += 1?>

                    @endforeach
            ],@endif

                                type:{{$filter['type']}}
                            }
            <?php $fi += 1?>
        @endforeach
        @endif

<?php echo count($column['filters']) > 1 ? ']':'';?><?php echo count($column['filters']) > 0 ? ',':'';?>

            enableHiding: {{$column['enableHiding']}}
                    }
                    @if($hasActions || $i < count($config['columns'])) , @endif
        @endforeach

        @if($hasActions)
            {
        field:'idss',
                displayName:'{{ trans('skimia.angular::crud.actions.list.__actions') }}',
            enableColumnMenu:false,
            enableFiltering:false,
            enableSorting:false,
            enableHiding:false,
            cellTemplate:'ui-grid/actionscell.html',
            width: widthActions > 100? widthActions : 100
    }
    @endif

    ],
    rowHeight:{{$image ? '140':'40'}}

    };

    @if($config['enableFiltering'])
        $scope.toggleFiltering = function(){
            $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
            $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
        };

    @endif

    $scope.gridOptions.onRegisterApi = function(gridApi){
        //set gridApi on scope
        $scope.gridApi = gridApi;

        @if($config['dragabbleRows'])
            var $saveInProgress = false;
            var $needResave = false;
            var saveOrder = function(){
                if($saveInProgress){
                    $needResave = true;
                    return;
                }
                $saveInProgress = true;

                $orders = {};
                angular.forEach($scope.gridOptions.data,function(picture,order){
                    $orders[picture.id] = order;
                });
                $angular_response($scope).post('{{route($form->getRouteStart().'reorder')}}',{ 'orders':$orders})
                        .then(function(data){
                            console.log(data.data);
                            $saveInProgress = false;
                            if($needResave){
                                $needResave = false;
                                saveOrder();
                            }
                        });
            }
            gridApi.draggableRows.on.rowDropped($scope, function (info, dropTarget) {

                saveOrder();
                });
        @endif

        @if($can_batch)
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    if(gridApi.selection.getSelectedRows().length > 0)
                        $scope.allowBatch = true;
                    else
                        $scope.allowBatch = false;
                });

        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
            if(gridApi.selection.getSelectedRows().length > 0)
                $scope.allowBatch = true;
            else
                $scope.allowBatch = false;
            console.log(gridApi.selection.getSelectedRows());
        });
        @endif

        $timeout(function(){
                    $scope.gridApi.core.handleWindowResize();
                }, 100);

        $(window).on('resize', function(){
            $scope.gridApi.core.handleWindowResize();
        });

        $scope.$on('sidenavopenchanged', function($open){
            $interval(function(){
                $scope.gridApi.core.handleWindowResize();


            },5,100);
            $scope.gridApi.core.refresh();
        });

        @if($config['enableFiltering'] && $filter_enabled)



        $scope.validateSearch = function() {
            var grid = $scope.gridApi.grid;

            var columns = {};
            angular.forEach(grid.columns,function(value){
                if(value.enableFiltering && filteredServer.indexOf(value.name) != -1){
                    var filters = [];

                    angular.forEach(value.filters,function(filter){
                        if(angular.isDefined(filter.term)){
                            filters.push(filter.term);
                        }

                    });

                    if(filters.length){
                        columns[value.name] = filters;
                        applied = true;
                    }

                }
            });
            $scope.bigDataFilters = columns;
            $scope.gridOptions.data = [];
            $scope.bigDataReset();




        };
        @endif

        };

    controller_handler.notifyScopeInitialised = function($scope){

        @if($config['autoRedirect'])

            if($scope.entities.length == 1 @if(is_string($config['autoRedirectOptions']['onlyWithout']) && !empty($config['autoRedirectOptions']['onlyWithout'])) &&      ! angular.isDefined($rootScope.can_actions['{{$config['autoRedirectOptions']['onlyWithout']}}'] ) &&  $rootScope.can_actions['{{$config['autoRedirectOptions']['onlyWithout']}}']
            @endif
            ){
                @if($config['autoRedirectOptions']['mode'] == 'raw')

                $state.go('{{$config['autoRedirectOptions']['state']}}',{{json_encode($config['autoRedirectOptions']['params'],JSON_FORCE_OBJECT)}});

                @else

                $state.go('{{$config['autoRedirectOptions']['state']}}',{ {{$config['autoRedirectOptions']['params']}}: $scope.entities[0].id });

                @endif
            }
        @endif
        @if(!$form->getListRestConfiguration()->steppingEnabled())
            $scope.gridOptions.data = $filter('toArray')($scope.entities);
        @endif


    };
    @if($form->getListRestConfiguration()->steppingEnabled())
    var milisecondsToString = function(milliseconds) {
        // TIP: to find current time in milliseconds, use:
        // var  current_time_milliseconds = new Date().getTime();

        function numberEnding (number) {
            return (number > 1) ? 's' : '';
        }

        var temp = Math.floor(milliseconds / 1000);
        var years = Math.floor(temp / 31536000);
        if (years) {
            return years + ' year' + numberEnding(years);
        }
        //TODO: Months! Maybe weeks?
        var days = Math.floor((temp %= 31536000) / 86400);
        if (days) {
            return days + ' day' + numberEnding(days);
        }
        var hours = Math.floor((temp %= 86400) / 3600);
        if (hours) {
            return hours + ' hour' + numberEnding(hours);
        }
        var minutes = Math.floor((temp %= 3600) / 60);
        if (minutes) {
            return minutes + ' minute' + numberEnding(minutes);
        }
        var seconds = temp % 60;
        if (seconds) {
            return seconds + ' second' + numberEnding(seconds);
        }
        return 'less than a second'; //'just now' //or other string you like;
    };
    var time = (new Date()).getTime();
    $scope.bigDataManualMerge = true;
    $scope.end_bigData = function(){
        $scope.gridOptions.bigDataMax = '';
    };

    $scope.notify_bigData = function($new){
        var c_time = (new Date()).getTime();
        var elpased = c_time - time;
        var alltime = (elpased * $scope.bigDataAllCount) / $scope.gridOptions.data.length;
        var remaining = milisecondsToString(alltime - elpased);
        $scope.gridOptions.bigDataMax = 'Loading( '+$scope.bigDataAllCount+' ) - ' + remaining;
        angular.forEach($new,function(valsue){
            $scope.gridOptions.data.push(valsue);

        });
        //console.warn('Big data penser a faire une meilleur methode de merge dans la grid>Api');
        $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ROW );
    };
    @endif
    @if($form->canAction('delete'))

    $scope.remove = function($entity, $overridde) {

        if(!angular.isDefined($overridde))
            $overridde = false;

        if ($overridde || confirm("Voulez-vous supprimer ??")) { // Clic sur OK

            var $r = {};
            angular.forEach($stateParams,function(value,key){
                $r[':'+key] = value;
            });


            $url = replace('{{ $delete_url }}', angular.extend($r,{':id': $entity.id}));

            $angular_response($scope).delete($url).success(function (data) {
                $scope.gridOptions.data.splice($scope.gridOptions.data.indexOf($entity), 1);
                $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ROW );
            });
        }
    };
    @endif

    @if($can_batch)
        $scope.getBatchIds = function(){
        $ids = [];
        angular.forEach($scope.gridApi.selection.getSelectedRows(), function(item) {
            $ids.push(item.id);
        });
        console.log($ids);
        return $ids;
    };

    @foreach($batch_actions as $batchname=>$bat)

        @if($bat['type'] == 'php')
            $scope.{{'__batch'.ucfirst($batchname)}} = function(){
        $ids = $scope.getBatchIds();

        $closure = $scope.{{'batch'.ucfirst($batchname)}};
        $closure = angular.isDefined($closure) ? $closure : function(){} ;

        $angular_response($scope).post('{{route($bat['route'])}}',{ 'ids_batch':$ids})
                .success(function(){
                    $closure($ids);
                });

    };
    @endif

@endforeach

$scope.batchDelete = function($ids){
        $remove_entities = [];
        angular.forEach($scope.gridApi.data, function(item) {
            if($ids.indexOf(item.id) !== -1 ){
                $remove_entities.push(item);

            }
        });

        angular.forEach($remove_entities, function(item) {
            $scope.gridApi.data.splice($scope.gridApi.data.indexOf(item), 1);
        });
        $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ROW );

    };
    @endif

//</script>