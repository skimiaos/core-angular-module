{{-- Crud list default view
  Implements blocks:
    - page.title (angular.form.layout)
    - page.icon (angular.form.layout)
    - page.actions (angular.form.layout)
    - page.content (angular.form.layout)
    - page.scripts (angular.form.layout)
  --}}

@extends('skimia.angular::form.layout')



@block('page.title')
    <?php echo (isset($title) ? str_replace(['{{','}}'],['\'+','+\''],addslashes($title)): 'Titre de l\'action' ); ?>
@endoverride



@block('page.icon')
    <?php echo (isset($icon) ? $icon : 'os-icon-cancel-3' ); ?>
@endoverride



@block('page.actions')
    @if($form->canAction('create'))
        <a class="waves-effect waves-light btn-flat transparent white-text"
           ng-click="$state.go('{{$form->getStateStart()}}create',$stateParams)">
            <i class="os-icon-plus-5"/> {{ trans('skimia.angular::crud.actions.list.__add') }}
        </a>
    @endif

    @foreach($actions as $id=>$action)
        <a class="waves-effect waves-light btn-flat transparent white-text @if(isset($action['class']) && $action['class']) {{$action['class']}}@endif"
           ng-click="$state.go('{{$action['state']['name']}}'@if(isset($action['state']['params'])), {{$action['state']['params']}} @endif );">
            @if(isset($action['icon']) && $action['icon']) <i class="{{$action['icon']}}"></i> @endif
            {{$action['label']}}
        </a>
    @endforeach
    <span flex></span>

    @if($can_batch)
        @foreach($batch_actions as $batchname=>$bat)
            <a class="layout-crud-list-actions-filters waves-effect waves-light btn-flat transparent white-text @if(isset($bat['class'])) {{$bat['class']}}@endif"
               ng-show="allowBatch"
               ng-click="{{($bat['type'] == 'php' ? '__batch':'batch').ucfirst($batchname)}}()" >
                @if(isset($bat['icon'])) <i class="{{$bat['icon']}}"/>@endif {{$bat['label']}}
            </a>
        @endforeach
    @endif


    @if($config['enableFiltering'])
        <a class="waves-effect waves-light btn-flat transparent white-text layout-crud-list-actions-filters"
           ng-click="toggleFiltering()" >
            {{ trans('skimia.angular::crud.actions.list.__filters') }} <i class="os-icon-filter-1"/>
        </a>
    @endif
@endoverride



@block('page.content')
    <div ui-grid="gridOptions" @if($empty_message !== false)ng-hide="entities.length == 0"@endif os-flex="1"
         ui-grid-pagination
         ui-grid-selection
         ui-grid-resize-columns
         @if($config['dragabbleRows']) ui-grid-draggable-rows @endif
         ui-grid-move-columns>

    </div>
@if($empty_message !== false)
<div class="uiGridDefaultMessage" ng-show="entities.length == 0" os-flex="1">
<div class="uiGridDefaultMessage" ng-show="entities.length == 0" os-flex="1">
    <div class="row"><i class="os-icon-up-bold-1 fa-2x" style="padding-left: 20px"></i></div>
        <div class="container">
        <br><br>
        <h1 class="header center">{{$empty_message[1]}}</h1>
        <div class="row center">
            <h5 class="header col s12 light">{{$empty_message[0]}}</h5>
        </div>

        <br><br>

    </div>

</div>
@endif
@endoverride



@block('page.scripts')
    <script type="text/ng-template" id="ui-grid/actionscell.html">
        <div class="ui-grid-row-actions">

            @if($form->canAction('get'))
                <a {{os_tooltip(trans('skimia.angular::crud.actions.get.__name-action'),'left')}}
                   ng-click="grid.appScope.$state.go('{{ $form->getStateStart() }}get',grid.appScope.angular.extend({'entityId':row.entity.id},grid.appScope.$stateParams));"
                   class="btn-floating blue"><i class="large mdi-action-search"></i></a>
            @endif
            @foreach($item_actions as $id=>$action)
                <a {{os_tooltip($action['label'],'top')}}
                   @if(isset($action['ngShow']))ng-show="{{$action['ngShow']}}" @endif
                   @if(isset($action['ngHide']))ng-hide="{{$action['ngHide']}}" @endif
                   ng-click="grid.appScope.$state.go('{{$action['state']['name']}}'@if(isset($action['state']['params'])), {{$action['state']['params']}} @endif );"
                   class="btn-floating @if(isset($action['class']) && $action['class']) {{$action['class']}}@endif"><i class="{{$action['icon']}}"></i></a>
            @endforeach
            @if($form->canAction('edit'))
                <a {{os_tooltip(trans('skimia.angular::crud.actions.edit.__name-action'),'left')}}

                   ng-click="grid.appScope.$state.go('{{ $form->getStateStart() }}edit',grid.appScope.angular.extend({'entityId':row.entity.id},grid.appScope.$stateParams));"
                   class="btn-floating yellow darken-1"><i class="large mdi-editor-mode-edit"></i></a>
            @endif
            @if($form->canAction('duplicate'))
                <a {{os_tooltip(trans('skimia.angular::crud.actions.duplicate.__name-action'),'left')}}
                   ng-click="alert('duplicate')"
                   class="btn-floating blue"><i class="large mdi-content-content-copy"></i></a>
            @endif
            @if($form->canAction('delete'))
                <a {{os_tooltip(trans('skimia.angular::crud.actions.delete.__name-action'),'left')}}
                   ng-click="grid.appScope.remove(row.entity)"
                   class="btn-floating red"><i class="large mdi-action-highlight-remove"></i></a>
            @endif

        </div>
    </script>

<style>
    .ui-grid-pager-control .btn{
        line-height: 30px;
        height: 30px;
        padding: 0 5px;
        margin: 0 3px;
        vertical-align: initial;}
    .flipbefore:before{
        transform:rotate(180deg)
    }
</style>
    <script type="text/ng-template" id="ui-grid/pagination-ng.html">
        <div
                role="contentinfo"
                class="ui-grid-pager-panel"
                ui-grid-pager
                ng-show="grid.options.enablePaginationControls">
            <div
                    role="navigation"
                    class="ui-grid-pager-container">
                <div
                        role="menubar"
                        class="ui-grid-pager-control">
                    <button
                            type="button"
                            role="menuitem"
                            class="ui-grid-pager-first btn"
                            ui-grid-one-bind-title="aria.pageToFirst"
                            ui-grid-one-bind-aria-label="aria.pageToFirst"
                            ng-click="pageFirstPageClick()"
                            ng-disabled="cantPageBackward()">
                        <i class="os-icon-to-start-2"></i>
                    </button>
                    <button
                            type="button"
                            role="menuitem"
                            class="ui-grid-pager-previous btn"
                            ui-grid-one-bind-title="aria.pageBack"
                            ui-grid-one-bind-aria-label="aria.pageBack"
                            ng-click="pagePreviousPageClick()"
                            ng-disabled="cantPageBackward()">
                        <i class="os-icon-play-3 flipbefore"></i>
                    </button>
                    <input
                            type="number"
                            ui-grid-one-bind-title="aria.pageSelected"
                            ui-grid-one-bind-aria-label="aria.pageSelected"
                            class="ui-grid-pager-control-input"
                            ng-model="grid.options.paginationCurrentPage"
                            min="1"
                            max="@{{ paginationApi.getTotalPages() }}"
                            required />
        <span
                class="ui-grid-pager-max-pages-number"
                ng-show="paginationApi.getTotalPages() > 0">
                <abbr
                        ui-grid-one-bind-title="paginationOf">
                    /
                </abbr>
            @{{ paginationApi.getTotalPages() }}
              </span>
                    <button
                            type="button"
                            role="menuitem"
                            class="ui-grid-pager-next btn"
                            ui-grid-one-bind-title="aria.pageForward"
                            ui-grid-one-bind-aria-label="aria.pageForward"
                            ng-click="pageNextPageClick()"
                            ng-disabled="cantPageForward()">
                        <i class=" os-icon-play-3"></i>
                    </button>
                    <button
                            type="button"
                            role="menuitem"
                            class="ui-grid-pager-last btn"
                            ui-grid-one-bind-title="aria.pageToLast"
                            ui-grid-one-bind-aria-label="aria.pageToLast"
                            ng-click="pageLastPageClick()"
                            ng-disabled="cantPageToLast()">
                        <i class="os-icon-to-end-2"></i>
                    </button>
                </div>
                <div
                        class="ui-grid-pager-row-count-picker"
                        ng-if="grid.options.paginationPageSizes.length > 1">
                    <select
                            ui-grid-one-bind-aria-labelledby-grid="'items-per-page-label'"
                            ng-model="grid.options.paginationPageSize"
                            ng-options="o as o for o in grid.options.paginationPageSizes"></select>
                <span
                        ui-grid-one-bind-id-grid="'items-per-page-label'"
                        class="ui-grid-pager-row-count-label">
                &nbsp;@{{sizesLabel}}
          </span>
                </div>
        <span
                ng-if="grid.options.paginationPageSizes.length <= 1"
                class="ui-grid-pager-row-count-label">
                @{{grid.options.paginationPageSize}}&nbsp;@{{sizesLabel}}
        </span>
            </div>
            <div
                    class="ui-grid-pager-count-container">
                <div
                        class="ui-grid-pager-count">
                <span
                        ng-show="grid.options.totalItems > 0">
                @{{showingLow}}
                    <abbr
                            ui-grid-one-bind-title="paginationThrough">
                        -
                    </abbr>
                    @{{showingHigh}} @{{paginationOf}} @{{grid.options.totalItems}} @{{totalItemsLabel}} <i>@{{grid.options.bigDataMax}}</i>
              </span>
                </div>
            </div>
        </div>
    </script>


    <script type="text/ng-template" id="ui-grid/imagecell.html">
        <div class="ui-grid-cell-contents"><img style="height: 100%" ng-src="@{{COL_FIELD}}"/></div>
    </script>
<style>
    .ui-grid-draggable-row-over {
        position: relative;
        color: #AAA;
    }

    .ui-grid-draggable-row-over:before {
        content: " ";
        display: block;
        position: absolute;
        left: 0;
        width: 100%;
        border-bottom: 1px dashed #AAA;
    }

    .ui-grid-draggable-row-over--above:before {
        top: 0;
    }

    .ui-grid-draggable-row-over--below:before {
        bottom: 0;
    }
</style>
@endoverride


@Controller

@include('skimia.angular::form.crud.actions.list-js')

@EndController