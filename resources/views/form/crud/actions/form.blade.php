@extends('skimia.angular::form.layout')

@block('page.title')
<?php echo (isset($title) ? str_replace(['{{','}}'],['\'+','+\''],addslashes($title)): 'Titre de l\'action' ); ?>
@endoverride



@block('page.icon')
<?php echo (isset($icon) ? $icon : 'os-icon-cancel-3' ); ?>
@endoverride

@block('page.actions')
    @if(isset($action) && $action != 'create')
    <a class="waves-effect waves-light btn-flat transparent white-text"
       ng-class="{disabled:!activate,green:activate}"
       ng-click="save(form,$event,1)">
        <i class="os-icon-floppy-1"></i>
        {{isset($action) && $action == 'create' ? 'Creer et rester':'Enregistrer'}}
    </a>
    @endif
    @if(isset($action) && $action == 'create')
    <a class="waves-effect waves-light btn-flat transparent white-text"
       ng-class="{disabled:!activate,orange:activate}"
       ng-click="save(form,$event,2)">
        <i class="os-icon-floppy-1"></i>
        {{ 'Creer'}}
    </a>
    @endif

    <a class="waves-effect waves-light btn-flat transparent white-text"
       ng-class="{disabled:!activate,red:activate}"
       ng-click="save(form,$event,3)">
        <i class="os-icon-cancel"></i>
        {{ 'Quitter'}}
    </a>
@endblock


@block('page.content')
    {{AForm::open('form')}}

    @foreach($fields as $name=>$field)
        @block('page.content.form.before'.$name)@endshow
        @if(!isset($field['display']) || $field['display'] !== false)
            {{AngularFormHelper::render($field['type'],$name, $field);}}
        @endif
        @block('page.content.form.after'.$name)@endshow
    @endforeach

    {{AForm::close()}}
    @block('page.content.afterform')@endshow
@endoverride


@Controller

@include('skimia.angular::form.crud.actions.form-js')

@EndController