
@AddDependency('$rootScope')
@AddDependency('$timeout')
@AddDependency('messageCenterService')
//<script>

    $scope.activate = false;

    controller_handler.notifyScopeInitialised = function($scope){
        if(angular.isDefined($scope.beforeControllerInit)) $scope.beforeControllerInit($scope);
        $scope.activate = true;
    };

    $scope.gotoList = function(event){

        if(!angular.isDefined(event)){
            $state.go('{{$list_state}}',$stateParams);
        }else{
            @if(isset($redirect)  && $redirect =='edit')
            $state.go('{{$edit_state}}',angular.extend({entityId:event.entity.id},$stateParams));
            @else
            $state.go('{{$list_state}}',$stateParams);
            @endif
        }





    };

    $scope.save = function(form,$event,status){


        if(angular.isDefined($scope.beforeSave) && status != 3)$scope.beforeSave(form,$event,status);
        if($scope.activate == false)
            return ;

        $scope.activate = false;

        if(status == 3){
            $scope.gotoList();
            return
        }
        var $cleaned_form = {};
        if(angular.isDefined(form.__deleting)){
            angular.forEach(form,function(value,key){
                if($.inArray(key, form.__deleting) == -1 && key != '__deleting'){
                    $cleaned_form[key] = value;
                }
            });
        }else
        {
            $cleaned_form = form;
        }


        @if(isset($action) && $action == 'edit')

            if(angular.isDefined(form.id) && {{$action == 'edit'?'true':'false'}}){

                var url_params = {':id':form.id};

                angular.forEach($stateParams,function(value,key){
                    url_params[':'+key] = value;
                });
                $url = replace('{{ $post_url }}',url_params);
                $angular_response($scope,$event).post($url,$cleaned_form).success(function(data){



                    if(status == 2){
                        $scope.gotoList(data);
                        return
                    }
                    $scope.activate = true;
                });
            }

        @endif

        @if(isset($action) && $action == 'create')

            if({{$action == 'create'?'true':'false'}}){

                var url_params = {};

                angular.forEach($stateParams,function(value,key){
                    url_params[':'+key] = value;
                });
                $url = replace('{{ $post_url }}',url_params);

                $angular_response($scope,$event).post($url,$cleaned_form).success(function(data){

                    if(status == 2){
                        $scope.gotoList(data);

                    }else if(status == 1){
                        $timeout(function(){
                            messageCenterService.add('info','Attention valider une deuxième fois ce formulaire entrainera la création d\'une nouvelle donnée',{timeout:5000});
                        },3000);

                    }
                    $scope.activate = true;
                });
            }
        @endif


    };
    //</script>
