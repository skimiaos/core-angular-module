{{-- Get crud page

  --}}

<os-container direction="column" class="layout--page-content">
    <md-toolbar>
        <div class="md-toolbar-tools">

            <md-button ng-click="$state.go('{{$form->getStateStart()}}edit',$stateParams)" class="md-margin md-flex">Modifier</md-button>

            <span flex></span>
            <md-button ng-click="$state.go('{{$form->getStateStart()}}list')" class="md-margin md-flex">Retour a la liste</md-button>
        </div>
    </md-toolbar>

    @foreach($fields as $name=>$field)
        <md-card>
            <md-card-content>
                <div>
                    <h3>{{ $field['label'] }}</h3>
                    <h4>{{ entity.<?php echo $name?> }}</h4>
                </div>

            </md-card-content>
        </md-card>

    @endforeach
</os-container>

