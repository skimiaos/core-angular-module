{{-- List crud page

  --}}

@extends('skimia.angular::form.layout')

@block('page.content')
    <os-container direction="row" class="layout-crud-list-actions">
        <a class="green waves-effect waves-light btn"
           ng-show="{{$form->hasCreate() ?'true':'false'}}"
           ng-click="$state.go('{{$form->getStateStart()}}create',$stateParams)">Ajouter</a>

        @foreach($actions as $id=>$action)
            <a class="waves-effect waves-light btn @if(isset($action['class'])) {{$action['class']}}@endif"
               ng-click="$state.go('{{$action['state']}}'@if(isset($action['stateParams'])), {{$action['stateParams']}} @endif );">{{$action['label']}}</a>
        @endforeach
        <span flex></span>
        <a class="waves-effect waves-light btn grey darken-1 layout-crud-list-actions-filters" ng-click="toggleFiltering()" >Filtres</a>

        @if($is_batch)
            @foreach($batch as $batchname=>$bat)
                <a ng-show="allowBatch" class="waves-effect waves-light btn @if(isset($action['class'])) {{$action['class']}}@endif" ng-click="{{($bat['type'] == 'php' ? '__batch':'batch').ucfirst($batchname)}}()" >{{$bat['label']}}</a>
            @endforeach
        @endif
    </os-container>
    <os-container direction="column" class="layout-crud-list-grid">
        <div ui-grid="gridOptions" os-flex="1"
             ui-grid-pagination
             ui-grid-selection>
        </div>
    </os-container>
@endoverride

@block('page.scripts')
    <script type="text/ng-template" id="ng-table/headers/checkbox.html">
        <input type="checkbox" ng-model="checkboxes.checked" id="select_all" name="filter-checkbox" value="" />
    </script>

    <script type="text/ng-template" id="ui-grid/imagecell.html">
        <div class="ui-grid-cell-contents"><img style="height: 100%" ng-src="@{{COL_FIELD}}"/></div>
    </script>

    <script type="text/ng-template" id="ui-grid/actionscell.html">
        <div class="ui-grid-row-actions">
            @if($form->hasGet())
                <a {{os_tooltip('Voir','left')}}
                   ui-sref="{{ $form->getStateStart() }}get({'entityId':row.entity.id})"
                   class="btn-floating blue"><i class="large mdi-action-search"></i></a>
            @endif
            @if($form->hasEdit())
                <a {{os_tooltip('Editer','left')}}
                   ui-sref="{{ $form->getStateStart() }}edit({'entityId':row.entity.id})"
                   class="btn-floating yellow darken-1"><i class="large mdi-editor-mode-edit"></i></a>
            @endif
            @if($form->hasDuplicate())
                <a {{os_tooltip('Dupliquer','left')}}
                   ng-click="alert('duplicate')"
                   class="btn-floating blue"><i class="large mdi-content-content-copy"></i></a>
            @endif
            @if($form->hasDelete())
                <a {{os_tooltip('Supprimer','left')}}
                   ng-click="grid.appScope.remove(row.entity)"
                   class="btn-floating red"><i class="large mdi-action-highlight-remove"></i></a>
            @endif
            @foreach($item_actions as $id=>$action)
                <a {{os_tooltip($action['label'],'left')}}
                   ng-click="grid.appScope.$state.go('{{$action['state']}}'@if(isset($action['stateParams'])), {{$action['stateParams']}} @endif );"
                   class="btn-floating @if(isset($action['class'])) {{$action['class']}}@endif">
                    <i class="large @if(isset($action['icon'])){{$action['icon']}}@else mdi-action-highlight-remove @endif"></i>
                </a>
            @endforeach
        </div>
    </script>
@endoverride



@Controller
@AddDependency('$interval')
@AddDependency('$timeout')
@AddDependency('uiGridConstants')
//<script>
    var widthActions = ((0 @if($form->hasGet()) + 1 @endif @if($form->hasEdit()) + 1 @endif @if($form->hasDuplicate()) + 1 @endif @if($form->hasDelete()) + 1 @endif + {{count($item_actions)}})*50);
    $scope.gridOptions = {
      enableFiltering: false,
      enableGridMenu: true,
      paginationPageSizes: [25, 50, 100, 200, 500],
      paginationPageSize: 25,
      @if($is_batch)
      enableRowSelection: true,
      enableSelectAll: true,
      selectionRowHeaderWidth: 35,
      multiSelect:true,
      @endif
      <?php $image = false;?>
      columnDefs:[
          @foreach($fields as $name=>$field)
          { field: '{{isset($field['selector'])?$field['selector']: $name}}', displayName: '{{isset($field['label'])? addslashes($field['label']):$name}}',
            @if(isset($field['type']))

                @if($field['type'] == 'select')
                     cellTemplate:'<div class="ui-grid-cell-contents">'+
                    @foreach($field['choices'] as $key=>$sub_name)
                        '<span ng-if="row.entity[col.field] == {{$key}}">{{$sub_name}}</span>'+
                    @endforeach
                    '</div>',

                @endif
                @if($field['type'] == 'image') <?php $image = true;?>
                    cellTemplate:'ui-grid/imagecell.html',

                @endif
                filter:{
                    @if($field['type'] == 'select')
                        type: uiGridConstants.filter.SELECT,
                        selectOptions:[
                                <?php $i = 0;?>
                            @foreach($field['choices'] as $key=>$sub_name)
                            @if($i != 0),@endif{ value: {{$key}}, label: '{{$sub_name}}' }
                                <?php $i++ ?>
                            @endforeach
                        ]
                    @else
                        condition: uiGridConstants.filter.CONTAINS
                    @endif
                }
          @else
            filter:{condition: uiGridConstants.filter.CONTAINS}

          @endif
          },
          @endforeach
          {
              field:'idss',
              displayName:'Actions',
              enableColumnMenu:false,
              enableFiltering:false,
              enableSorting:false,
              enableHiding:false,
              cellTemplate:'ui-grid/actionscell.html',
              width: widthActions > 100? widthActions : 100
          }
    ],
        rowHeight:{{$image ? '140':'40'}}
    };

  $scope.toggleFiltering = function(){
      $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
      $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
  };

  $scope.gridOptions.onRegisterApi = function(gridApi){
      //set gridApi on scope
      $scope.gridApi = gridApi;
      @if($is_batch)
      gridApi.selection.on.rowSelectionChanged($scope,function(row){
          if(gridApi.selection.getSelectedRows().length > 0)
              $scope.allowBatch = true;
          else
              $scope.allowBatch = false;
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
          if(gridApi.selection.getSelectedRows().length > 0)
              $scope.allowBatch = true;
          else
              $scope.allowBatch = false;
          console.log(gridApi.selection.getSelectedRows());
      });
      @endif

      $timeout(function(){
          $scope.gridApi.core.handleWindowResize();
      }, 100);

      $(window).on('resize', function(){
          $scope.gridApi.core.handleWindowResize();
      });

      $scope.$on('sidenavopenchanged', function($open){
          $interval(function(){
              $scope.gridApi.core.handleWindowResize();


          },5,100);
          $scope.gridApi.core.refresh();
      });

  };
//</script>

//<script>
    @AddDependency('ngTableParams')
    @AddDependency('$filter')
    @AddDependency('$location')


    controller_handler.notifyScopeInitialised = function($scope){
        $scope.gridOptions.data = $filter('toArray')($scope.entities);
        $scope.notify_bigData = function(){
            console.warn('Big data penser a faire une meilleur methode de merge dans la grid>Api');
            $scope.gridOptions.data = $filter('toArray')($scope.entities);
            $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ROW );
        }
    };

    @if(isset($form->getUrls()['delete']))

    $scope.remove = function($entity, $overridde) {

        if(!angular.isDefined($overridde))
            $overridde = false;

        if ($overridde || confirm("Voulez-vous supprimer ?")) { // Clic sur OK

            $url = replace('{{ $form->getUrls()['delete'] }}', {':id': $entity.id});

            $angular_response($scope).delete($url).success(function (data) {
                $scope.gridOptions.data.splice($scope.gridOptions.data.indexOf($entity), 1);
                $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ROW );
            });
        }
    };
    @endif

    @if($is_batch)

        $scope.getBatchIds = function(){
            $ids = [];
            angular.forEach($scope.gridApi.selection.getSelectedRows(), function(item) {
                $ids.push(item.id);
            });
            console.log($ids);
            return $ids;
        };

        @foreach($batch as $batchname=>$bat)

            @if($bat['type'] == 'php')
                $scope.{{'__batch'.ucfirst($batchname)}} = function(){
                    $ids = $scope.getBatchIds();

                    $closure = $scope.{{'batch'.ucfirst($batchname)}};
                    $closure = angular.isDefined($closure) ? $closure : function(){} ;

                    $angular_response($scope).post('{{route($bat['route'])}}',{ 'ids_batch':$ids})
                            .success(function(){
                                $closure($ids);
                            });

                }
            @endif

        @endforeach

        $scope.batchDelete = function($ids){
                $remove_entities = [];
                angular.forEach($scope.gridApi.data, function(item) {
                    if($ids.indexOf(item.id) !== -1 ){
                        $remove_entities.push(item);

                    }
                });

                angular.forEach($remove_entities, function(item) {
                    $scope.gridApi.data.splice($scope.gridApi.data.indexOf(item), 1);
                });
                $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ROW );

        };

    @endif
//</script>
@EndController