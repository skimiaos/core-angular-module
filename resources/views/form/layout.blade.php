{{-- Crud pages layout
  Provides blocks:
    - page.content
    - page.icon
    - page.title
    - page.actions
    - page.scripts
  --}}

<os-container direction="column" class="layout--page-inner">
    <os-container direction="column" class="layout--page-header @{{ activity.bg }}">
        @block('page.header')
            <os-container direction="row" class="layout--page-title">
                <i class="@{{ crud.icon || '@block('page.icon')os-icon-question@endshow' }} small white-text"></i>
                <h5>@{{ crud.list.title || '@block('page.title')Titre du Crud@endshow' }}</h5>
            </os-container>
            <os-container direction="row" class="layout--page-actions">
                @block('page.actions')

                @endshow
            </os-container>
        @endshow
    </os-container>

    <os-container direction="column" class="layout--page-content" os-flex="1">
        @block('page.content') 
             
        @endshow
    </os-container>
</os-container>

@block('page.scripts') 

@endshow
