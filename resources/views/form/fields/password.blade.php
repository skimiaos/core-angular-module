<os-input name="{{$name}}" type="password" class="col s12"
          @block('form.row.widget.attributes')@endshow
          @block($name.'.attributes')@endshow
          @block('form.row.widget.ng-model')ng-model="form['{{$name}}']"@endshow
          ng-init="form['{{$name}}'] = (form['{{$name}}'] ? form['{{$name}}']:null)"
          attributes="@block('form.row.widget.attributes')@endshow {{$field['dsbl']}}">
    {{$field['label']}}
</os-input>