<div @block('form.row.widget.repeat.label.container.attributes')@endshow>

    <label ng-init="{{AngularFormHelper::getNgModel($name)}} = {{AngularFormHelper::getNgModel($name)}} || []">
        {{isset($field['label'])?$field['label']:''}}
        (<a ng-click="{{AngularFormHelper::getNgModel($name)}}.push(null)">ajouter</a>)
    </label>


</div>
<os-container direction="{{isset($field['direction'])?$field['direction']:'row'}}" @block('form.row.widget.repeat.container.attributes')@endshow>
    <os-container direction="column" os-flex="1" ng-repeat="line in {{AngularFormHelper::getNgModel($name)}} track by $index" @block('form.row.widget.repeat.attributes')@endshow>

        <label>
            (<a ng-click="{{AngularFormHelper::getNgModel($name)}}.splice($index,1)">supprimer</a>)
        </label>
        {{AngularFormHelper::render($field['repeat']['type'], $name.'\'][$index' , $field['repeat']);}}
    </os-container>
</os-container>