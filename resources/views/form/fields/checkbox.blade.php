<div class="col s12">
    <input type="checkbox" id="{{$name}}" @block('form.row.widget.ng-model') ng-model="<?php echo AngularFormHelper::getNgModel($name)?>" @endshow @block('form.row.widget.attributes')@endshow>
    <label for="{{$name}}">{{$field['label']}}</label>
</div>