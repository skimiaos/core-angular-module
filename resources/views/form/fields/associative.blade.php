<label>{{isset($field['label'])?$field['label']:''}}</label>
<os-container direction="{{isset($field['direction'])?$field['direction']:'column'}}" os-flex="1" @if(isset($field['ng-show']))ng-show="{{$field['ng-show']}}"@endif>

    @foreach($field['fields'] as $key=>$sub_field)
        {{AngularFormHelper::render($sub_field['type'], ($name.(Str::endsWith($name,'$index')?'':'\'').'][\''.$key), $sub_field);}}
    @endforeach
</os-container>


