<div ng-controller="AngularTreeSelectController" os-flex="1" ng-init="ngPath = '{{str_replace(["']['",'\'][$index][\''],'.',$name)}}'">
<div class="col s12"><label for="{{$name}}">{{$field['label']}}</label></div>
<os-container direction="row" gutter="20px" class="col s12" os-flex="1">


    <select os-flex="1" id="country" ng-model="depth1value" ng-options="sdepth1value.name for (sdepth1key, sdepth1value) in form['{{str_replace(["']['",'\'][$index][\''],'.',$name)}}*selectItems']"
            ng-change="form['{{$name}}'] = depth1value.id">
        <option value=''>Select</option>
    </select>

    <select os-flex="1" id="country" ng-show="depth1value && depth1value.children.length > 0" ng-model="depth2value" ng-options="sdepth2value.name for (sdepth2key, sdepth2value) in depth1value.children"
            ng-change="form['{{$name}}'] = depth2value.id">
        <option value=''>Select</option>
    </select>

    <select os-flex="1" id="country" ng-show="depth2value && depth2value.children.length > 0" ng-model="depth3value" ng-options="sdepth3value.name for (sdepth3key, sdepth3value) in depth2value.children"
            ng-change="form['{{$name}}'] = depth3value.id">
        <option value=''>Select</option>
    </select>

    <select os-flex="1" id="country" ng-show="depth3value && depth3value.children.length > 0" ng-model="depth4value" ng-options="sdepth4value.name for (sdepth4key, sdepth4value) in depth3value.children"
            ng-change="form['{{$name}}'] = depth4value.id">
        <option value=''>Select</option>
    </select>

    <select os-flex="1" id="country" ng-show="depth4value && depth4value.children.length > 0" ng-model="depth5value" ng-options="sdepth5value.name for (sdepth5key, sdepth5value) in depth4value.children"
            ng-change="form['{{$name}}'] = depth5value.id">
        <option value=''>Select</option>
    </select>

    <select os-flex="1" id="country" ng-show="depth5value && depth5value.children.length > 0" ng-model="depth6value" ng-options="sdepth6value.name for (sdepth6key, sdepth6value) in depth5value.children"
            ng-change="form['{{$name}}'] = depth6value.id">
        <option value=''>Select</option>
    </select>




</os-container>
</div>