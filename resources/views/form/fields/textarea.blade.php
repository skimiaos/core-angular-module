<div class="row" os-flex="1">
<div class="input-field col s12">

    <textarea onclick="$('.materialize-textarea').trigger('keydown')" class="materialize-textarea" @block('form.row.widget.ng-model')
              ng-model="form['{{$name}}']" @endshow name="{{$name}}" {{$field['dsbl']}} @block('form.row.widget.attributes')@endshow>
    </textarea>
    <label for="{{$name}}">{{$field['label']}}</label>
</div><script></script>
</div>