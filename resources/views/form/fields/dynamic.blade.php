<label>{{isset($field['label'])?$field['label']:''}}</label>

<os-container direction="column">

    <div os-flex ng-repeat="(key,field) in form['{{str_replace(["']['",'\'][$index][\''],'.',$name)}}*dynamicDefinition']">


        <div class="row" os-flex="1" ng-if="field.type == 'textarea'">
            <div class="input-field col s12">

                <textarea id="dynamic-@{{field.key}}" onclick="$('.materialize-textarea').trigger('keydown')" class="materialize-textarea"
                          ng-model="form['{{$name}}'][field.key]">
                </textarea>
                <label for="dynamic-@{{field.key}}">@{{field.name}}</label>
            </div>
        </div>
        <div class="row" os-flex="1" ng-if="field.type == 'wysiwyg'">
            <div class="col s12">
                <label for="dynamic-@{{field.key}}">@{{field.name}}</label>
                <textarea ckeditor="{}" id="dynamic-@{{field.key}}" name="dynamic-@{{field.key}}"
                          ng-model="form['{{$name}}'][field['key']]">
                </textarea>
            </div>
        </div>
        <div class="row" os-flex="1" ng-if="field.type == 'text'">
            <label for="dynamic-@{{field.key}}">@{{field.name}}</label>
            <input
                      type="text" class="os-input col s12"
                      ng-model="form['{{$name}}'][field.key]">

            </input>

        </div>
    </div>
</os-container>
<style>
    .cke_chrome {
        visibility: inherit !important;
    }
</style>