@if($field['relation'] == 'many_to_many')
    <multi-select
            ng-model="form['{{$name}}']"
            available="form['{{$name}}.relation'].available"
            selected-label="Current {{$name}}"
            available-label="Available {{$name}}"
            display-attr="{{form['<?php echo $name?>.relation'].show_collumn}}">

    </multi-select>
@endif

@if($field['relation'] == 'many_to_one')

    {{AngularFormHelper::render('select', $name , $field);}}
@endif