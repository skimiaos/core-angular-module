<a class="waves-effect waves-light btn green" ng-click="{{ isset($field['ng-click']) ? $field['ng-click'] :  AForm::getSubmitAction($field['angular_submit'])}}" >
    {{isset($field['label']) ? $field['label'] : 'Enregistrer'}}
</a>