<os-input name="{{$name}}" type="{{$field['type']}}" class="os-input col s12"
        @block('form.row.widget.attributes')@endshow
        @block($name.'.attributes')@endshow
        @block('form.row.widget.ng-model') ng-model="<?php echo AngularFormHelper::getNgModel($name,isset($field['formName']) ? $field['formName']:'form')?>" @endshow
        @if(isset($field['ng-show']))ng-show="{{$field['ng-show']}}"@endif
          @if( (isset($field['disabled']) && $field['disabled'] == true) || in_array('disabled',is_object($field) ? $field->toArray():$field,true)) os-disabled="true" @endif
        attributes="@block('form.row.widget.attributes')@endshow {{$field['dsbl']}}">
    {{$field['label']}}
</os-input>