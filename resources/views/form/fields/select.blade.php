<os-select name="{{$name}}"
        @block('form.row.widget.attributes')@endshow
        @block('form.row.widget.ng-model') ng-model="<?php echo AngularFormHelper::getNgModel($name,isset($field['formName']) ? $field['formName']:'form')?>" @endshow

        @block('form.row.widget.select-options')
           options="form['{{str_replace(["']['",'\'][$index][\''],'.',$name)}}*selectItems']"
           @endshow
        placeholder="Choisissez une option">
    {{isset($field['label']) ? $field['label'] : ucfirst($name)}}
</os-select>
