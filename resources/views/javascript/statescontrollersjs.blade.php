

application.controller('{{{ $controller }}}',function($stateParams, $state, $scope, $http, $angular_response, $api, $q @if(isset($controller_deps) && $controller_deps !== ''),{{$controller_deps}}@endif ){
    var controller_handler = {};
    @if(isset($rest))
    $angular_response($scope).get('{{$rest}}').
    success(function(data, status, headers, config) {
        if(status == '200'){

            if(angular.isDefined(controller_handler.notifyScopeInitialised))
                controller_handler.notifyScopeInitialised($scope);

        }
    });
    @endif
    @if(isset($calls))
    @foreach($calls as $call)
        $scope.{{{$call['action']}}} = {};
        @foreach($call['methods'] as $method)
            $scope.{{{$call['action']}}}.{{{$method}}} = function(@foreach($call['parameters']as $param)${{{$param}}}{{{$param === end($call['parameters'])?'':','}}}@endforeach @if (strtolower($method) === 'post'||strtolower($method) === 'post'){{{count($call['parameters']) == 0 ? '':','}}}$params @endif @if (strtolower($method) === 'post'||strtolower($method) === 'post' || count($call['parameters']) > 0),@endif $event) {

                $uri = replace('{{{$call['uri']}}}',{
                            @foreach($call['parameters'] as $param)
                                ':{{{$param}}}' : ${{{$param}}}{{{$param === end($call['parameters'])?'':','}}}
                            @endforeach
                            } );
                $angular_response($scope).{{{strtolower($method)}}}($uri @if (strtolower($method) === 'post'||strtolower($method) === 'post'),$params @endif ).
                    success(function(data, status, headers, config) {

                      });
            };

        @endforeach
    @endforeach
    @endif


    @if(isset($controller_contents)){{$controller_contents}}@endif
});
