
//<script>
var replace = function($url,$params){
$n_url = $url;
angular.forEach($params, function(value, key) {
$n_url = $n_url.replace(key,value);
});
return $n_url;
};

var application = angular.module('{{{$name}}}', [@if(count($deps) > 0)'{{implode("','",$deps)}}'@endif]);
application.config(['cfpLoadingBarProvider','$httpProvider', function(cfpLoadingBarProvider,$httpProvider) {
    cfpLoadingBarProvider.includeSpinner = false;

    $httpProvider.interceptors.push('apiHttpInterceptor');
}]);

application.run(
[        '$rootScope', '$state', '$stateParams','$api','$dataSource','$angular_response',
function ($rootScope,   $state,   $stateParams, $api, $dataSource,$angular_response) {

// It's very handy to add references to $state and $stateParams to the $rootScope
// so that you can access them from any scope within your applications.For example,
// <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
    // to active whenever 'contacts.list' or one of its decendents is active.
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$api = $api;
    $rootScope.angular = angular;
    $rootScope.user = {{json_encode(Auth::user())}};
    $rootScope.can_actions = {{json_encode($can_actions)}};
    $rootScope.skResponse = $angular_response($rootScope);

    $rootScope.$on("$stateChangeSuccess",  function(event, toState, toParams, fromState, fromParams) {
        // to be used for back button //won't work when page is reloaded.
        $rootScope.previousState_name = fromState.name;
        $rootScope.previousState_params = fromParams;
    });

    $rootScope.back = function() {
        $state.go($rootScope.previousState_name,$rootScope.previousState_params);
    };


    @foreach($default_sources as $type=>$contexts)
    @foreach($contexts as $context=>$data)
    $dataSource.provideDefault('{{$type}}','{{$context}}',{{json_encode($data, JSON_PRETTY_PRINT)}});
    @endforeach
    @endforeach

    }]);
    var app = application;

app.directive('stopEvent', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind('click', function (e) {
                e.stopPropagation();
            });
        }
    };
});
//</script>