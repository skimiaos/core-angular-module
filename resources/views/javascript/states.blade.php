
application.config(
    [          '$stateProvider', '$urlRouterProvider',
      function ($stateProvider,   $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        @foreach($states as $state)
            $stateProvider.state("{{$state['name']}}",{{stripcslashes(json_encode($state,JSON_FORCE_OBJECT|JSON_UNESCAPED_UNICODE))}} );
        @endforeach

}]);
