//<script>
    application.factory('apiHttpInterceptor', function($q, messageCenterService) {
        return {
            'request': function(config) {
                if(config.method == 'POST'){
                    var data = {};
                    angular.copy(config.data, data);
                    config.data = data;
                    if(angular.isDefined(config.data.__deleting)){
                        angular.forEach(config.data.__deleting, function(value){
                            delete config.data[value];
                        });
                        delete config.data.__deleting;
                    }
                }
                return config;
            },
            'responseError': function (responseError) {
                if(!angular.isDefined(responseError.data) || responseError.data == null)
                    return $q.reject(responseError);
                if (angular.isDefined(responseError.data.error)){

                    messageCenterService.add('danger',responseError.data.error.message);

                }

                return $q.reject(responseError);
            }
        };
    });
application.factory('$api',function($http, messageCenterService,$q,$injector,$rootScope,$timeout){


    var $api = {};
    @foreach($calls as $call)
            $api.{{{$call['alias']}}} = {};
            @foreach($call['methods'] as $method)
                $api.{{{$call['alias']}}}.{{{ $method }}} = function(@foreach($call['parameters']as $param)${{{$param}}}{{{$param === end($call['parameters'])?'':','}}}@endforeach @if (strtolower($method) === 'post'||strtolower($method) === 'post'){{{count($call['parameters']) == 0 ? '':','}}}$params @endif @if (strtolower($method) === 'post'||strtolower($method) === 'post' || count($call['parameters']) > 0),@endif $event) {
                    var deferred = $q.defer();
                    $uri = replace('{{{$call['uri']}}}',{
                                @foreach($call['parameters'] as $param)
                                    ':{{{$param}}}' : ${{{$param}}}{{{$param === end($call['parameters'])?'':','}}}
                                @endforeach
                                } );
                    $http.{{{strtolower($method)}}}($uri @if (strtolower($method) === 'post'||strtolower($method) === 'post'),$params @endif ).
                        success(function(data, status, headers, config) {
                            deferred.resolve($api.response(data,$event));
                          });
                    return deferred.promise;
                };

            @endforeach


       @endforeach

    var bigs_datas = {};
    $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams){
                bigs_datas
                angular.forEach(bigs_datas,function(value,key){
                    bigs_datas[key].started = false;
                });
            });

    $api.response = function(data, $event, $scope){
        if(angular.isDefined($event) && $event != null && angular.isDefined($scope) && $scope != null && $event !== null){

            if(angular.isDefined($($event.target).parents('form:first')) && angular.isDefined($($event.target).parents('form:first').attr('form-name')) ){
                if(angular.isDefined(data.errors)){
                    $scope[$($event.target).parents('form:first').attr('form-name')].errors = data.errors;
                }else{
                    $scope[$($event.target).parents('form:first').attr('form-name')].errors = {};
                }
            }

        }
        if(!angular.isDefined(data))
            return ;
        if (angular.isDefined(data.messages)){
            $i = 1;
            angular.forEach(data.messages, function(value){
                messageCenterService.add(value[0], value[1], { timeout: value[2] ? value[2]:$i*6000 });

                $i++;
            });
        }

        if (angular.isDefined(data['redirect-url'])){
            window.location.replace(data['redirect-url']);
        }

        if(angular.isDefined(data.bigData)){
            if(!angular.isDefined($scope) || $scope == null){
                alert('pour utiliser bigData il faut donner la scope à $angular_response');
            }

            $scope.bigDataReset = function(){

                $angu = $injector.get('$angular_response');

                if(angular.isDefined(bigs_datas[$scope.bigDataCacheContext.checkKey]))
                    bigs_datas[$scope.bigDataCacheContext.checkKey].canceller.resolve('cancelled');
                $url = $scope.bigDataCacheContext.queryUrl;

                if(angular.isDefined($scope.bigDataFilters)){
                    $url = $url +"?big_data_filters=" + encodeURI(JSON.stringify($scope.bigDataFilters));
                }
                if(angular.isDefined(bigs_datas[$scope.bigDataCacheContext.checkKey]))
                    delete bigs_datas[$scope.bigDataCacheContext.checkKey];
                $angu($scope)[$scope.bigDataCacheContext.queryMethod]($url).success(function(data){});

            };

            angular.forEach(data.bigData, function(value,key){
                $key = key + value.queryUrl ;
                $scope.bigDataCacheContext = value;
                $scope.bigDataCacheContext.checkKey = $key;
                if(!angular.isDefined(bigs_datas[$key])){
                    var canceller = $q.defer();
                    bigs_datas[$key] = {

                        'started': true,
                        'canceller':canceller
                    };
                }
                $scope.bigDataAllCount = value.count;
                if(!(angular.isDefined($scope.bigDataManualMerge) && $scope.bigDataManualMerge == true)){
                    if(angular.isDefined($scope[key])){
                        angular.forEach(value.data,function(valsue){
                            $scope[key].push(valsue);

                        });
                    }else{
                        $scope[key] = value.data;
                    }
                }

                if(angular.isDefined($scope.notify_bigData))
                    $scope.notify_bigData(value.data);

                if(!value.isLastStep && bigs_datas[$key].started){

                    $angu = $injector.get('$angular_response');

                    $url = value.queryUrl+'?big_data_query='+key+"&big_data_step="+(parseInt(value.step)+1)+"&big_data_count="+value.count;

                    if(angular.isDefined($scope.bigDataFilters)){
                        $url = $url +"&big_data_filters=" + encodeURI(JSON.stringify($scope.bigDataFilters));
                    }
                    $angu($scope)[value.queryMethod]($url,{ timeout: bigs_datas[$key].canceller.promise }).success(function(data){});
                }

                if(value.isLastStep || !bigs_datas[$key].started){
                    delete bigs_datas[$key];
                    if(angular.isDefined($scope.end_bigData))
                        $scope.end_bigData($key);
                }
            });


        }




        var $data = {};
        if (angular.isUndefined(data.data) || data.data == null) $data = data;else $data = data.data;

        if(angular.isDefined($scope) && $scope != null){
            angular.forEach($data,function(value,key){

                $scope[key] = value;
            });
        }



        return $data;


    };
    return $api;
});
    function isInt(n) {
        return n % 1 === 0;
    }

application.factory('$angular_response',function($http, messageCenterService,$q,$api){


    return function($scope ,$event){
        if(!angular.isDefined($scope))
            $scope = false;

        if(!angular.isDefined($event))
            $event = false;

        var getDeffered = function(){
            var deferred = $q.defer();

            deferred.promise.success = function(fn) {
                deferred.promise.then(function(response) {

                    fn(response.data, response.status, response.headers, response.config);
                });
                return deferred.promise;
            };

            deferred.promise.error = function(fn) {
                deferred.promise.then(null, function(response) {

                    fn(response.data, response.status, response.headers, response.config);
                });
                return deferred.promise;
            };

            return deferred;
        };

        var handleHttp = function($h,$defer){
            $h.success(function(data, status, headers, config) {
                //console.log(data);
                data = $api.response(data, $event ? $event:null,$scope ? $scope : null)

                $defer.resolve({
                    'data':data,
                    'status':status,
                    'headers':headers,
                    'config':config});
            }).error(function(data, status, headers, config) {
                console.error(data);
                data = $api.response(data, $event ? $event:null,$scope ? $scope : null)

                $defer.reject({
                    'data':data,
                    'status':status,
                    'headers':headers,
                    'config':config});
            });
        };
        return {
            'get': function(url,config){

                var deferred = getDeffered();

                handleHttp($http.get(url,config),deferred);

                return deferred.promise;
            },
            'head': function(url,config){

                var deferred = getDeffered();

                handleHttp($http.head(url,config),deferred);

                return deferred.promise;
            },
            'jsonp': function(url,config){

                var deferred = getDeffered();

                handleHttp($http.jsonp(url,config),deferred);

                return deferred.promise;
            },
            'delete': function(url,config){

                var deferred = getDeffered();

                handleHttp($http.delete(url,config),deferred);

                return deferred.promise;
            },
            'patch': function(url,data,config){

                var deferred = getDeffered();

                handleHttp($http.patch(url,data,config),deferred);

                return deferred.promise;
            },
            'put': function(url,data,config){

                var deferred = getDeffered();

                handleHttp($http.put(url,data,config),deferred);

                return deferred.promise;
            },
            'post': function(url,data,config){

                var deferred = getDeffered();

                handleHttp($http.post(url,data,config),deferred);

                return deferred.promise;
            }
        };


    }
});
//</script>

