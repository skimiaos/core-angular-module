<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/11/2014
 * Time: 10:24
 */

Route::group(array('namespace' => 'Skimia\Angular\Controllers'), function(){

    Route::get('/angular/application/{name}.js', array('as' => 'angular::resource.application', 'uses' => 'Resources@application'));
    Route::get('/angular/view/{name}/{template}.html', array('as' => 'angular::resource.template', 'uses' => 'Resources@template'));

});
