/*
 Multiple-select directive for AngularJS
 (c) 2013 Alec LaLonde (https://github.com/alalonde/angular-multi-select)
 License: MIT
 */
(function(angular) {
    'use strict';

    angular.module('multi-select',[])
        .directive('multiSelect', function($q,$filter) {
            return {
                restrict: 'E',
                require: 'ngModel',
                scope: {
                    selectedLabel: "@",
                    availableLabel: "@",
                    displayAttr: "@",
                    available: "=",
                    model: "=ngModel"
                },
                template: '<div class="multiSelect row">' +
                '<div class="select col m12 l5">' +
                '<label class="control-label" for="multiSelectSelected">{{ selectedLabel }} ' +
                '({{ model.length }})</label>' +
                '<select id="currentRoles" ng-model="selected.current" multiple ' +
                'class="pull-left" ng-options="e as e[displayAttr] for e in model">' +
                '</select>' +
                '</div>' +
                '<div class="select buttons col m12 l2">' +
                '<button class="btn mover left" ng-click="add()" title="Add selected" ' +
                'ng-disabled="selected.available.length == 0">' +
                '<i class="os-icon-left-open"></i>' +
                '</button>' +
                '<button class="btn mover right" ng-click="remove()" title="Remove selected" ' +
                'ng-disabled="selected.current.length == 0">' +
                '<i class="os-icon-right-open"></i>' +
                '</button>' +
                '</div>' +
                '<div class="select col m12 l5" >' +
                '<label class="control-label" for="multiSelectAvailable">{{ availableLabel }} ' +
                '({{ available.length }})</label>' +
                '<input type="text" ng-model="availableFilter">' +
                '<select id="multiSelectAvailable" ng-model="selected.available" multiple ' +
                'ng-options="e as e[displayAttr] for e in availablelist"></select>' +
                '</div>' +
                '</div>',
                link: function(scope, elm, attrs) {
                    scope.selected = {
                        available: [],
                        current: []
                    };

                    /* Handles cases where scope data hasn't been initialized yet */
                    var dataLoading = function(scopeAttr) {
                        var loading = $q.defer();
                        if(scope[scopeAttr]) {
                            loading.resolve(scope[scopeAttr]);
                        } else {
                            scope.$watch(scopeAttr, function(newValue, oldValue) {
                                if(newValue !== undefined)
                                    loading.resolve(newValue);
                            });
                        }
                        return loading.promise;
                    };

                    /* Filters out items in original that are also in toFilter. Compares by reference. */
                    var filterOut = function(original, toFilter, ignoreFilter) {
                        var filtered = [];
                        angular.forEach(original, function(entity) {
                            var match = false;
                            for(var i = 0; i < toFilter.length; i++) {
                                if(toFilter[i][attrs.displayAttr] == entity[attrs.displayAttr]) {
                                    match = true;
                                    break;
                                }
                            }
                            if(!match) {
                                filtered.push(entity);
                            }
                        });

                        if(angular.isDefined(ignoreFilter) && ignoreFilter == true)
                            return filtered;

                        var filteredData = scope.availableFilter ?
                            $filter('filter')(filtered, scope.availableFilter) :
                            filtered;
                        return filteredData;
                    };

                    scope.refreshAvailable = function() {
                        scope.availablelist = filterOut(scope.available, scope.model);
                        scope.selected.available = [];
                        scope.selected.current = [];
                    };

                    scope.add = function() {
                        scope.model = scope.model.concat(scope.selected.available);
                        scope.refreshAvailable();
                    };
                    scope.remove = function() {
                        scope.availablelist = scope.available.concat(scope.selected.current);
                        scope.model = filterOut(scope.model, scope.selected.current, true);
                        scope.refreshAvailable();

                    };

                    scope.$watch('availableFilter',function(oldV,newV){

                        scope.availablelist = filterOut(scope.available, scope.model);
                        scope.selected.available = [];
                        scope.selected.current = [];
                    });

                    $q.all([dataLoading("model"), dataLoading("available")]).then(function(results) {
                        scope.allAvailable = scope.available;
                        scope.refreshAvailable();
                    });
                }
            };
        })


})(angular);