


application.controller('AngularTreeSelectController',function($scope){

    $eventWatch = $scope.$watch('form', function(newValue, oldValue) {
        if(angular.isDefined($scope.form[$scope.ngPath+'*selectPath'])){

            $current = false;
            angular.forEach($scope.form[$scope.ngPath+'*selectPath'],function(value,key){
                if($current == false){
                    $current = $scope.form[$scope.ngPath+'*selectItems'];
                }else if(!angular.isDefined($current)){
                    return;
                }
                angular.forEach($current , function(v2){
                    if(v2.id == value){
                        $scope['depth'+(key+1)+'value'] = v2;
                        $current = v2.children;
                    }

                });

            });

            $eventWatch();
        }
    });


});