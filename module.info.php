<?php

return [
    'name'        => 'Skimia Angular',
    'author'      => 'Skimia',
    'description' => 'Mix Angular with laravel',
    'namespace'   => 'Skimia\\Angular',
    'require'     => ['skimia.modules','skimia.blade','skimia.form']
];